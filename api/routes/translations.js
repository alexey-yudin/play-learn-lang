const express = require('express');
const router = express.Router();
const TranslationController = require('../controllers/Translation');

router.get('/getTranslations', TranslationController.getTranslations);

module.exports = router;
