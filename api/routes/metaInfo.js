const express = require('express');
const router = express.Router();
const MetaInfoController = require('../controllers/MetaInfo');

router.get('/getMetaInfo/:appName', MetaInfoController.getMetaInfo);

module.exports = router;
