const express = require('express');
const router = express.Router();
const LocalityController = require('../controllers/Locality');

router.get('/getLocalities/:appName', LocalityController.getLocalities);

module.exports = router;
