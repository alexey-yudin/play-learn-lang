const express = require('express');
const router = express.Router();
const CardController = require('../controllers/Card');

router.get('/getCards/:appName/:cardsId', CardController.getCards);
router.get('/getCardsConfig/:appName', CardController.getCardsConfig);

module.exports = router;
