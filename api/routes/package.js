const express = require('express');
const router = express.Router();
const PackageController = require('../controllers/Package');

router.get('/getPackages/:appName', PackageController.getPackages);
router.get('/getNestedPackages/:appName/:packageId', PackageController.getNestedPackages);
router.get('/getNestedPackagesIds/:appName', PackageController.getNestedPackageIds);

module.exports = router;
