class LocalityController {
  static getLocalities(req, res) {
    const appName = req.params.appName;
    const locality = global.lyrebird.localities.find(app => app.appName === appName);
    res.json({
      foreign: locality.foreignLocality.localities,
      native: locality.nativeLocality.localities
    });
  }
}

module.exports = LocalityController;
