class TranslationController {
  static getTranslations(req, res) {
    const translations = global.lyrebird.translations;
    res.json({ translations });
  }
}

module.exports = TranslationController;
