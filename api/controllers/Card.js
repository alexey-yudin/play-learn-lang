class CardController {
  static getCards(req, res) {
    const appName = req.params.appName;
    const cardsId = req.params.cardsId;
    const app = global.lyrebird.cards.find(app => app.appName === appName);
    const cards = app.cards.find(card => card.id === cardsId);
    res.json(cards);
  }

  static getCardsConfig(req, res) {
    const appName = req.params.appName;
    const cardsConfig = global.lyrebird.cardsConfig.find(cardConfig => cardConfig.appName === appName);
    res.json({ cardsConfig: cardsConfig.config });
  }
}

module.exports = CardController;
