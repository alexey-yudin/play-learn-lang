class PackageController {
  static getPackages(req, res) {
    const appName = req.params.appName;
    const packages =  global.lyrebird.packages.find(app => app.appName === appName);
    if (packages) {
      res.json(packages.packageItem);
    } else {
      res.status(404).json({message: 'Not found'});
    }
  }

  static getNestedPackages(req, res) {
    const appName = req.params.appName;
    const packageId = req.params.packageId;
    const nestedAppPackages = global.lyrebird.nestedPackages.find(app => app.appName === appName);
    const nestedPackages = nestedAppPackages.nestedPackages
      .find(nestedPackage => nestedPackage.packageId === packageId);
    if (nestedPackages) {
      res.json(nestedPackages.nestedPackage)
    } else {
      res.status(404).json({message: 'Not found'});
    }
  }

  static getNestedPackageIds(req, res) {
    const appName = req.params.appName;
    const nestedPackageIds = global.lyrebird.nestedPackageIds.nestedPackageIds
      .find(app => app.appName === appName);
    res.json(nestedPackageIds.nestedIds);
  }
}

module.exports = PackageController;
