class MetaInfoController {
  static getMetaInfo(req, res) {
    const appName = req.params.appName;
    const metaInfo = global.lyrebird.metaInfo.find(app => app.appName === appName);
    res.json(metaInfo);
  }
}

module.exports = MetaInfoController;
