const fs = require('fs');
const Hjson = require('hjson');

class Package {
  constructor(directoryReader) {
    this.directoryReader = directoryReader;
  }

  getPackages() {
    return new Promise((resolve, reject) => {
      this.directoryReader.getAppDirectories().then(appDirectories => {
        const packages = appDirectories.map(appDirectory => {
          const packageItem = Hjson.parse(fs.readFileSync(`${appDirectory.pathToApp}/packs/_packages.json`).toString());
          const appName = appDirectory.appName;
          return { appName, packageItem };
        });
        resolve(packages);
      });
    });
  }

  getNestedPackages() {
    return new Promise((resolve, reject) => {
      this.directoryReader.getNestedPackagePaths().then(packagePaths => {
        const allNestedPackages = packagePaths.map(packagePath => {
          const nestedPackages = packagePath.nestedPackages.map(nestedPackagePath => {
            const packageId = nestedPackagePath.split('/').reverse()[0];
            const nestedPackage = Hjson.parse(fs.readFileSync(`${nestedPackagePath}/_packages.json`).toString());
            return { packageId, nestedPackage };
          });
          return { appName: packagePath.appName, nestedPackages };
        });
        resolve(allNestedPackages);
      });
    });
  }

  getNestedPackagesIds() {
    return new Promise((resolve, reject) => {
      this.directoryReader.getNestedPackagePaths().then(packagePaths => {
        const nestedPackageIds = packagePaths.map(nestedPackagePath => {
          const nestedIds = nestedPackagePath.nestedPackages.map(packagePath => {
            const parentId = packagePath.split('/').reverse()[0]
            const packagesFile = Hjson.parse(fs.readFileSync(`${packagePath}/_packages.json`).toString());
            return packagesFile.packages.map(packageItem => ({id: packageItem.id, parentId}));
          });
          return { appName: nestedPackagePath.appName, nestedIds: [].concat.apply([], nestedIds) };
        });
        resolve({ nestedPackageIds: [].concat.apply([], nestedPackageIds) });
      });
    });
  }
}

module.exports = Package;
