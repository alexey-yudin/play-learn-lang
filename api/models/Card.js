const fs = require('fs');
const Hjson = require('hjson');

class Card {
  constructor(directoryReader) {
    this.directoryReader = directoryReader;
  }

  getCardsConfig() {
    return new Promise((resolve, reject) => {
      this.getCards().then(cards => {
        const cardConfigs = cards.map(appCard => {
          const configCard = appCard.cards.map(card => ({id: card.id, config: card.config}));
          return { appName: appCard.appName, config: configCard };
        });
        resolve(cardConfigs);
      });
    });
  }

  getCards() {
    return new Promise((resolve, reject) => {
      this.directoryReader.getAppDirectories().then(appDirectories => {
        const allCards = appDirectories.map(appDirectory => {
          const allFiles = fs.readdirSync(`${appDirectory.pathToApp}/packs`);
            const nestedCards = allFiles
              .filter(file => {
                const fileStat = fs.statSync(`${appDirectory.pathToApp}/packs/${file}`);
                return fileStat.isDirectory();
              })
              .map(nestedPackage => {
                const nestedPackageFiles = fs.readdirSync(`${appDirectory.pathToApp}/packs/${nestedPackage}`);
                return nestedPackageFiles
                  .filter(packageFile => packageFile.endsWith('json') && packageFile !== '_packages.json')
                  .map(nestedCard => {
                    const card = fs.readFileSync(`${appDirectory.pathToApp}/packs/${nestedPackage}/${nestedCard}`).toString();
                    return Hjson.parse(card);
                  });
              });

          const cards = allFiles
            .filter(file => {
              const fileStat = fs.statSync(`${appDirectory.pathToApp}/packs/${file}`);
              return fileStat.isFile() && file !== '_packages.json' && file.endsWith('json');
            })
            .map(cardFile => {
              const card = fs.readFileSync(`${appDirectory.pathToApp}/packs/${cardFile}`).toString()
                .replace(/\\n/g, "\\n")
                .replace(/\\'/g, "\\'")
                .replace(/\\"/g, '\\"')
                .split("\n")
                .map(item => item.trim())
                .join('');
              try {
                return Hjson.parse(card);
              } catch (error) {
                reject(error);
                console.error(error);
              }
            });
            const allCards = cards.concat(nestedCards);

          return { appName: appDirectory.appName, cards: [].concat.apply([], allCards) };
        });
        resolve(allCards);
      });
    });
  }
}

module.exports = Card;
