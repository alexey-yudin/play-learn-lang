const fs = require('fs');
const Hjson = require('hjson');

class DirectoryReader {
  constructor(pathToConfig) {
    this.basePath = pathToConfig;
  }

  getNestedPackagePaths() {
    return new Promise((resolve, reject) => {
      this.getAppDirectories().then(appDirectories => {
        const packagePaths = appDirectories.map(appDirectory => {
          const pathToPackages = `${this.basePath}/${appDirectory.appName}/packs`;
          const packagesFile = Hjson.parse(fs.readFileSync(`${pathToPackages}/_packages.json`).toString());
          const nestedPackages = packagesFile.packages
            .filter(packageItem => packageItem.hasChildPacks === 'true')
            .map(packageItem => `${pathToPackages}/${packageItem.id}`);
          return { appName: appDirectory.appName, nestedPackages };
        });
        const mergedPackagePaths = [].concat.apply([], packagePaths);
        resolve(mergedPackagePaths);
      });
    });
  }

  getAppDirectories() {
    return new Promise((resolve, reject) => {
      const appDirectories = fs.readdirSync(this.basePath).map(appName => {
        const pathToApp = `${this.basePath}/${appName}`;
        return {appName, pathToApp};
      });
      resolve(appDirectories);
    });
  }
}

module.exports = DirectoryReader;
