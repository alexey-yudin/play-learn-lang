const fs = require('fs');
const Hjson = require('hjson');
const xmlParser = require('xml2json');

class MetaInfo {
  constructor(directoryReader) {
    this.directoryReader = directoryReader;
  }

  getMetaInfo() {
    return new Promise((resolve, reject) => {
      this.directoryReader.getAppDirectories().then(appDirectories => {
        const metaFiles = appDirectories.map(appDirectory => {
          const meta = fs.readFileSync(`${appDirectory.pathToApp}/values/values/strings.xml`).toString();
          const parsedXml = Hjson.parse(xmlParser.toJson(meta));
          return { appName: appDirectory.appName, strings: parsedXml };
        });
        resolve(metaFiles);
      });
    });
  }
}

module.exports = MetaInfo;
