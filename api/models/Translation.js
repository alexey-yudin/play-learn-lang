const fs = require('fs');
const Hjson = require('hjson');

class Translation {
  constructor(pathToTranslations) {
    this.pathToTranslations = pathToTranslations;
  }

  getTranslations() {
    return new Promise((resolve, reject) => {
      const translationFileNames = fs.readdirSync(this.pathToTranslations);
      const translations = translationFileNames.map(translationFileName => {
        const translationFile = fs.readFileSync(`${this.pathToTranslations}/${translationFileName}`);
        return Hjson.parse(translationFile.toString());
      });
      resolve(translations);
    });
  }
}

module.exports = Translation;
