const fs = require('fs');
const Hjson = require('hjson');

class Locality {
  constructor(directoryReader) {
    this.directoryReader = directoryReader;
  }

  getLocalities() {
    return new Promise((resolve, reject) => {
      this.directoryReader.getAppDirectories().then(appDirectories => {
        const localities = appDirectories.map(appDirectory => {
          const appFiles = fs.readdirSync(`${appDirectory.pathToApp}`);
          const localityFiles = appFiles
            .filter(appFile => appFile === '_localities.json' || appFile === '_localities_native.json');

          const foreignLocalityFile = localityFiles.find(locality => locality === '_localities.json');
          const nativeLocalityFile = localityFiles.find(locality => locality === '_localities_native.json');

          const foreignLocality = Hjson.parse(fs.readFileSync(`${appDirectory.pathToApp}/${foreignLocalityFile}`).toString());
          const nativeLocality = Hjson.parse(fs.readFileSync(`${appDirectory.pathToApp}/${nativeLocalityFile}`).toString());

          return { appName: appDirectory.appName, foreignLocality, nativeLocality };
        });
        resolve(localities);
      });
    });
  }
}

module.exports = Locality;
