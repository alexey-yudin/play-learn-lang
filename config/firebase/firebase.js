if (NODE_ENV === 'production') {
  module.exports = require('./firebase.prod.json');
} else if (NODE_ENV === 'staging') {
  module.exports = require('./firebase.staging.json');
} else if (NODE_ENV === 'dev') {
  console.log("It is", NODE_ENV);
  module.exports = require('./firebase.dev.json');
} else {
  console.error("Node env not recognized");
}
