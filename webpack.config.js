const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  debug: true,

  noInfo: false,

  entry: [
    './frontend/index.js'
  ],

  output: {
    path: __dirname + '/public/static/',
    filename: 'bundle.js'
  },

  devtool: "cheap-source-map",

  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel', query: {compact: false},
        exclude: /node_modules/
      }, {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('style', 'css')
      }, {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style', 'css!sass')
      }, {
        test: /\.(png|jpg|svg)$/,
        loader: 'file?name=[name].[ext]&regExp=node_modules/(.*)'
      }, {
        test: /\.json$/,
        loader: 'json'
      }, {
        test: /\.(ttf|otf|eot|woff|woff2)$/,
        loader: 'file?name=[name].[ext]&regExp=node_modules/(.*)'
      }
    ]
  },

  resolve: {
    extensions: ['', '.js', '.jsx']
  },

  devServer: {
    contentBase: './frontend'
  },

  plugins: [
    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify('dev')
    }),
    new ExtractTextPlugin('styles.css')
  ]
};
