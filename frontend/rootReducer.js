import { combineReducers } from 'redux';

import playLearnLang from './languages';
import khmerAlphabet from './alphakm';
import common from './common';

const rootReducer = combineReducers({
  playLearnLang: playLearnLang.reducer,
  khmerAlphabet: khmerAlphabet.reducer,
  common: common.reducer
});

export default rootReducer;
