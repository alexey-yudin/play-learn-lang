import React, { Component } from 'react';
import authService from './common/services/auth';
import * as authActions from './common/actions/auth';
import {browserHistory} from 'react-router';
import { connect } from 'react-redux';

class App extends Component {
  componentWillMount() {
    const authCredentials = authService.getSession();
    if (authCredentials) {
      this.props.loginSuccess(authCredentials);
    } else {
      browserHistory.push('/');
    }
  }

  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}

export default connect(null, {
  loginSuccess: authActions.loginSuccess
})(App);
