import * as constants from './constants/packages';

export const getLanguagePacks = state => state.playLearnLang.selectedCards;
