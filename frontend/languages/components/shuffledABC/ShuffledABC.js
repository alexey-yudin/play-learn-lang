import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/cards';
import ShuffledAbcContainer from '../../../common/components/shuffledABC/ShuffledAbcContainer';

class ShuffledABC extends Component {
  render() {
    return (
      <ShuffledAbcContainer
        cards={this.props.cards}
        fetchCards={this.props.fetchCards}
        cardsId={this.props.params.id}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    cards: state.playLearnLang.selectedCards,
  }
}

export default connect(mapStateToProps, {
  fetchCards: actions.fetchCards
})(ShuffledABC);
