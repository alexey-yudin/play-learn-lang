import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/cards';
import * as selectors from '../../selectors';
import * as commonSelectors from '../../../common/selectors';
import BingoQuizContainer from '../../../common/components/bingoQuiz/BingoQuizContainer';

class BingoQuiz extends Component {
  componentWillMount() {
    this.props.fetchCards(this.props.params.id);
  }

  render() {
    return (
      <BingoQuizContainer
        cards={this.props.cards}
        settings={this.props.settings}
        paramsId={this.props.params.id}
      />
    );
  }
}

export default connect(state => ({
  cards: selectors.getLanguagePacks(state),
  settings: commonSelectors.getBingoQuizSettings(state)
}), {
  fetchCards: actions.fetchCards
})(BingoQuiz);
