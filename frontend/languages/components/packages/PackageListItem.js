import React, { Component, PropTypes } from 'react';
import * as localityConstants from '../../../common/constants/localities';
import LanguageTranslation from '../../../common/components/shared/language/LanguageTranslation';
import LocalityImage from '../../../common/components/shared/language/LocalityImage';
import PackageListItemMenu from '../../../common/components/packages/PackageListItemMenu';

class PackageListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMenu: false
    };
    this.toggleIsMenu = this.toggleIsMenu.bind(this);
  }

  toggleIsMenu() {
    this.setState({ isMenu: !this.state.isMenu });
  }

  renderPackageItem() {
    const { packageItem } = this.props;
    return (
      <div className='col-xs-12 col-sm-6 col-md-4'>
        <div className="package-item">
          <div className="locality-translation">
            <LocalityImage
              localityType={localityConstants.FOREIGN_LOCALITY}
              className={'package-locality'}
            />
            <LanguageTranslation
              translations={packageItem.translations}
              isPackage={true}
              localityType={localityConstants.FOREIGN_LOCALITY}
            />
          </div>
          <div>
            <img src={packageItem.image.url_standard} alt="image"/>
          </div>
          <div className="locality-translation">
            <LocalityImage
              localityType={localityConstants.NATIVE_LOCALITY}
              className={'package-locality'}
            />
            <LanguageTranslation
              translations={packageItem.translations}
              isPackage={true}
              localityType={localityConstants.NATIVE_LOCALITY}
            />
          </div>
        </div>
      </div>
    );
  }

  renderPackage() {
    const { packageItem } = this.props;
    if (this.state.isMenu) {
      return (
        <PackageListItemMenu appName={'languages'} packageId={packageItem.id}/>
      );
    } else {
      return (
        <div>{this.renderPackageItem()}</div>
      );
    }
  }

  render() {
    return (
      <div onClick={this.toggleIsMenu}>
        {this.renderPackage()}
      </div>
    );
  }
}

PackageListItem.propTypes = {
  packageItem: PropTypes.object.isRequired
};

export default PackageListItem;
