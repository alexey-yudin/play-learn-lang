import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as localityActions from '../actions/localities';
import * as viewConfigActions from '../../common/actions/viewConfig';
import * as settingsActions from '../../common/actions/generalSettings';
import * as routeConstants from '../../common/constants/routes';
import NavBar from '../../common/components/navbar/NavBar';
import * as localitiesSelectors from '../../common/selectors/localities';

class App extends Component {
  componentWillMount() {
    this.props.setAppName(routeConstants.languages);
    this.props.fetchLocalities();
    this.props.fetchMetaInfo(routeConstants.languages);
    this.props.getCardsConfig(routeConstants.languages);
  }

  render() {
    const{ params, children, location } = this.props;
    return (
      <div>
        <NavBar params={params} location={location}/>
        <div className='container'>
          {children}
        </div>
      </div>
    );
  }
}

export default connect(state => ({
  localities: localitiesSelectors.getLocalities(state),
  selectedLocality: localitiesSelectors.getSelectedlocality(state)
}), {
  fetchLocalities: localityActions.fetchLocalities,
  setAppName: settingsActions.setAppName,
  fetchMetaInfo: settingsActions.fetchMetaInfo,
  getCardsConfig: viewConfigActions.getCardsConfig
})(App);

