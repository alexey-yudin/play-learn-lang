import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions/cards';
import MemoryGameContainer from '../../../common/components/memoryGame/MemoryGameContainer';

class MemoryGame extends Component {
  render() {
    return (
      <MemoryGameContainer
        cards={this.props.cards}
        fetchCards={this.props.fetchCards}
        cardsId={this.props.params.id}
        memoryGameSettings={this.props.memoryGameSettings}
        appName={'play-learn-lang'}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    cards: state.playLearnLang.selectedCards,
    memoryGameSettings: state.common.memoryGameSettings
  }
}

export default connect(mapStateToProps, {
  fetchCards: actions.fetchCards
})(MemoryGame);
