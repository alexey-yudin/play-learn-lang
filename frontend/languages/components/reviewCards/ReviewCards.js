import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/cards'
import CardsListItem from './CardsListItem';
import CardsList from '../../../common/components/reviewCards/CardsList';
import './styles.scss';

class ReviewCards extends Component {
  constructor(props) {
    super(props);
    this.renderCard = this.renderCard.bind(this);
  }

  renderCard(card, index) {
    return (
      <CardsListItem
        key={index}
        card={card}
      />
    );
  }

  render() {
    const { fetchCards, cards, params } = this.props;
    return (
      <CardsList
        fetchCards={fetchCards}
        cards={cards}
        cardsId={params.id}
        renderCard={this.renderCard}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    cards: state.playLearnLang.selectedCards
  }
}

export default connect(mapStateToProps, {
  fetchCards: actions.fetchCards
})(ReviewCards);
