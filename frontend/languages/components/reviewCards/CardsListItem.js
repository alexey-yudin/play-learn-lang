import React, { Component, PropTypes } from 'react';
import * as localityConstants from '../../../common/constants/localities';
import AudioPlayer from '../../../common/components/shared/generalAudio/AudioPlayer';
import RomanisedWithTranlsation
  from '../../../common/components/shared/romanization/RomanisationWithTranslation';
import LocalityImage from '../../../common/components/shared/language/LocalityImage';

class CardListItem extends Component {
  constructor(props) {
    super(props);
    this.playAudioForeign = this.playAudioForeign.bind(this);
    this.playAudioNative = this.playAudioNative.bind(this);
  }

  playAudioForeign() {
    const cardId = this.props.card.id;
    $(`#playLearnLang-card-first${cardId}`).trigger('play');
  }

  playAudioNative() {
    const cardId = this.props.card.id;
    $(`#playLearnLang-card-second${cardId}`).trigger('play');
  }

  render() {
    const { card } = this.props;
    return (
      <div className="col-xs-12 col-sm-6 col-md-4">
        <div className="review-card-item">
          <div className="locality-translation cursor-pointer" onClick={this.playAudioForeign}>
            <LocalityImage
              localityType={localityConstants.FOREIGN_LOCALITY}
              className={'locality-image'}
            />
            <RomanisedWithTranlsation
              translations={card.translations}
              localityType={localityConstants.FOREIGN_LOCALITY}
            />
            <AudioPlayer
              localityType={localityConstants.FOREIGN_LOCALITY}
              translations={card.translations}
              generalId={`playLearnLang-card-first`}
              cardId={card.id}
              autoPlay={false}
            />
          </div>
          <div>
            <img src={card.image.url_600} alt="" className='img-responsive'/>
          </div>
          <div className="locality-translation cursor-pointer" onClick={this.playAudioNative}>
            <LocalityImage
              localityType={localityConstants.NATIVE_LOCALITY}
              className={'locality-image'}
            />
            <RomanisedWithTranlsation
              localityType={localityConstants.NATIVE_LOCALITY}
              translations={card.translations}
            />
            <AudioPlayer
              localityType={localityConstants.NATIVE_LOCALITY}
              translations={card.translations}
              generalId={`playLearnLang-card-second`}
              cardId={card.id}
              autoPlay={false}
            />
          </div>
        </div>
      </div>
    );
  }
}

CardListItem.propTypes = {
  card: PropTypes.object.isRequired
};

export default CardListItem;