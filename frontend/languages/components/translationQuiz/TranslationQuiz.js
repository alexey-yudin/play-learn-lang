import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as constants from '../../../common/constants/quiz';
import * as actions from '../../actions/cards';
import QuizPage from '../../../common/components/quiz/QuizPage';

class TranslationQuiz extends Component {
  render() {
    return (
      <QuizPage
        cards={this.props.cards}
        fetchCards={this.props.fetchCards}
        appName={'playLearnLang'}
        cardsId={this.props.params.id}
        quizSettings={this.props.quizSettings}
        pageId={this.props.params.id}
        mode={constants.TRANSLATION_QUIZ_MODE}
        packConfig={{}}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    cards: state.playLearnLang.selectedCards,
    quizSettings: state.common.quizSettings
  }
}

export default connect(mapStateToProps, {
  fetchCards: actions.fetchCards
})(TranslationQuiz);
