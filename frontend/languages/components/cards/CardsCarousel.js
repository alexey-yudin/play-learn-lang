import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions/cards';
import CardsCarouselAbstract from '../../../common/components/carousel/CarouselContainer';

class CardsCarousel extends Component {
  render() {
    return (
      <CardsCarouselAbstract
        cards={this.props.cards}
        fetchCards={this.props.fetchCards}
        localities={this.props.localities}
        selectedLocality={this.props.selectedLocality}
        cardsId={this.props.params.id}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    localities: state.common.localities.localities,
    selectedLocality: state.common.localities.selectedLocality,
    cards: state.playLearnLang.selectedCards
  }
}

export default connect(mapStateToProps, {
  fetchCards: actions.fetchCards
})(CardsCarousel);
