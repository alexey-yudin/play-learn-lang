import * as constants from '../constants/cards';
import * as commonCardsTypes from '../../common/constants/reviewCards';

export default function(state = [], action) {
  switch(action.type) {
    case constants.FETCH_CARD:
      if (action.payload && action.payload.data) {
        return action.payload.data.cards;
      } else {
        return state;
      }

    case commonCardsTypes.CLEAR_REVIEW_CARDS_LIST:
      return [];

    default:
      return state;
  }
}
