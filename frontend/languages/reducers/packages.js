import * as constants from '../constants/packages';

export default function(state = [], action) {
  switch(action.type) {
    case constants.FETCH_PACKAGES:
      if (action.packages) {
        return action.packages;
      } else {
        return state;
      }

    default:
      return state;
  }
}
