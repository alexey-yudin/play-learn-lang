import { combineReducers } from 'redux';

import packages from './packages.js';
import selectedCards from './selectedCards.js';

const rootReducer = combineReducers({
  packages,
  selectedCards
});

export default rootReducer;
