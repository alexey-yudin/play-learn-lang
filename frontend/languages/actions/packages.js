import axios from 'axios';
import * as routeConstants from '../../common/constants/routes';
import * as constants from '../constants/packages';

export const fetchPackages = () => {
  return dispatch => {
    axios.get(`/api/package/getPackages/${routeConstants.languages}`)
      .then(response => {
        dispatch({
          type: constants.FETCH_PACKAGES,
          packages: response.data.packages
        });
      });
  }
}
