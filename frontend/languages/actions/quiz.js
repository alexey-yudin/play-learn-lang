import { SET_COUNT_OF_LEVELS, SET_COUNT_OF_CARDS } from '../constants/quiz.js';

export function setCountOfLevels(count) {
  return {
    type: SET_COUNT_OF_LEVELS,
    levelsCount: count
  }
}

export function setCountOfCards(count) {
  return {
    type: SET_COUNT_OF_CARDS,
    cardsCount: count
  }
}
