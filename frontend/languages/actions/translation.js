import axios from 'axios';
import * as constants from '../constants/translation';

export const fetchTranslations = () => {
  return dispatch => {
    axios.get('/api/v1/play-learn-lang/translations')
      .then(response => {
        dispatch({
          type: constants.FETCH_TRANSLATIONS,
          payload: response
        });
      });
  }
}

export const setTranslation = id => {
  return {
    type: constants.SET_TRANSLATION,
    id
  }
}
