import * as constants from '../constants/settings';

// Game audio settings
export function setAudioAnswersToTrue() {
  return {
    type: constants.SET_AUDIO_ANSWERS_TO_TRUE
  }
}

export function setAudioAnswersToFalse() {
  return {
    type: constants.SET_AUDIO_ANSWERS_TO_FALSE
  }
}

// Romanised settings
export function setRomanisedToTrue() {
  return {
    type: constants.SET_ROMANISED_TO_TRUE
  }
}

export function setRomanisedToFalse() {
  return {
    type: constants.SET_ROMANISED_TO_FALSE
  }
}

// General audio settings
export function setGeneralAudioToTrue() {
  return {
    type: constants.SET_GENERAL_AUDIO_TO_TRUE
  }
}

export function setGeneralAudioToFalse() {
  return {
    type: constants.SET_GENERAL_AUDIO_TO_FALSE
  }
}
