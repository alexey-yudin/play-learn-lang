import axios from 'axios';
import * as routeConstants from '../../common/constants/routes';
import * as constants from '../constants/cards.js';

export const fetchCards = id => {
  return dispatch => {
    axios.get(`/api/cards/getCards/${routeConstants.languages}/${id}`)
      .then(response => {
        dispatch({
          type: constants.FETCH_CARD,
          payload: response
        })
      });
  }
};
