import * as constants from '../constants/memoryGame';

export function setAmountOfCards(amount) {
  return {
    type: constants.SET_AMOUNT_OF_CARDS,
    amount
  }
}
