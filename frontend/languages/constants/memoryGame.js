export const DEFAULT_AMOUNT_OF_CARDS = '12';

export const SET_AMOUNT_OF_CARDS = 'SET_AMOUNT_OF_CARDS';

export const DEFAULT_VARIANTS_FOR = [
  {
    name: '3x2',
    amount: '6'
  }, {
    name: '4x3',
    amount: '12'
  }, {
    name: '5x4',
    amount: '20'
  }, {
    name: '6x5',
    amount: '30'
  }
];
