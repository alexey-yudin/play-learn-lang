import React from 'react';
import { Route, IndexRoute } from 'react-router';

import * as routeConstants from './common/constants/routes';

import App from './App';
import IndexPage from './common/components/index/IndexPage';

// Play learn lang routes
import LanguagesApp from './languages/components/App';
import LanguagesPackages from './languages/components/packages/PackagesList';
import LanguagesReviewCards from './languages/components/reviewCards/ReviewCards';
import LanguagesCardsCarousel from './languages/components/cards/CardsCarousel';
import LanguagesQuiz from './languages/components/quiz/Quiz';
import TranslationQuiz from './languages/components/translationQuiz/TranslationQuiz';
import LanguagesMemoryGame from './languages/components/memory_game/MemoryGame';
import BingoQuiz from './languages/components/bingoQuiz/BingoQuiz';
import ShuffledABC from './languages/components/shuffledABC/ShuffledABC';

// Khmer alphabet
import AlphakmApp from './alphakm/components/App';
import AlphakmPackages from './alphakm/components/packages/PackageList';
import AlphakmReviewCards from './alphakm/components/reviewCards/ReviewCards';
import AlphakmNestedPacks from './alphakm/components/packages/NestedPacksList';
import AlphakmQuiz from './alphakm/components/quiz/Quiz';
import khmerTranslationQuiz from './alphakm/components/translationQuiz/TranslationQuiz';
import KhmerMemoryGame from './alphakm/components/memoryGame/MemoryGame';
import KhmerCarousel from './alphakm/components/carousel/CardsCarousel';

import Auth from './common/components/auth/Auth';

export default (
  <Route path='/' component={App}>
    <IndexRoute component={IndexPage}/>

    <Route path={`/${routeConstants.languages}`} component={LanguagesApp}>
      <IndexRoute component={LanguagesPackages}/>
      <Route path={`package/:id/${routeConstants.reviewCards}`} component={LanguagesReviewCards}/>
      <Route path={`package/:id/${routeConstants.cardsCarousel}`} component={LanguagesCardsCarousel}/>
      <Route path={`package/:id/${routeConstants.quiz}`} component={LanguagesQuiz}/>
      <Route path={`package/:id/${routeConstants.translationQuiz}`} component={TranslationQuiz}/>
      <Route path={`package/:id/${routeConstants.memoryGame}`} component={LanguagesMemoryGame}/>
      <Route path={`package/:id/${routeConstants.bingoQuiz}`} component={BingoQuiz}/>
      <Route path={`package/:id/${routeConstants.shuffledAbc}`} component={ShuffledABC}/>
    </Route>

    <Route path={`/${routeConstants.alphakm}`} component={AlphakmApp}>
      <IndexRoute component={AlphakmPackages}/>
      <Route path={`package/:id/${routeConstants.reviewCards}`} component={AlphakmReviewCards}/>
      <Route path={`package-list/:id`} component={AlphakmNestedPacks}/>
      <Route path={`package/:id/${routeConstants.quiz}`} component={AlphakmQuiz}/>
      <Route path={`package/:id/${routeConstants.translationQuiz}`} component={khmerTranslationQuiz}/>
      <Route path={`package/:id/${routeConstants.memoryGame}`} component={KhmerMemoryGame}/>
      <Route path={`package/:id/${routeConstants.cardsCarousel}`} component={KhmerCarousel}/>
    </Route>
  </Route>
);
