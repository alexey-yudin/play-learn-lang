export const getAppName = state => state.common.generalSettings.meta.appName;
export const getBingoQuizSettings = state => state.common.bingoQuiz;
