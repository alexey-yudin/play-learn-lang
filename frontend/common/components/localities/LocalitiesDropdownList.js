import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/localities';
import * as constants from '../../constants/localities';
import LocalitiesDropdownItem from './LocalitiesDropdownItem';
import './localitiesDropdown.scss';

class LocalitiesDropdownList extends Component {
  constructor(props) {
    super(props);
    this.visibilityOptions = {
      visibleDropdown: 'visibleDropdown',
      hiddenDropdown: 'hiddenDropdown',
      hidden: 'hidden'
    };
  }

  componentWillReceiveProps(nextProps) {
    const { foreignLocalities, nativeLocalities, selectedLocality, appName } = nextProps;
    if (!selectedLocality.foreign && !selectedLocality.native && appName
        && foreignLocalities.length > 0 && nativeLocalities.length > 0) {
      this.props.setDefaultSelectedLocalities(appName);
    }
  }

  componentWillUnmount() {
    this.props.clearSelectedLocalities();
    this.props.clearLocalities();
  }

  getVisibility() {
    const { foreignLocalities, nativeLocalities } = this.props;
    if (foreignLocalities.length === 1 && nativeLocalities.length === 1) {
      return this.visibilityOptions.hidden;
      // const foreign = foreignLocalities[0];
      // const native = nativeLocalities[0];
      // if (foreign.id === native.id) {
      //   return this.visibilityOptions.hidden;
      // } else {
      //   return this.visibilityOptions.hiddenDropdown;
      // }
    } else {
      return this.visibilityOptions.visibleDropdown;
    }
  }

  render() {
    const { visibleDropdown, hiddenDropdown, hidden } = this.visibilityOptions;
    const visibility = this.getVisibility();
    if (visibility === visibleDropdown) {
      return (
        <div className="localities-dropdown">
          <LocalitiesDropdownItem
            localityType={constants.FOREIGN_LOCALITY}
            translationPath='localities.locality_1'
          />
          <LocalitiesDropdownItem
            localityType={constants.NATIVE_LOCALITY}
            translationPath='localities.locality_2'
          />
        </div>
      );
    } else if (visibility === hiddenDropdown) {
      return (
        <div className="localities-dropdown">
          <LocalitiesDropdownItem
            localityType={constants.FOREIGN_LOCALITY}
            translationPath='localities.locality_1'
            hiddenDropdown={true}
          />
          <LocalitiesDropdownItem
            localityType={constants.NATIVE_LOCALITY}
            translationPath='localities.locality_2'
            hiddenDropdown={true}
          />
        </div>
      );
    } else if (visibility === hidden) {
      return null;
    }
  }
}

const mapStateToProps = state => {
  return {
    foreignLocalities: state.common.localities.localities.foreign,
    nativeLocalities: state.common.localities.localities.native,
    selectedLocality: state.common.localities.selectedLocality,
    appName: state.common.generalSettings.meta.appName
  }
}

export default connect(mapStateToProps, {
  setDefaultSelectedLocalities: actions.setDefaultSelectedLocalities,
  clearSelectedLocalities: actions.clearSelectedLocalities,
  clearLocalities: actions.clearLocalities
})(LocalitiesDropdownList);
