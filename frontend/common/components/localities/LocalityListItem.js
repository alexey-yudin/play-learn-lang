import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/localities';
import * as constants from '../../constants/localities';

class LocalityListItem extends Component {
  constructor(props) {
    super(props);
    this.setLocality = this.setLocality.bind(this);
  }

  getSelected() {
    const { locality, selectedLocality, localityType } = this.props;
    if (localityType === constants.FOREIGN_LOCALITY) {
      return locality.id === selectedLocality.foreign ? 'selected' : '';
    } else if (localityType === constants.NATIVE_LOCALITY) {
      return locality.id === selectedLocality.native ? 'selected' : '';
    }
  }

  setLocality() {
    const { locality, localityType, appName } = this.props;
    if (localityType === constants.FOREIGN_LOCALITY) {
      this.props.setForeignLocality(locality.id, appName);
    } else if (localityType === constants.NATIVE_LOCALITY) {
      this.props.setNativeLocality(locality.id, appName);
    }
  }

  render() {
    const { locality } = this.props;
    return (
      <div className={`localities-list__item ${this.getSelected()}`} onClick={this.setLocality}>
        <img src={locality.imageUrl}/>
        {locality.label}
      </div>
    );
  }
}

LocalityListItem.propTypes = {
  locality: PropTypes.object.isRequired,
  localityType: PropTypes.string.isRequired
};

const mapStateToProps = state => {
  return {
    selectedLocality: state.common.localities.selectedLocality,
    appName: state.common.generalSettings.meta.appName
  }
};

export default connect(mapStateToProps, {
  setForeignLocality: actions.setForeignLocality,
  setNativeLocality: actions.setNativeLocality
})(LocalityListItem);
