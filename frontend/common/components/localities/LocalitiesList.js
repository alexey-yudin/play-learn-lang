import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import * as constants from '../../constants/localities';
import * as actions from '../../actions/localities';
import LocalityListItem from './LocalityListItem.js';

class LocalitiesList extends Component {
  constructor(props) {
    super(props);
    this.renderLocality = this.renderLocality.bind(this);
  }

  renderDisableLocality() {
    const { selectedLocality, localityType } = this.props;
    if (localityType === constants.NATIVE_LOCALITY) {
      return (
        <div
          onClick={this.props.disableNativeLocality}
          className={`localities-list__item ${!selectedLocality.native ? 'selected' : ''}`}>
          <i className="glyphicon glyphicon-remove"></i>
          Disable
        </div>
      );
    }
  }

  renderLocality(locality) {
    const { localityType } = this.props;
    return (
      <LocalityListItem
        key={locality.id}
        locality={locality}
        localityType={localityType}
      />
    );
  }

  renderLocalities() {
    const { localityType } = this.props;
    if (localityType === constants.FOREIGN_LOCALITY) {
      return (
        <div className="localities-list">
          {this.props.foreignLocalities.map(this.renderLocality)}
        </div>
      );
    } else if (localityType === constants.NATIVE_LOCALITY) {
      return (
        <div className="localities-list">
          {this.renderDisableLocality()}
          {this.props.nativeLocalities.map(this.renderLocality)}
        </div>
      );
    }
  }

  render() {
    return (
      <div className="localities-list-container">
        {this.renderLocalities()}
      </div>
    );
  }
}

LocalitiesList.propTypes = {
  localityType: PropTypes.string.isRequired
};

const mapStateToProps = state => {
  return {
    foreignLocalities: state.common.localities.localities.foreign,
    nativeLocalities: state.common.localities.localities.native,
    selectedLocality: state.common.localities.selectedLocality
  }
};

export default connect(mapStateToProps, {
  setDefaultLocalities: actions.setDefaultLocalities,
  disableNativeLocality: actions.disableNativeLocality
})(LocalitiesList);
