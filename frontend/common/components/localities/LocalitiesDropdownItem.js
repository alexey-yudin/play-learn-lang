import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import LocalitiesList from './LocalitiesList';
import LocalityImage from '../shared/language/LocalityImage';
import * as localityConstants from '../../constants/localities';

class LocalitiesDropdownItem extends Component {
  constructor(props) {
    super(props);
    this.toggleDropdown = this.toggleDropdown.bind(this);
  }

  componentDidMount() {
    this.closeOnOutsideClick();
  }

  toggleDropdown() {
    $(this.refs.dropdown).toggleClass('open');
  }

  closeDropdown() {
    $(this.refs.dropdown).removeClass('open');
  }

  closeOnOutsideClick() {
    $(document).on('click', () => this.closeDropdown());
  }

  getLocalityName() {
    const { localityType, selectedLocality, localities } = this.props;
    if (localityType === localityConstants.FOREIGN_LOCALITY) {
      const locality = localities.foreign.find(localityItem => {
        return localityItem.id === selectedLocality.foreign;
      });
      if (!locality) return;
      return locality.label;
    } else if (localityType === localityConstants.NATIVE_LOCALITY) {
      const locality = localities.native.find(localityItem => {
        return localityItem.id === selectedLocality.native;
      });
      if (!locality) return;
      return locality.label;
    }
  }

  renderItemInner() {
    const { localityType } = this.props;
    if (localityType === localityConstants.FOREIGN_LOCALITY) {
      return (
        <div>
          <div className='locality-left'>
            <LocalityImage localityType={localityType}/>
          </div>
          {this.getLocalityName()}
        </div>
      );
    } else if (localityType === localityConstants.NATIVE_LOCALITY) {
      return (
        <div>
          {this.getLocalityName()}
          <div className='locality-right'>
            <LocalityImage localityType={localityType}/>
          </div>
        </div>
      );
    }
  }

  render() {
    const { localityType, hiddenDropdown } = this.props;
    if (hiddenDropdown) {
      return (
        <div ref="dropdown" className="localities-dropdown-item dropdown-toggle">
          {this.renderItemInner()}
        </div>
      );
    } else {
      return (
        <div className="dropdown" ref="dropdown">
          <div className="localities-dropdown-item dropdown-toggle cursor-pointer"
             onClick={this.toggleDropdown}>
            {this.renderItemInner()}
          </div>
          <ul className='dropdown-menu'
            style={{overflow: 'hidden', padding: 0}}>
            <LocalitiesList localityType={localityType}/>
          </ul>
        </div>
      );
    }
  }
}

LocalitiesDropdownItem.propTypes = {
  localityType: PropTypes.string.isRequired,
  translationPath: PropTypes.string.isRequired,
  hiddenDropdown: PropTypes.bool
};

const mapStateToProps = state => {
  return {
    localities: state.common.localities.localities,
    selectedLocality: state.common.localities.selectedLocality
  }
};

export default connect(mapStateToProps)(LocalitiesDropdownItem);
