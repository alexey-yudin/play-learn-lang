import React, { Component, PropTypes } from 'react';
import RomanisationWithTranslation from '../shared/romanization/RomanisationWithTranslation';
import AudioAnswersContainer from '../shared/game_audio/AudioAnswersContainer';
import * as localityConstants from '../../constants/localities';

class BingoQuizListItem extends Component {
  constructor(props) {
    super(props);
    this.messages = null;
    this.messagesAudio = null;
    this.checkIfIsCorrect = this.checkIfIsCorrect.bind(this);
  }

  checkIfIsCorrect() {
    const { card, correctCardId, index } = this.props;
    if (card.id === correctCardId) {
      this.props.addToChecked(index)
      this.messagesAudio.playCorrectAnswer();
    } else {
      $(this.messages).find('.remove').show();
      this.messagesAudio.playWrongAnswer();
      setTimeout(() => {
        $(this.messages).find('.remove').hide();
      }, 300);
    }
  }

  renderItem() {
    const { card, isChecked } = this.props;
    if (isChecked) {
      return (
        <div className="bingo-quiz-list__item bingo-quiz-list__item-checked">
          <RomanisationWithTranslation
            translations={card.translations}
            localityType={localityConstants.FOREIGN_LOCALITY}
          />
          <div className="messages">
            <div className="glyphicon glyphicon-star star" style={{display: 'block'}}></div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="bingo-quiz-list__item" onClick={this.checkIfIsCorrect}>
          <RomanisationWithTranslation
            translations={card.translations}
            localityType={localityConstants.FOREIGN_LOCALITY}
          />
          <div className="messages" ref={item => this.messages = item}>
            <div className="glyphicon glyphicon-remove remove"></div>
          </div>
          <AudioAnswersContainer ref={item => this.messagesAudio = item}/>
        </div>
      );
    }
  }

  render() {
    const { index } = this.props;
    return this.renderItem();
  }
}

BingoQuizListItem.propTypes = {
  card: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  correctCardId: PropTypes.string.isRequired,
  addToChecked: PropTypes.func.isRequired,
  isChecked: PropTypes.bool.isRequired
};

export default BingoQuizListItem;
