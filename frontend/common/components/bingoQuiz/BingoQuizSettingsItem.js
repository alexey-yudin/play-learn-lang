import React, { Component, PropTypes } from 'react';

class BingoQuizSettingsItem extends Component {
  setGridSize() {
    const { buttonData, appName } = this.props;
    this.props.setGridSize(buttonData.value, appName);
  }

  isActive() {
    const { buttonData, bingoQuizValue } = this.props;
    return buttonData.value === bingoQuizValue;
  }

  render() {
    const { buttonData } = this.props;
    return (
      <div className="settings-button-item" onClick={this.setGridSize.bind(this)}>
        <div className={`settings-button` }>
          <div className={`${this.isActive() ? 'settings-button-selected' : ''}`}>
          </div>
        </div>
        <div>
          {buttonData.label}
        </div>
      </div>
    );
  }
}

BingoQuizSettingsItem.propTypes = {
  buttonData: PropTypes.object.isRequired,
  bingoQuizValue: PropTypes.number.isRequired,
  appName: PropTypes.string.isRequired,
  setGridSize: PropTypes.func.isRequired
};

export default BingoQuizSettingsItem;
