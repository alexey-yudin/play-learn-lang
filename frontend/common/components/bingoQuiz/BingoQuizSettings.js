import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as selectors from '../../selectors';
import * as constants from '../../constants/bingoQuiz';
import * as actions from '../../actions/bingoQuiz';
import BingoQuizSettingsItem from './BingoQuizSettingsItem';

class BingoQuizSettings extends Component {
  renderButton(buttonData, index) {
    const { appName, bingoQuizSettings, setGridSize } = this.props;
    return (
      <BingoQuizSettingsItem
        key={index}
        buttonData={buttonData}
        appName={appName}
        bingoQuizValue={bingoQuizSettings.value}
        setGridSize={setGridSize}
      />
    );
  }

  render() {
    return (
      <div className="settings-buttons-container">
        Size: {constants.GRID_SIZES.map(this.renderButton.bind(this))}
      </div>
    );
  }
}

export default connect(state => ({
  appName: selectors.getAppName(state),
  bingoQuizSettings: selectors.getBingoQuizSettings(state)
}), {
  setGridSize: actions.setGridSize
})(BingoQuizSettings);
