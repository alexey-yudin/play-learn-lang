import React, { Component, PropTypes } from 'react';
import Loader from '../shared/loader/Loader';
import BingoQuizList from './BingoQuizList';
import ModesContainer from '../modes/ModesContainer';
import './styles.scss';

class BingoQuizContainer extends Component {
  componentDidMount() {
    this.refs.loader.runOnImageLoad();
  }

  render() {
    const { cards, settings, paramsId } = this.props;
    return (
      <div>
        <Loader ref="loader"/>
        <div className="row">
          <div className="col-sm-12 col-md-10 col-md-offset-1">
            <ModesContainer
              title={'Bingo'}
              imageName={''}
              imageHoverName={''}
              paramsId={paramsId}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-10 col-md-offset-1">
            <BingoQuizList
              cards={cards}
              settings={settings}
            />
          </div>
        </div>
      </div>
    );
  }
}

BingoQuizContainer.propTypes = {
  cards: PropTypes.array.isRequired,
  settings: PropTypes.object.isRequired,
  paramsId: PropTypes.string.isRequired
};

export default BingoQuizContainer;
