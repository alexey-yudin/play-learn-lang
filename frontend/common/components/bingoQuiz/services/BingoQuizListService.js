export default class BingoQuizService {
  static getActiveCardsIndexes(cardsForStage) {
    return cardsForStage.map((card, index) => index);
  }

  static getCorrectCard(activeCardsIndexes, cardsForStage) {
    const correctCardIndex = activeCardsIndexes[_.random(0, activeCardsIndexes.length - 1)];
    return cardsForStage[correctCardIndex];
  }

  static getActiveCardsIndexes(cardsForStage) {
    return cardsForStage.map((card, index) => index);
  }

  static getActiveCardsWithoutChecked(activeCardsIndexes, checkedCardIndex) {
    return activeCardsIndexes.filter(cardIndex => cardIndex !== checkedCardIndex);
  }
}
