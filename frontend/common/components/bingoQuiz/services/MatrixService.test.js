import MatrixService from './MatrixService';
import MatrixTestHelper from './MatrixServiceTestHelper';
import chai from 'chai';
const expect = chai.expect;

describe('MatrixService', () => {
  describe('#extractMatrix', () => {
    it('should return new matrix with cardIndex and isChecked field in items', () => {
      const matrix = [[0, 1], [2, 3]];
      const checkedIndexes = [1, 3];
      const expectedResult = [[{
        cardIndex: 0,
        isChecked: false,
      }, {
        cardIndex: 1,
        isChecked: true
      }], [{
        cardIndex: 2,
        isChecked: false
      }, {
        cardIndex: 3,
        isChecked: true
      }]];
      expect(MatrixService.extractMatrix(matrix, checkedIndexes)).to.eql(expectedResult);
    });
  });

  describe('#isMatrixColumnChecked', () => {
    it('should return true if the column is checked', () => {
      const matrix = MatrixTestHelper.getExtractedMatrixColumns();
      const isChecked = MatrixService.isMatrixColumnChecked(matrix);
      expect(isChecked).to.equal(true);
    });

    it('should return false if the column is not checked', () => {
      const matrix = MatrixTestHelper.getExtractedMatrix(false);
      const isChecked = MatrixService.isMatrixColumnChecked(matrix);
      expect(isChecked).to.equal(false);
    });
  });

  describe('#isMatrixRowChecked', () => {
    it('should return true if one of rows is checked', () => {
      const matrix = MatrixTestHelper.getExtractedMatrix();
      expect(MatrixService.isMatrixRowChecked(matrix)).to.equal(true);
    });

    it('should return false if none of rows is checked', () => {
      const matrix = MatrixTestHelper.getExtractedMatrix(false);
      expect(MatrixService.isMatrixRowChecked(matrix)).to.equal(false);
    });

    it('should return false if elements is checked but not the row', () => {
      const matrix = MatrixTestHelper.getExtractedMatrixWithChecked();
      expect(MatrixService.isMatrixRowChecked(matrix)).to.equal(false);
    });
  });

  describe('#isMatrixDiagonalChecked', () => {
    it('should return true if the straight diagonal is checked', () => {
      const matrix = MatrixTestHelper.getExtractedMatrixDiagonals(5);
      const isChecked = MatrixService.isMatrixDiagonalChecked(matrix);
      expect(isChecked).to.equal(true);
    });

    it('should return false if the straight diagonal is not checked', () => {
      const matrix = MatrixTestHelper.getExtractedMatrix(false);
      const isChecked = MatrixService.isMatrixDiagonalChecked(matrix);
      expect(isChecked).to.equal(false);
    });
  });

  describe('#isMatrixReverseDiagonalChecked', () => {
    it('should return true if the reversed diagonal is checked', () => {
      const matrix = MatrixTestHelper.getExtractedMatrixDiagonalsReverse(5);
      const isChecked = MatrixService.isMatrixReverseDiagonalChecked(matrix);
      expect(isChecked).to.equal(true);
    });

    it('should return false if the reversed diagonal is not checked', () => {
      const matrix = MatrixTestHelper.getExtractedMatrix(false);
      const isChecked = MatrixService.isMatrixReverseDiagonalChecked(matrix);
      expect(isChecked).to.equal(false);
    });
  });

  describe('#isMatrixChecked', () => {
    it('should return true if row is checked', () => {
      const matrix = MatrixTestHelper.getExtractedMatrix();
      const isChecked = MatrixService.isMatrixChecked(matrix);
      expect(isChecked).to.equal(true);
    });

    it('should return true if column is checked', () => {
      const matrix = MatrixTestHelper.getExtractedMatrixColumns();
      const isChecked = MatrixService.isMatrixChecked(matrix);
      expect(isChecked).to.equal(true);
    });

    it('should return true if straight diagonal is checked', () => {
      const matrix = MatrixTestHelper.getExtractedMatrixDiagonals(3);
      const isChecked = MatrixService.isMatrixChecked(matrix);
      expect(isChecked).to.equal(true);
    });

    it('should return true if reversed diagonal is checked', () => {
      const matrix = MatrixTestHelper.getExtractedMatrixDiagonalsReverse(3);
      const isChecked = MatrixService.isMatrixChecked(matrix);
      expect(isChecked).to.equal(true);
    });

    it('should return false if nothing is checked', () => {
      const matrix = MatrixTestHelper.getExtractedMatrix(false);
      const isChecked = MatrixService.isMatrixChecked(matrix);
      expect(isChecked).to.equal(false);
    });
  });

  describe('#arrayToMatrix', () => {
    it('should return matrix', () => {
      const array = [1, 2, 3, 4, 5, 6, 7, 8, 9];
      const matrix = MatrixService.arrayToMatrix(array, 3);
      expect(matrix).to.be.an('array');
    });

    it('should return matrix with selected rows', () => {
      const array1 = [1, 2, 3, 4, 5, 6, 7, 8, 9];
      const matrix1 = MatrixService.arrayToMatrix(array1, 3);
      expect(matrix1.length).to.equal(array1.length / 3);
      const array2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
      const matrix2 = MatrixService.arrayToMatrix(array2, 2);
      expect(matrix2.length).to.equal(array2.length / 2);
    });

    it('should return matrix with values from array', () => {
      const array = [1, 2, 3, 4, 5, 6, 7, 8, 9];
      const matrix = MatrixService.arrayToMatrix(array, 3);
      const expectedResult = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
      expect(matrix).to.eql(expectedResult);
    });
  });
});
