export default class MatrixTestHelper {
  static getExtractedMatrix(includeChecked = true) {
    const matrix = [];
    let row = [];
    for (let i = 0; i < 9; i++) {
      if (row.length < 3) {
        if (i < 3) {
          row.push({ cardIndex: i, isChecked: includeChecked });
        } else {
          row.push({ cardIndex: i, isChecked: false });
        }
      }
      if (row.length === 3) {
        matrix.push(row);
        row = [];
      }
    }

    return matrix;
  }

  static getExtractedMatrixWithChecked() {
    const matrix = [];
    let row = [];
    for (let i = 0; i < 9; i++) {
      if (row.length < 3) {
        if (i === 0 || i === 2 || i === 4) {
          row.push({ cardIndex: i, isChecked: true });
        } else {
          row.push({ cardIndex: i, isChecked: false });
        }
      }
      if (row.length === 3) {
        matrix.push(row);
        row = [];
      }
    }

    return matrix;
  }

  static getExtractedMatrixColumns() {
    const matrix = [];
    let row = [];
    for (let i = 0; i < 9; i++) {
      if (row.length < 3) {
        if (i === 0 || i === 3 || i === 6) {
          row.push({ cardIndex: i, isChecked: true });
        } else {
          row.push({ cardIndex: i, isChecked: false });
        }
      }
      if (row.length === 3) {
        matrix.push(row);
        row = [];
      }
    }

    return matrix;
  }

  static getExtractedMatrixDiagonals(rowLength = 3) {
    const matrix = [];
    let row = [];
    let diagonalIndex = 0;
    const gridSize = rowLength * rowLength;
    for (let i = 0; i < gridSize; i++) {
      if (row.length < rowLength) {
        if (i === diagonalIndex) {
          row.push({ cardIndex: i, isChecked: true });
        } else {
          row.push({ cardIndex: i, isChecked: false });
        }
      }
      if (row.length === rowLength) {
        matrix.push(row);
        row = [];
        diagonalIndex += rowLength + 1;
      }
    }

    return matrix;
  }

  static getExtractedMatrixDiagonalsReverse(rowLength = 3) {
    const matrix = [];
    let row = [];
    let diagonalIndex = rowLength - 1;
    const gridSize = rowLength * rowLength
    for (let i = 0; i < gridSize; i++) {
      if (i === diagonalIndex) {
        row.push({ cardIndex: i, isChecked: true });
      } else {
        row.push({ cardIndex: i, isChecked: false });
      }
      if (row.length === rowLength) {
        matrix.push(row);
        row = [];
        diagonalIndex += rowLength - 1;
      }
    }

    return matrix;
  }
}
