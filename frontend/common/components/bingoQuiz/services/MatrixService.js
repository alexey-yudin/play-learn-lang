export default class MatrixService {
  static isMatrixChecked(extractedMatrixRows) {
    return MatrixService.isMatrixRowChecked(extractedMatrixRows) ||
      MatrixService.isMatrixColumnChecked(extractedMatrixRows) ||
      MatrixService.isMatrixDiagonalChecked(extractedMatrixRows) ||
      MatrixService.isMatrixReverseDiagonalChecked(extractedMatrixRows);
  }

  static isMatrixDiagonalChecked(extractedMatrixRows) {
    if (extractedMatrixRows.length === 0) return;
    const rowLength = extractedMatrixRows[0].length;
    let count = 0;
    let diagonalIndex = 0;
    extractedMatrixRows.forEach(row => {
      row.forEach(cardItem => {
        if (cardItem.cardIndex === diagonalIndex) {
          if (cardItem.isChecked) count += 1;
        }
      });
      diagonalIndex += rowLength + 1;
    });

    return count === extractedMatrixRows.length;
  }

  static isMatrixReverseDiagonalChecked(extractedMatrixRows) {
    if (extractedMatrixRows.length === 0) return;
    const rowLength = extractedMatrixRows[0].length;
    let count = 0;
    let diagonalIndex = rowLength - 1;
    extractedMatrixRows.forEach(row => {
      row.forEach(cardItem => {
        if (cardItem.cardIndex === diagonalIndex) {
          if (cardItem.isChecked) count += 1;
        }
      });
      diagonalIndex += rowLength - 1;
    });

    return count === extractedMatrixRows.length;
  }

  static isMatrixColumnChecked(extractedMatrixRows) {
    if (extractedMatrixRows.length === 0) return;
    const columns = [];
    extractedMatrixRows.forEach((row, index) => {
      const column = MatrixService.getColumn(extractedMatrixRows, index);
      columns.push(column);
    });

    return MatrixService.isMatrixRowChecked(columns);
  }

  static getColumn(extractedMatrixRows, index) {
    if (extractedMatrixRows.length === 0) return;
    const rowLength = extractedMatrixRows[0].length;
    const column = [];
    extractedMatrixRows.forEach(row => {
      row.forEach((cardItem, cardIndex) => {
        if (cardIndex === index) column.push(cardItem);
      });
    })

    return column;
  }

  static isMatrixRowChecked(extractedMatrixRows) {
    if (extractedMatrixRows.length === 0) return;
    const rowLength = extractedMatrixRows[0].length;
    let count = 0;
    let isChecked = false;
    extractedMatrixRows.forEach(row => {
      row.forEach(item => {
        if (item.isChecked) count += 1;
      });
      if (count === rowLength) isChecked = true;
      else count = 0;
    });

    return isChecked;
  }

  static arrayToMatrix(array, rowLength) {
    const matrix = [];
    let row = [];
    array.forEach((item, index) => {
      if (row.length < rowLength) row.push(item);
      if (row.length === rowLength) {
        matrix.push(row);
        row = [];
      }
    });

    return matrix;
  }

  static extractMatrix(matrix, checkedIndexes) {
    if (!matrix) return;
    return matrix.map(row => {
      return row.map(cardIndex => {
        const isChecked = checkedIndexes.includes(cardIndex);
        return { cardIndex, isChecked };
      });
    });
  }
}
