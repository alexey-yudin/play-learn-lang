import React, { Component, PropTypes } from 'react';
import BingoQuizListItem from './BingoQuizListItem';
import BingoQuizService from './services/BingoQuizListService';
import MatrixService from './services/MatrixService';
import BingoQuizMenu from './BingoQuizMenu';
import * as bingoConstants from '../../constants/bingoQuiz';

class BingoQuizList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardsForStage: [],
      checkedCardsIndexes: [],
      activeCardsIndexes: [],
      correctCard: null,
      matrix: null,
      isWinner: false
    };
    this.addToChecked = this.addToChecked.bind(this);
    this.renderListItem = this.renderListItem.bind(this);
    this.restartGame = this.restartGame.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.settings.value !== this.props.settings.value) {
      this.restartGame();
    }
  }

  componentDidUpdate() {
    const { cardsForStage, activeCardsIndexes, correctCard } = this.state;
    if (cardsForStage.length === 0 && this.props.cards.length > 0) {
      this.setCardsForStage();
    }
    if (cardsForStage.length > 0 && !correctCard) {
      const correctCard = BingoQuizService.getCorrectCard(activeCardsIndexes, cardsForStage);
      this.setState({ correctCard });
    }
    if (this.state.isWinner) {
      $('.bingo-message').fadeIn(400);
      setTimeout(() => {
        $('.bingo-message').fadeOut(400);
        setTimeout(() => {
          this.restartGame();
        }, 400);
      }, 1500);
    }
  }

  restartGame() {
    this.setState({
      cardsForStage: [],
      rowsForStage: [],
      checkedCardsIndexes: [],
      activeCardsIndexes: [],
      correctCard: null,
      matrix: null,
      isWinner: false
    });
  }

  setCardsForStage() {
    const { cards, settings } = this.props;
    if (cards.length === 0) return;
    const cardsForStage = [];
    while (cardsForStage.length < settings.value) {
      const randomCard = cards[_.random(0, cards.length - 1)];
      cardsForStage.push(randomCard);
    }
    const activeCardsIndexes = BingoQuizService.getActiveCardsIndexes(cardsForStage);
    const rowLength = this.getRowLength();
    const matrix = MatrixService.arrayToMatrix(activeCardsIndexes, rowLength);
    this.setState({ cardsForStage, activeCardsIndexes, matrix });
  }

  getRowLength() {
    const { settings } = this.props;
    const sizeItem = bingoConstants.GRID_SIZES.find(size => size.value === settings.value);
    return sizeItem.rowLength;
  }

  addToChecked(checkedCardIndex) {
    const { activeCardsIndexes, cardsForStage, matrix } = this.state;
    const checkedCardsIndexes = this.state.checkedCardsIndexes.concat(checkedCardIndex);
    const updatedActiveCardsIndexes = BingoQuizService.getActiveCardsWithoutChecked(activeCardsIndexes, checkedCardIndex);
    const correctCard = BingoQuizService.getCorrectCard(updatedActiveCardsIndexes, cardsForStage);
    const extractedMatrix = MatrixService.extractMatrix(matrix, checkedCardsIndexes);
    const isWinner = MatrixService.isMatrixChecked(extractedMatrix);
    this.setState({ checkedCardsIndexes, correctCard, activeCardsIndexes: updatedActiveCardsIndexes, isWinner });
  }

  isWinner() {
    const { matrix, checkedCardsIndexes } = this.state;
    if (!matrix) return;
    const extractedMatrix = MatrixService.extractMatrix(matrix, checkedCardsIndexes);
    return MatrixService.isMatrixChecked(extractedMatrix);
  }

  isChecked(index) {
    return this.state.checkedCardsIndexes.some(cardIndex => cardIndex === index);
  }

  getGridSize() {
    const { settings } = this.props;
    if (settings.value === 9) {
      return 'bingo-quiz-list-9';
    } else if (settings.value === 16) {
      return 'bingo-quiz-list-16';
    } else if (settings.value === 25) {
      return 'bingo-quiz-list-25'
    }
  }

  renderListItem(card, index) {
    const { correctCard } = this.state;
    return (
      <BingoQuizListItem
        key={index}
        index={index}
        card={card}
        correctCardId={correctCard.id}
        isChecked={this.isChecked(index)}
        addToChecked={this.addToChecked}
      />
    );
  }

  render() {
    const { cardsForStage, correctCard, isWinner } = this.state;
    if (!correctCard) return null;
    return (
      <div className="bingo-quiz-container">
        <div className="bingo-quiz-subcontainer">
          <BingoQuizMenu
            correctCard={correctCard}
            restartGame={this.restartGame}
            isWinner={isWinner}
          />
          <div className={`bingo-quiz-list ${this.getGridSize()}`}>
            {cardsForStage.map(this.renderListItem)}
          </div>
        </div>
        <div className="bingo-message">
          bingo
        </div>
      </div>
    );
  }
}

BingoQuizList.propTypes = {
  cards: PropTypes.array.isRequired,
  settings: PropTypes.object.isRequired
};

export default BingoQuizList;
