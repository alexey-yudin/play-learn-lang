import React, { Component, PropTypes } from 'react';
import * as localityConstants from '../../constants/localities';
import PlayAudio from '../shared/helpers/PlayAudio';
import LocalityImage from '../shared/language/LocalityImage';
import AudioPlayer from '../shared/generalAudio/AudioPlayer';

class BingoQuizMenu extends Component {
  constructor(props) {
    super(props);
    this.playAudio = this.playAudio.bind(this);
    this.audioPlayer = new PlayAudio()
  }

  componentDidMount() {
    this.playAudio();
  }

  componentDidUpdate() {
    if (!this.props.isWinner) {
      this.playAudio();
    }
  }

  playAudio() {
    const { correctCard } = this.props;
    this.audioPlayer.playAudio(`bingo-quiz-game-correct-word${correctCard.id}`);
  }

  render() {
    const { correctCard, restartGame } = this.props;
    return (
      <div className="bingo-quiz-list__menu">
        <div className="correct-word">
          <div>
            <LocalityImage
              localityType={localityConstants.FOREIGN_LOCALITY}
              className={'locality-image'}
            />
          </div>
          <div onClick={this.playAudio} className="image-voice">
            <img src="/resources/icons/voice-icon.png" alt=""/>
            <AudioPlayer
              translations={correctCard.translations}
              localityType={localityConstants.FOREIGN_LOCALITY}
              generalId='bingo-quiz-game-correct-word'
              cardId={correctCard.id}
              autoPlay={false}
            />
          </div>
        </div>
        <div>
          <button type='button' onClick={restartGame} className='btn btn-default'>
            <i className="glyphicon glyphicon-repeat"></i>
          </button>
        </div>
      </div>
    );
  }
}

BingoQuizMenu.propTypes = {
  correctCard: PropTypes.object.isRequired,
  restartGame: PropTypes.func.isRequired,
  isWinner: PropTypes.bool.isRequired
};

export default BingoQuizMenu;
