import React, { Component } from 'react';
import { Link } from 'react-router';
import * as routeConstants from '../../constants/routes';

class AppsList extends Component {
  render() {
    return (
      <div className="index-page-gradient">
        <div className='container'>
          <div className='index-apps-list'>
            <div>
              <figure>
                <Link to={`/${routeConstants.alphakm}`}>
                  <img src="/assets/khmer-alphabet.png" className='img-app-label' alt="khmer-alphabet"/>
                </Link>
                <figcaption className='app-description'>Khmer Alphabet</figcaption>
              </figure>
            </div>
            <div>
              <figure>
                <Link to={`/${routeConstants.languages}`}>
                  <img src="/assets/learn-lang.png" className='img-app-label' alt="play-learn-lang"/>
                </Link>
                <figcaption className='app-description'>Learn Languages</figcaption>
              </figure>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AppsList;
