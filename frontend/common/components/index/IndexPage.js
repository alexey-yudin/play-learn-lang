import React, { Component } from 'react';
import AppsList from './AppsList';
import Auth from '../auth/Auth';
import {getIsAuthenticated} from '../../selectors/auth';
import {connect} from 'react-redux';
import './styles.scss';

class IndexPage extends Component {
  render() {
    const {isAuthenticated} = this.props;

    if (isAuthenticated) {
      return (
        <AppsList/>
      );
    } else {
      return (
        <Auth/>
      );
    }
  }
}

const mapStateToProps = state => ({
  isAuthenticated: getIsAuthenticated(state)
});

export default connect(mapStateToProps)(IndexPage);
