import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import * as localityConstants from '../../constants/localities';
import LanguageTranslation from '../shared/language/LanguageTranslation';
import CardImageHelper from '../shared/helpers/CardImageHelper';
import LocalityImage from '../shared/language/LocalityImage';
import CardListItemNotes from './CardsListItemNotes';

class CardsListItem extends Component {
  constructor(props) {
    super(props);
    this.setCardIndex = this.setCardIndex.bind(this);
  }

  setCardIndex() {
    const { index } = this.props;
    this.props.setCurrentCardIndex(index);
  }

  getFontSizeStyle() {
    const { selectedLocality } = this.props;
    if (selectedLocality.foreign === 'km') {
      return 'font-size-40px';
    } else {
      return 'font-size-15px';
    }
  }

  renderCardBody() {
    const { card } = this.props;
    const cardImageHelper = new CardImageHelper;
    if (card['cfg-note_char'] === 'true') {
      return (
        <CardListItemNotes
          card={card}
        />
      );
    } else {
      if (cardImageHelper.cardHasImage(card)) {
        return (
          <img src={card.image.url_600} alt=""/>
        );
      } else {
        return (
          <div>
            <LanguageTranslation
              translations={card.translations}
              localityType={localityConstants.NATIVE_LOCALITY}
              className={this.getFontSizeStyle()}
            />
          </div>
        );
      }
    }
  }

  render() {
    const { index, currentCardIndex } = this.props;
    return (
      <div onClick={this.setCardIndex}
         className={`carousel-card-list-item ${index === currentCardIndex ? 'selected' : ''}`}>
        {this.renderCardBody()}
      </div>
    );
  }
}

CardsListItem.propTypes = {
  card: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  currentCardIndex: PropTypes.number.isRequired,
  setCurrentCardIndex: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  return {
    selectedLocality: state.common.localities.selectedLocality
  }
};

export default connect(mapStateToProps)(CardsListItem);
