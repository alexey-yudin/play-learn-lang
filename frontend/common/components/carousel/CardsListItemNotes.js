import React, { Component, PropTypes } from 'react';

class CardsListItemNotes extends Component {
  getParsedXml(xmlString) {
    if (window.DOMParser) {
      const parser = new DOMParser();
      return parser.parseFromString(xmlString, 'text/xml');
    } else {
      const xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
      xmlDoc.async = false;
      return xmlDoc.loadXML(xmlString);
    }
  }

  getDefinitionLongInXml() {
    const { card } = this.props;
    const definitionsLong = card.translations[1].definitions_long.translation;
    const xmlText = `<xml>${definitionsLong}</xml>`;
    return this.getParsedXml(xmlText);
  }

  getDefinitionShort() {
    const { card } = this.props;
    const definitionsShort = card.translations[1].definitions_short.translation;
    return definitionsShort;
  }

  getFirstWordsOfDefinitionLong() {
    const definitionsLongInXml = this.getDefinitionLongInXml();
    const elements = definitionsLongInXml.getElementsByTagName('xml')[0].childNodes;
    let definitionsLong = [];
    for (let i = 0; i < elements.length; i++) {
      if (elements[i].nodeName === 'rule') {
        let ruleChildNodes = elements[i].childNodes;
        for (let j = 0; j < ruleChildNodes.length; j++) {
          if (ruleChildNodes[j].data) {
            definitionsLong.push(ruleChildNodes[j].data);
          } else if (ruleChildNodes[j].innerHTML) {
            definitionsLong.push(ruleChildNodes[j].innerHTML);
          }
        }
      } else {
        if (elements[i].data) {
          definitionsLong.push(elements[i].data);
        } else if (elements[i].innerHTML) {
          definitionsLong.push(elements[i].innerHTML);
        }
      }
    }
    definitionsLong = definitionsLong.join(' ');
    definitionsLong = definitionsLong.split(' ');
    let definitionToRender = [];
    definitionsLong.forEach(defItem => {
      if (defItem !== '') {
        definitionToRender.push(defItem);
      }
    });
    return definitionToRender.slice(0, 2).join(' ');
  }

  renderBodyText() {
    const { card } = this.props;
    const translation = card.translations[0].translation.translation;
    if (typeof translation === 'string' && translation.length > 0) {
      return (
        <div className="font-size-40px">
          {translation}
        </div>
      );
    } else {
      const definitionsShort = this.getDefinitionShort();
      if (definitionsShort.length > 0) {
        return (
          <div>
            {definitionsShort}
          </div>
        );
      } else {
        const definitionsLong = this.getFirstWordsOfDefinitionLong();
        return (
          <div>
            {definitionsLong}...
          </div>
        );
      }
    }
  }

  render() {
    return (
      <div>
        {this.renderBodyText()}
      </div>
    );
  }
}

CardsListItemNotes.propTypes = {
  card: PropTypes.object.isRequired
}

export default CardsListItemNotes;
