import React, { Component, PropTypes } from 'react';
import CardsListItem from './CardsListItem';

class CardsList extends Component {
  constructor(props) {
    super(props);
    this.renderCard = this.renderCard.bind(this);
  }

  renderCard(card, index) {
    const { currentCardIndex, setCurrentCardIndex } = this.props;
    return (
      <CardsListItem
        key={index}
        card={card}
        index={index}
        currentCardIndex={currentCardIndex}
        setCurrentCardIndex={setCurrentCardIndex}
      />
    );
  }

  render() {
    const { cards } = this.props;
    return (
      <div className='carousel-cards-list'>
        {cards.map(this.renderCard)}
      </div>
    );
  }
}

CardsList.propTypes = {
  cards: PropTypes.array.isRequired,
  setCurrentCardIndex: PropTypes.func.isRequired,
  currentCardIndex: PropTypes.number.isRequired
};

export default CardsList;
