import React, { Component, PropTypes } from 'react';

import * as localityConstants from '../../constants/localities';
import CardImageHelper from '../shared/helpers/CardImageHelper';
import LanguageTranslation from '../shared/language/LanguageTranslation';
import CardListItemNotes from '../reviewCards/CardListItemNotes';
import CarouselLanguage from './CarouselLanguage';

class Carousel extends Component {
  constructor(props) {
    super(props);
    this.previousImage = this.previousImage.bind(this);
    this.nextImage = this.nextImage.bind(this);
  }

  previousImage() {
    if (this.props.cards[this.props.currentCardIndex - 1]) {
      this.props.decrementCurrentCardIndex();
    }
  }

  nextImage() {
    if (this.props.cards[this.props.currentCardIndex + 1]) {
      this.props.incrementCurrentCardIndex();
    }
  }

  renderImage() {
    const { currentCard } = this.props;
    if (currentCard) {
      return (
        <img src={currentCard.image.url_600} alt={currentCard.id} className='slide-image'/>
      );
    }
  }

  renderLanguageTranslation() {
    const { cards, currentCard } = this.props;
    if (currentCard['cfg-note_char'] === 'true') {
      return (
        <div className="carousel-notes-wrapper">
          <CardListItemNotes
            card={currentCard}
            cards={cards}
          />
        </div>
      );
    } else {
      if (currentCard && currentCard.translations) {
        return (
          <LanguageTranslation
            translations={currentCard.translations}
            localityType={localityConstants.FOREIGN_LOCALITY}
          />
        );
      }
    }
  }

  renderSlide(card) {
    const { currentCard } = this.props;
    const cardImageHelper = new CardImageHelper();
    if (cardImageHelper.cardHasImage(currentCard)) {
      return this.renderImage();
    } else {
      return this.renderLanguageTranslation(card);
    }
  }

  getStyleForNativeLang() {
    const { packConfig } = this.props;
    if (!packConfig || !packConfig.packtype) return;
    if (packConfig.packtype === 'alphabet_letters') {
      return {visibility: 'hidden'};
    }
  }

  render() {
    const { cards, currentCardIndex, currentCard } = this.props;
    const cardsLength = cards.length - 1;
    this.getStyleForNativeLang();
    return (
      <div className='carousel-gallery'>
        <div className="slide">
          <div>
            <CarouselLanguage
              cards={cards}
              currentCard={currentCard}
              languageType={localityConstants.FOREIGN_LOCALITY}
            />
          </div>
          <div>
            {this.renderSlide()}
          </div>
          <div style={this.getStyleForNativeLang()}>
            <CarouselLanguage
              cards={cards}
              currentCard={currentCard}
              languageType={localityConstants.NATIVE_LOCALITY}
            />
          </div>
        </div>
        <div className="slide-buttons">
          {currentCardIndex > 0 ?
            <div className='back' onClick={this.previousImage}>
              Back
            </div>
          : null}
          {currentCardIndex < cardsLength ?
            <div className='next' onClick={this.nextImage}>
              Next
            </div>
          : null}
        </div>
      </div>
    );
  }
}

Carousel.propTypes = {
  cards: PropTypes.array.isRequired,
  currentCard: PropTypes.object.isRequired,
  currentCardIndex: PropTypes.number.isRequired,
  decrementCurrentCardIndex: PropTypes.func.isRequired,
  incrementCurrentCardIndex: PropTypes.func.isRequired,
  packConfig: PropTypes.object
};

export default Carousel;
