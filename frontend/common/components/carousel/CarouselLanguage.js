import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as localityConstants from '../../constants/localities';
import RomanisationWithTranslation from '../shared/romanization/RomanisationWithTranslation';
import AudioPlayer from '../shared/generalAudio/AudioPlayer';
import LocalityImage from '../shared/language/LocalityImage';
import PlayAudio from '../shared/helpers/PlayAudio';

class CarouselLanguage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      voiceHover: false
    };
    this.audioPlayer = new PlayAudio();
    this.playForeignAudio = this.playForeignAudio.bind(this);
    this.playNativeAudio = this.playNativeAudio.bind(this);
    this.toggleVoiceHover = this.toggleVoiceHover.bind(this);
  }

  playForeignAudio() {
    this.audioPlayer.playAudio('audio-carousel-control-element-foreign')
  }

  playNativeAudio() {
    this.audioPlayer.playAudio('audio-carousel-control-element-native');
  }

  toggleVoiceHover() {
    this.setState({ voiceHover: !this.state.voiceHover });
  }

  getVoiceStyle() {
    if (this.state.voiceHover) {
      return {
        backgroundImage: 'url("/resources/icons/voice-icon-hover.png")',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'right top',
        backgroundSize: '25px 25px'
      };
    } else {
      return {
        backgroundImage: 'url("/resources/icons/voice-icon.png")',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'right top',
        backgroundSize: '25px 25px'
      };
    }
  }

  renderForeignLang() {
    const { currentCard } = this.props;
    return (
      <div className="slide-language">
        <div>
          <LocalityImage localityType={localityConstants.FOREIGN_LOCALITY} className='locality-image'/>
        </div>
        <div>
          <RomanisationWithTranslation
            localityType={localityConstants.FOREIGN_LOCALITY}
            translations={currentCard.translations}
          />
        </div>
        <div
          className="audio-image"
          style={this.getVoiceStyle()}
          onMouseEnter={this.toggleVoiceHover}
          onMouseLeave={this.toggleVoiceHover}
          onClick={this.playForeignAudio}>
          <AudioPlayer
            translations={currentCard.translations}
            localityType={localityConstants.FOREIGN_LOCALITY}
            generalId={'audio-carousel-control-element-'}
            cardId={'foreign'}
            autoPlay={true}
          />
        </div>
      </div>
    );
  }

  renderNativeLang() {
    const { selectedLocality, currentCard, packType } = this.props;
    if (selectedLocality.native) {
      if (packType !== 'alphabet_letters') {
        return (
          <div className="slide-language" id={'carousel-control-element-second'}>
            <div>
              <LocalityImage localityType={localityConstants.NATIVE_LOCALITY} className='locality-image'/>
            </div>
            <div>
              <RomanisationWithTranslation
                translations={currentCard.translations}
                localityType={localityConstants.NATIVE_LOCALITY}
              />
            </div>
            <div
              style={this.getVoiceStyle()}
              onMouseEnter={this.toggleVoiceHover}
              onMouseLeave={this.toggleVoiceHover}
              className="audio-image"
              onClick={this.playNativeAudio}>
              <AudioPlayer
                localityType={localityConstants.NATIVE_LOCALITY}
                translations={currentCard.translations}
                generalId={'audio-carousel-control-element-'}
                cardId={'native'}
                autoPlay={false}
              />
            </div>
          </div>
        );
      }
    }
  }

  renderLang() {
    const { languageType } = this.props;
    if (languageType === localityConstants.FOREIGN_LOCALITY)  {
      return this.renderForeignLang();
    } else if (languageType === localityConstants.NATIVE_LOCALITY) {
      return this.renderNativeLang();
    }
  }

  render() {
    return (
      <div>
        {this.renderLang()}
      </div>
    );
  }
}

CarouselLanguage.propTypes = {
  packType: PropTypes.string,
  cards: PropTypes.array.isRequired,
  currentCard: PropTypes.object.isRequired,
  languageType: PropTypes.string.isRequired
};

const mapStateToProps = state => {
  return {
    selectedLocality: state.common.localities.selectedLocality
  }
};

export default connect(mapStateToProps)(CarouselLanguage);
