import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import CardList from './CardsList';
import { clearReviewCardsList } from '../../actions/reviewCards';
import './styles.scss';
import Carousel from "./Carousel";
import ModesContainer from '../modes/ModesContainer';
import Loader from '../shared/loader/Loader';

class CarouselContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentCardIndex: 0
    };
    this.decrementCurrentCardIndex = this.decrementCurrentCardIndex.bind(this);
    this.incrementCurrentCardIndex = this.incrementCurrentCardIndex.bind(this);
    this.setCurrentCardIndex = this.setCurrentCardIndex.bind(this);
    this.getCurrentCard = this.getCurrentCard.bind(this);
  }

  componentWillMount() {
    this.props.fetchCards(this.props.cardsId);
  }

  componentDidMount() {
    this.refs.loader.runOnImageLoad();
  }

  componentWillUnmount() {
    this.props.clearReviewCardsList();
  }

  decrementCurrentCardIndex() {
    this.setState({
      currentCardIndex: this.state.currentCardIndex - 1
    });
  }

  incrementCurrentCardIndex() {
    this.setState({
      currentCardIndex: this.state.currentCardIndex + 1
    });
  }

  setCurrentCardIndex(newIndex) {
    this.setState({
      currentCardIndex: parseInt(newIndex)
    });
  }

  getCurrentCard() {
    const cards = this.props.cards;
    if (Array.isArray(cards) && cards.length > 0) {
      return this.props.cards[this.state.currentCardIndex];
    } else {
      return {
        translations: []
      };
    }
  }

  getPackType() {
    const { packConfig } = this.props;
    if (packConfig) {
      return packConfig.packtype;
    }
  }

  render() {
    const { cards, cardsId } = this.props;
    return (
      <div>
        <Loader ref="loader"/>
        <div className="row">
          <div className="col-md-8 col-md-offset-2">
            <ModesContainer
              title="Play cards"
              imageName={'cards-icon.png'}
              imageHoverName={'cards icon hover.png'}
              paramsId={cardsId}
            />
          </div>
        </div>
        <div className='row'>
          <div className="col-md-8 col-md-offset-2">
            <Carousel
              cards={cards}
              currentCard={this.getCurrentCard()}
              currentCardIndex={this.state.currentCardIndex}
              decrementCurrentCardIndex={this.decrementCurrentCardIndex}
              incrementCurrentCardIndex={this.incrementCurrentCardIndex}
              packConfig={this.props.packConfig}
            />
            <CardList
              cards={cards}
              currentCardIndex={this.state.currentCardIndex}
              setCurrentCardIndex={this.setCurrentCardIndex}
            />
          </div>
        </div>
      </div>
    );
  }
}

CarouselContainer.propTypes = {
  cards: PropTypes.array.isRequired,
  fetchCards: PropTypes.func.isRequired,
  cardsId: PropTypes.string.isRequired,
  packConfig: PropTypes.object
};

export default connect(null, {
  clearReviewCardsList
})(CarouselContainer);
