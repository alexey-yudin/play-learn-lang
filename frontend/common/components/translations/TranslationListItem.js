import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/translation';

class TranslationListItem extends Component {
  constructor(props) {
    super(props);
    this.setTranslation = this.setTranslation.bind(this);
  }

  setTranslation() {
    const { translation } = this.props;
    this.props.setTranslation(translation.id);
  }

  render() {
    const { translation, selectedId } = this.props;
    return (
      <li className={`translation-list-item ${translation.id === selectedId ? 'selected' : ''}`}
        onClick={this.setTranslation}>
        {translation.language}
      </li>
    );
  }
}

TranslationListItem.propTypes = {
  translation: PropTypes.object.isRequired,
  selectedId: PropTypes.string.isRequired
};

export default connect(null, {
  setTranslation: actions.setTranslation,
})(TranslationListItem);
