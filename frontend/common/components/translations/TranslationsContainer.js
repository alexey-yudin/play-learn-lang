import React, { Component } from 'react';
import TranslationList from './TranslationsList';
import Translation from '../shared/translations/Translation';
import './styles.scss';

class TranslationsContainer extends Component {
  constructor(props) {
    super(props);
    this.toggleDropdown = this.toggleDropdown.bind(this);
  }

  componentDidMount() {
    this.closeOnOutsideClick();
  }

  toggleDropdown() {
    $(this.refs['dropdown']).toggleClass('open');
  }

  closeDropdown() {
    $(this.refs.dropdown).removeClass('open');
  }

  closeOnOutsideClick() {
    $(document).on('click', () => this.closeDropdown());
  }

  getLinkStyle() {
    return {
      backgroundImage: 'url("/resources/icons/translate.png")',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'left top',
      backgroundSize: '20px 20px'
    };
  }

  render() {
    return (
      <div className="dropdown" ref="dropdown">
        <div
          style={this.getLinkStyle()}
          className="translation-dropdown-item dropdown-toggle cursor-pointer"
          onClick={this.toggleDropdown}>
          <Translation path='translation'/>
        </div>
        <ul className='dropdown-menu pull-right'
          style={{overflow: 'hidden'}}>
          <TranslationList/>
        </ul>
      </div>
    );
  }
}

export default TranslationsContainer;
