import React, { Component } from 'react';
import { connect } from 'react-redux';
import TranslationListItem from './TranslationListItem';
import * as actions from '../../actions/translation';

class TranslationsList extends Component {
  constructor(props) {
    super(props);
    this.renderTranslationLabel = this.renderTranslationLabel.bind(this);
  }

  componentWillMount() {
    this.props.fetchTranslations();
    this.props.setDefaultTranslation();
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.selectedTranslation.id) {
      this.props.setTranslation('en_us');
    }
  }

  renderTranslationLabel(translation) {
    const { selectedTranslation } = this.props;
    return (
      <TranslationListItem
        key={translation.id}
        translation={translation}
        selectedId={selectedTranslation.id}
      />
    );
  }

  render() {
    const { translations } = this.props;
    return (
      <div className="translation-list" style={{display: 'block'}}>
        {translations.map(this.renderTranslationLabel)}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    translations: state.common.translation.translations,
    selectedTranslation: state.common.translation.selectedTranslation
  }
}

export default connect(mapStateToProps, {
  fetchTranslations: actions.fetchTranslations,
  setTranslation: actions.setTranslation,
  setDefaultTranslation: actions.setDefaultTranslation
})(TranslationsList);
