import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import * as localityConstants from '../../constants/localities';
import RomanisedTranslation from '../shared/romanization/RomanisedTranslation';
import LanguageTranslation from '../shared/language/LanguageTranslation';

class CardItem extends Component {
  renderLockImage() {
    const { cardKey, card, type, onClickCallback } = this.props;
    return (
      <img data-card-key={cardKey}
        data-card-id={card.id} data-card-type={type}
        onClick={onClickCallback} src="/assets/lock.jpg" alt='lock'
        className='memory-game-lock-image memory-game-card'/>
    );
  }

  renderCardImage() {
    const { card } = this.props;
    if (card.image.url_600) {
      return (
        <img className='memory-game-card-image memory-game-card'
          src={card.image.url_600} alt={card.id}/>
      );
    }
  }

  renderAudioImage() {
    const { card } = this.props;
    const blockStyle = {
      display: 'inline-block',
    };
    const imageStyle = {
      maxWidth: '60px'
    };
    return (
      <div style={blockStyle} className='memory-game-card'>
        <img style={imageStyle} src='/assets/audio.png' alt={card.id}/>
      </div>
    );
  }

  renderCardLabel(localityType) {
    const { card, selectedLocality, packConfig, type, romanisedEnabled } = this.props;
    if (packConfig) {
      const { packtype } = packConfig;
      if (packtype === 'alphabet_letters') {
        if (type === 'image') {
          return (
            <div style={{fontSize: '50px'}} className='memory-game-card-label memory-game-card'>
              <LanguageTranslation
                translations={card.translations}
                localityType={localityType}
              />
            </div>
          );
        } else {
          if (romanisedEnabled) {
            return (
              <div style={{fontSize: '25px'}} className='memory-game-card-label memory-game-card font-size-25px'>
                <RomanisedTranslation
                  localityType={localityConstants.FOREIGN_LOCALITY}
                  translations={card.translations}
                />
              </div>
            );
          } else {
            return this.renderAudioImage();
          }
        }
      } else if (packtype === 'alphabet_numbers') {
        if (type === 'image') {
          return (
            <div style={{fontSize: '25px'}} className='memory-game-card-label memory-game-card'>
              {card.number}
            </div>
          );
        } else {
          return (
            <div style={{fontSize: '40px'}} className='memory-game-card-label memory-game-card'>
              <LanguageTranslation
                localityType={localityType}
                translations={card.translations}
              />
            </div>
          );
        }
      } else {
        return (
          <div style={{fontSize: '40px'}} className='memory-game-card-label memory-game-card'>
            <LanguageTranslation
              localityType={localityConstants.FOREIGN_LOCALITY}
              translations={card.translations}
            />
          </div>
        );
      }
    } else {
      return (
        <div className='memory-game-card-label memory-game-card'>
          <LanguageTranslation
            localityType={localityType}
            translations={card.translations}
          />
        </div>
      );
    }
  }

  imageExist(card) {
    if (typeof card.image.url_600 === 'string' && card.image.url_600.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  render() {
    const { isOpened, type, selectedLocality, card } = this.props;
    if (type === 'image') {
      if (isOpened) {
        if (this.imageExist(card)) {
          return this.renderCardImage();
        } else {
          if (selectedLocality.native) {
            return this.renderCardLabel(localityConstants.NATIVE_LOCALITY);
          } else {
            return this.renderCardLabel(localityConstants.FOREIGN_LOCALITY);
          }
        }
      } else {
        return this.renderLockImage();
      }
    } else if (type === 'label') {
      if (isOpened) {
        return this.renderCardLabel(localityConstants.NATIVE_LOCALITY);
      } else {
        return this.renderLockImage();
      }
    }
  }
}

CardItem.propTypes = {
  type: PropTypes.string.isRequired,
  isOpened: PropTypes.bool.isRequired,
  cardKey: PropTypes.string.isRequired,
  card: PropTypes.object.isRequired,
  packConfig: PropTypes.object
}

function mapStateToProps(state) {
  return {
    selectedLocality: state.common.localities.selectedLocality,
    romanisedEnabled: state.common.generalSettings.romanisation
  }
}

export default connect(mapStateToProps)(CardItem);
