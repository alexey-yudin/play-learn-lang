import React, { Component } from 'react';
import { connect } from 'react-redux';

import { DEFAULT_VARIANTS_FOR } from '../../constants/memoryGame';
import * as actions from '../../actions/memoryGameSettings';

class MemoryGameSettings extends Component {
  constructor(props) {
    super(props);
    this.handleOnClick = this.handleOnClick.bind(this);
    this.renderButton = this.renderButton.bind(this);
  }

  handleOnClick(event) {
    const { appName } = this.props;
    const cardAmount = event.target.dataset['cardAmount'];
    if ($('#memory-game-card-list').length === 1) {
      $('#memory-game-card-list').fadeTo(0, false);
      setTimeout(() => {
        this.props.setAmountOfCards(cardAmount, appName);
        $('#memory-game-play-again-button').trigger('click');
        $('#memory-game-card-list').fadeTo(700, true);
      }, 100);
    } else {
      this.props.setAmountOfCards(cardAmount, appName);
    }
  }

  isButtonActive(amount) {
    if (this.props.memoryGameSettings.amount == amount) {
      return true;
    } else {
      return false;
    }
  }

  renderButton(variant, index) {
    return (
      <div key={index} className="settings-button-item">
        <div
          data-card-amount={variant.amount} onClick={this.handleOnClick}
          className={`settings-button`}>
          <div className={`${this.isButtonActive(variant.amount) ? 'settings-button-selected' : ''}`}>
          </div>
        </div>
        <div>
          {variant.name}
        </div>
      </div>
    )
  }

  render() {
    return (
      <div className="settings-buttons-container">
        Size: {DEFAULT_VARIANTS_FOR.map(this.renderButton)}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    appName: state.common.generalSettings.meta.appName,
    memoryGameSettings: state.common.memoryGameSettings
  }
}

export default connect(mapStateToProps, {
  setAmountOfCards: actions.setAmountOfCards
})(MemoryGameSettings);
