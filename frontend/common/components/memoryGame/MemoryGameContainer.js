import React, { Component, PropTypes } from 'react';
import _ from 'lodash';
import CardItem from './CardItem';
import ImagePreloader from '../shared/imagePreloader/ImagePreloader';
import AudioCardLabel from './AudioCardLabel';
import AudioAnswersContainer from '../shared/game_audio/AudioAnswersContainer.js';
import ModesContainer from '../modes/ModesContainer';
import { DEFAULT_AMOUNT_OF_CARDS } from '../../constants/memoryGame';
import Loader from '../shared/loader/Loader';
import './styles.scss';

class MemoryGameContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allCards: [],
      keysOfCardsInReview: [], // need to display the image or label when the card was clicked
      openedCards: [],
      reviewedCard: null, // the card clicked first time
      audioForCards: [],
      timerToClose: null
    }
    this.moveCardKeyToReview = this.moveCardKeyToReview.bind(this);
    this.moveCardIdToOpenedCards = this.moveCardIdToOpenedCards.bind(this);
    this.clearKeysOfCardsInReview = this.clearKeysOfCardsInReview.bind(this);
    this.onClickHandler = this.onClickHandler.bind(this);
    this.renderCards = this.renderCards.bind(this);
    this.compareClickedCardWithCardInReview = this.compareClickedCardWithCardInReview.bind(this);
    this.clearReviewedCardKey = this.clearReviewedCardKey.bind(this);
    this.getDefaultListOfCards = this.getDefaultListOfCards.bind(this);
    this.getCardItem = this.getCardItem.bind(this);
    this.handlePlayAgainClick = this.handlePlayAgainClick.bind(this);
    this.playAgain = this.playAgain.bind(this);
  }

  componentDidMount() {
    this.props.fetchCards(this.props.cardsId);
    this.refs.loader.runOnImageLoad();
  }

  componentWillReceiveProps(nextProps) {
    const cards = nextProps.cards;
    if (Array.isArray(cards)) {
      this.startGame(cards);
    }
  }

  componentDidUpdate() {
    this.hideSpinnerWhenImagesArePreloaded();
  }

  startGame(cards) {
    const randomCards = this.getRandomCards(cards, this.normalizeCardAmount());
    const cardImageItems = this.getDefaultListOfCards('image', randomCards, 0);
    const cardLabelItems = this.getDefaultListOfCards('label', randomCards, cardImageItems.length);
    const allCards = _.shuffle(cardImageItems.concat(cardLabelItems));
    this.createAudioForCardsFromCardItemsArray(cardLabelItems);
    this.setState({ allCards });
  }

  playAgain(cards) {
    this.clearKeysOfCardsInReview();
    this.clearReviewedCardKey();
    this.clearOpenedCards();
    this.startGame(cards);
  }

  normalizeCardAmount() {
    // In the settigns there is the amount of all of the cards
    // We need the half of this. If we cannot get the half, return the default
    let memoryGameSettings = {...this.props.memoryGameSettings}
    if (memoryGameSettings) {
      const cardAmount = parseInt(memoryGameSettings.amount);
      if (cardAmount % 2 === 0) {
        return cardAmount / 2;
      } else {
        return parseInt(DEFAULT_AMOUNT_OF_CARDS);
      }
    }
  }

  createAudioForCardsFromCardItemsArray(cardsItems) {
    let audioForCards = [];
    if (Array.isArray(cardsItems)) {
      audioForCards = cardsItems.map((cardItem, index) => {
        return <AudioCardLabel key={index} card={cardItem.props.card} cardKey={cardItem.key}/>
      });
    }
    this.setState({ audioForCards });
  }

  isCardProper(card) {
    if (card['cfg-note_char'] !== 'true' && card['cfg-between_char'] !== 'true') {
      return true;
    } else {
      return false;
    }
  }

  getRandomCards(cards, amount) {
    let randomCards = [];

    if (Array.isArray(cards) && typeof amount === 'number') {
      while (randomCards.length < amount) {
        const randomNumber = _.random(0, cards.length - 1);
        const randomCard = cards[randomNumber];
        if (randomCards.indexOf(randomCard) === -1 && this.isCardProper(randomCard)) {
          randomCards.push(randomCard);
        }
      }
    }

    return randomCards;
  }

  getDefaultListOfCards(type, cards, key=0) {
    let labelCards = [];
    if (Array.isArray(cards)) {
      return cards.map((card, index) => {
        const cardKey = key > 0 ? index + key : index;
        return this.getCardItem(type, card, cardKey, false);
      });
    }
  }

  getCardItem(type, card, cardKey, isOpened=false) {
    const { packConfig } = this.props;
    return (
      <CardItem
        key={cardKey}
        card={card}
        cardKey={cardKey.toString()}
        isOpened={isOpened}
        type={type}
        onClickCallback={this.onClickHandler}
        packConfig={packConfig}
      />
    );
  }

  moveCardKeyToReview(cardKey) {
    this.setState({
      keysOfCardsInReview: [...this.state.keysOfCardsInReview, cardKey]
    });
  }

  setClickedCardToReview(clickedCard) {
    this.setState({
      reviewedCard: clickedCard
    });
  }

  moveCardIdToOpenedCards(cardId) {
    this.setState({
      openedCards: [...this.state.openedCards, cardId]
    });
  }

  clearKeysOfCardsInReview() {
    this.setState({
      keysOfCardsInReview: []
    });
  }

  clearReviewedCardKey() {
    this.setState({
      reviewedCard: null
    });
  }

  clearOpenedCards() {
    this.setState({
      openedCards: []
    });
  }

  isGameFinished() {
    if (this.state.openedCards.length + 1 === this.state.allCards.length / 2) {
      return true;
    } else {
      return false;
    }
  }

  compareClickedCardWithCardInReview(clickedCard) {
    if (this.state.keysOfCardsInReview.length + 1 >= 3) {
      clearTimeout(this.state.timerToClose);
      setTimeout(() => {
        this.setClickedCardToReview(clickedCard);
        this.clearKeysOfCardsInReview();
        this.moveCardKeyToReview(clickedCard.key);
      }, 0);
      return;
    }
    if (this.state.reviewedCard !== null) {
      if (this.state.reviewedCard.id === clickedCard.id) {
        this.refs['correct-wrong-answer'].stopAudio();
        this.refs['correct-wrong-answer'].playCorrectAnswer();
        this.moveCardIdToOpenedCards(clickedCard.id);
        this.clearReviewedCardKey();
        setTimeout(() => {
          this.clearKeysOfCardsInReview();
        }, 0);
        if (this.isGameFinished()) {
          setTimeout(() => {
            $('#memory-game-card-list').hide('slow');
            $('#memory-game-play-again-btn').show('slow');
          }, 800);
        }
      } else {
        this.refs['correct-wrong-answer'].stopAudio();
        this.refs['correct-wrong-answer'].playWrongAnswer();
        let timerToClose = setTimeout(() => {
          this.clearReviewedCardKey();
          this.clearKeysOfCardsInReview();
        }, 800);
        this.setState({ timerToClose })
      }
    }
  }

  onClickHandler(event) {
    const cardKey = event.target.dataset['cardKey'];
    const cardId = event.target.dataset['cardId'];
    const clickedCard = {key: cardKey, id: cardId};
    const cardType = event.target.dataset['cardType'];

    if (cardType === 'label') {
      $(`#memory-game-audio-${cardKey}`).trigger('play');
    }

    if (this.state.reviewedCard !== null) {
      this.compareClickedCardWithCardInReview(clickedCard);
    } else {
      this.setClickedCardToReview(clickedCard);
    }
    this.moveCardKeyToReview(cardKey);
  }

  handlePlayAgainClick(event) {
    this.playAgain(this.props.cards);
    $('#memory-game-card-list').show('slow');
    $('#memory-game-play-again-btn').hide('slow');
  }

  renderCards() {
    const keysOfCardsInReview = this.state.keysOfCardsInReview;
    const openedCards = this.state.openedCards;
    return this.state.allCards.map((cardItem, index) => {
      if (keysOfCardsInReview.indexOf(cardItem.key) > -1
          || openedCards.indexOf(cardItem.props.card.id) > -1) {
        if (cardItem.props.type === 'image') {
          return this.getCardItem('image', cardItem.props.card, cardItem.key, true);
        } else if (cardItem.props.type === 'label') {
          return this.getCardItem('label', cardItem.props.card, cardItem.key, true);
        }
      }
      return cardItem;
    });
  }

  getCardsWithGrid(cards, devider) {
    if (Array.isArray(cards) && typeof devider === 'number') {
      return cards.map((cardItem, index) => {
        if (index % devider == 0) {
          return (
            <span key={cardItem.key}>
              <br/>
              {cardItem}
            </span>
          );
        }
        return cardItem;
      });
    }
  }

  renderCardsAsGrid(cards) {
    const cardsAmount = this.props.memoryGameSettings.amount;
    if (cardsAmount) {
      if (parseInt(cardsAmount) === 6) {
        return this.getCardsWithGrid(cards, 3);
      } else if (parseInt(cardsAmount) === 12) {
        return this.getCardsWithGrid(cards, 4);
      } else if (parseInt(cardsAmount) === 20) {
        return this.getCardsWithGrid(cards, 5);
      } else if (parseInt(cardsAmount) === 30) {
        return this.getCardsWithGrid(cards, 6);
      }
    }
  }

  preloadImage(card, index) {
    return (
      <img key={index} src={card.image.url_600} className='quiz-image-preload'/>
    );
  }

  preloadAllOfTheImages() {
    let cardsArray = [];
    if (Array.isArray(this.props.cards)) {
      cardsArray = this.props.cards.map(this.preloadImage);
    }
    return cardsArray;
  }

  hideSpinnerWhenImagesArePreloaded() {
    const cards = this.props.cards;
    const quizImagesElement = $('.quiz-images-preload-list');
    if (Array.isArray(cards)) {
      let count = 0;
      if (quizImagesElement.children('img').length === cards.length) {
        this.hideSpinner();
      }
      quizImagesElement.on('DOMNodeInserted', (event) => {
        count = count += 1;
        if (count === cards.length) {
          this.hideSpinner();
        }
      });
    }
  }

  hideSpinner() {
    setTimeout(() => {
      $('#spinner-wheel-container').hide('fast');
    }, 1000);
  }

  render() {
    const { cardsId } = this.props;
    return (
      <div>
        <Loader ref="loader"/>
        <div className="row">
          <div className="col-sm-12 col-md-10 col-md-offset-1">
            <ModesContainer
              title={'Memory game'}
              imageName={'memory game icon.png'}
              imageHoverName={'memory game icon.png'}
              paramsId={cardsId}
            />
          </div>
        </div>
        <div className='row'>
          <div className="col-sm-12 col-md-10 col-md-offset-1">
            <div className="memory-game-container" id='memory-game-panel'>
              <div className="memory-game-card-list" id='memory-game-card-list'>
                {this.renderCardsAsGrid(this.renderCards())}
              </div>
              <div className="clearfix"></div>
              <div id="memory-game-play-again-btn" className="memory-game-play-again-container">
                <button onClick={this.handlePlayAgainClick} type='button' id='memory-game-play-again-button'
                        className='btn btn-default'>Play again</button>
              </div>
            </div>
            <div className='memory-game-audio'>
              {this.state.audioForCards}
            </div>

            <AudioAnswersContainer ref='correct-wrong-answer'/>
            <ImagePreloader cards={this.props.cards}/>
          </div>
        </div>
      </div>
    );
  }
}

MemoryGameContainer.propTypes = {
  cards: PropTypes.array.isRequired,
  fetchCards: PropTypes.func.isRequired,
  cardsId: PropTypes.string.isRequired,
  memoryGameSettings: PropTypes.object.isRequired,
  appName: PropTypes.string.isRequired,
  packConfig: PropTypes.object
};

export default MemoryGameContainer;
