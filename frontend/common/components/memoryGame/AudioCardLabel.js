import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as localityConstants from '../../constants/localities';
import AudioPlayer from '../shared/generalAudio/AudioPlayer';

class AudioCardLabel extends Component {
  render() {
    const { card, cardKey } = this.props;
    if (card) {
      return (
        <AudioPlayer
          translations={card.translations}
          localityType={localityConstants.FOREIGN_LOCALITY}
          generalId='memory-game-audio-'
          cardId={cardKey}/>
      );
    } else {
      return (
        <div></div>
      );
    }
  }
}

export default AudioCardLabel;
