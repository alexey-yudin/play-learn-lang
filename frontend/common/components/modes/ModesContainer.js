import React, { Component, PropTypes } from 'react';
import ModesTitle from './ModesTitle';
import ModesList from './ModesList';
import './styles.scss';

class ModesContainer extends Component {
  render() {
    const { title, paramsId, imageName, imageHoverName } = this.props;
    return (
      <div className="modes-container">
        <ModesTitle
          title={title}
          imageName={imageName}
          imageHoverName={imageHoverName}
        />
        <ModesList paramsId={paramsId}/>
      </div>
    );
  }
}

ModesContainer.propTypes = {
  title: PropTypes.string.isRequired,
  paramsId: PropTypes.string.isRequired,
  imageName: PropTypes.string.isRequired,
  imageHoverName: PropTypes.string.isRequired
};

export default ModesContainer;
