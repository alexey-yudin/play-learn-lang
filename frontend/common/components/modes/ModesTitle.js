import React, { Component, PropTypes } from 'react';

class ModesTitle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hover: false
    };
    this.toggleHover = this.toggleHover.bind(this);
  }

  getBackgroundStyle() {
    const { imageName, imageHoverName } = this.props;
    if (this.state.hover) {
      return {
        backgroundImage: `url('/resources/icons/${imageHoverName}')`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'left, top',
        backgroundSize: '25px 25px'
      }
    } else {
      return {
        backgroundImage: `url('/resources/icons/${imageName}')`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'left, top',
        backgroundSize: '25px 25px'
      }
    }
  }

  toggleHover() {
    this.setState({ hover: !this.state.hover });
  }

  render() {
    const { title } = this.props;
    return (
      <div
        onMouseEnter={this.toggleHover}
        onMouseLeave={this.toggleHover}
        className="modes-title"
        style={this.getBackgroundStyle()}>
        {title}
      </div>
    );
  }
}

ModesTitle.propTypes = {
  title: PropTypes.string.isRequired,
  imageName: PropTypes.string.isRequired,
  imageHoverName: PropTypes.string.isRequired
};

export default ModesTitle;
