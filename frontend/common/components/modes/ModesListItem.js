import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

class ModesListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hover: false
    };
    this.toggleHover = this.toggleHover.bind(this);
    this.getLinkClass = this.getLinkClass.bind(this);
    this.getSelectedClass = this.getSelectedClass.bind(this);
    this.getBackgroundImageStyles = this.getBackgroundImageStyles.bind(this);
  }

  isCurrentMode() {
    const { modeName } = this.props;
    if (window.location.pathname.indexOf(modeName) > -1) return true;
  }

  getSelectedClass() {
    if (this.isCurrentMode()) return 'selected';
  }

  getBackgroundImageStyles() {
    const { imageName, imageHoverName } = this.props;
    if (this.isCurrentMode() || this.state.hover) {
      return {
        backgroundImage: `url('/resources/modes-menu-icons/${imageHoverName}')`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: '10px, top',
        backgroundSize: '20px'
      }
    } else {
      return {
        backgroundImage: `url('/resources/modes-menu-icons/${imageName}')`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: '10px, top',
        backgroundSize: '20px'
      }
    }
  }

  getLinkClass() {
    if (!this.isCurrentMode()) return;
    return {
      color: '#fff'
    }
  }

  toggleHover() {
    this.setState({ hover: !this.state.hover });
  }

  isHidden() {
    const { invisibleAppNames } = this.props;
    if (!invisibleAppNames) return false;
    const appName = location.pathname.split('/').filter(item => item.length > 0)[0];
    return invisibleAppNames.indexOf(appName) > -1;
  }

  render() {
    const { modeName, label, appName, paramsId } = this.props;
    if (this.isHidden()) return null;
    return (
      <div
        onMouseLeave={this.toggleHover}
        onMouseEnter={this.toggleHover}
        className={this.getSelectedClass()}
        style={this.getBackgroundImageStyles()}>
        <Link
          style={this.getLinkClass()}
          to={`/${appName}/package/${paramsId}/${modeName}`}>
          {label}
        </Link>
      </div>
    );
  }
}

ModesListItem.propTypes = {
  modeName: PropTypes.string.isRequired,
  imageName: PropTypes.string.isRequired,
  imageHoverName: PropTypes.string.isRequired,
  appName: PropTypes.string.isRequired,
  paramsId: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  invisibleAppNames: PropTypes.array
};

export default ModesListItem;
