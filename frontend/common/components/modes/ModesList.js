import React, { Component, PropTypes } from 'react';
import * as routeConstants from '../../constants/routes';
import ModesListItem from './ModesListItem';
import { connect } from 'react-redux';

class ModesList extends Component {
  constructor(props) {
    super(props);
    this.toggleDropdown = this.toggleDropdown.bind(this);
  }

  componentDidMount() {
    this.closeOnOutsideClick();
  }

  toggleDropdown() {
    $(this.refs['dropdown']).toggleClass('open');
  }

  closeDropdown() {
    $(this.refs.dropdown).removeClass('open');
  }

  closeOnOutsideClick() {
    $(document).on('click', event => {
      const target = event.target;
      if (!$(target).closest(this.refs.dropdown).length) {
        this.closeDropdown();
      }
    });
  }

  render() {
    const { appName, paramsId } = this.props;
    return (
      <div className="dropdown" ref="dropdown">
        <div className='modes-button' onClick={this.toggleDropdown}>
          <div>
            <img src="/resources/modes-menu-icons/chande-game-icon.png" alt=""/>
          </div>
          <div>
            Change mode
          </div>
        </div>
        <ul className="dropdown-menu pull-right"
          style={{padding: 0}}
          onClick={this.toggleDropdown}>
          <div className="triangle"></div>
          <div className="modes-list">
            <ModesListItem
              modeName={routeConstants.cardsCarousel}
              imageName="cards 20x20px.png"
              imageHoverName="cards HOVER 20x20px.png"
              appName={appName}
              paramsId={paramsId}
              label='Play cards'
            />
            <ModesListItem
              modeName={routeConstants.quiz}
              imageName="quiz 20x20px.png"
              imageHoverName="quiz HOVER 20x20px.png"
              appName={appName}
              paramsId={paramsId}
              label="Quiz"
            />
            <ModesListItem
              modeName={routeConstants.memoryGame}
              imageName="memory game 20x20px.png"
              imageHoverName="memory game HOVER 20x20px.png"
              appName={appName}
              paramsId={paramsId}
              label="Memory game"
            />
            <ModesListItem
              modeName={routeConstants.reviewCards}
              imageName="show-all.png"
              imageHoverName="show-all.png"
              appName={appName}
              paramsId={paramsId}
              label="Show all cards"
            />
            <ModesListItem
              modeName={routeConstants.bingoQuiz}
              imageName="bingo.png"
              imageHoverName="bingo.png"
              appName={appName}
              paramsId={paramsId}
              label="Bingo"
              invisibleAppNames={[routeConstants.alphakm]}
            />
            <ModesListItem
              modeName={routeConstants.shuffledAbc}
              imageName="abc_black.png"
              imageHoverName="abc_white.png"
              appName={appName}
              paramsId={paramsId}
              label="Shuffled ABC"
              invisibleAppNames={[routeConstants.alphakm]}
            />
          </div>
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    appName: state.common.generalSettings.meta.appName,
    packtype: state.khmerAlphabet.cards.config.packtype
  }
};

ModesList.propTypes = {
  paramsId: PropTypes.string.isRequired
};

export default connect(mapStateToProps)(ModesList);
