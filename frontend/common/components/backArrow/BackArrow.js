import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { Link } from 'react-router';
import * as actions from '../../actions/viewConfig';

class BackArrow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      exist: false,
      currentPackageId: null
    }
  }

  componentWillMount() {
    const { appName } = this.props;
    this.props.getNestedPackageIds(appName);
  }

  componentWillReceiveProps(nextProps) {
    this.setPackageIdExist(nextProps.nestedPackageIds, nextProps.pathname);
  }

  setPackageIdExist(nestedPackageIds, pathname) {
    let currentPackageId;
    const exist = nestedPackageIds
      .map(nestedPackageId => {
        if (nestedPackageIds[pathname.indexOf(nestedPackageId.id)]) {
          currentPackageId = nestedPackageId.parentId;
        }
        return nestedPackageId;
      })
      .some(nestedPackageId => pathname.includes(nestedPackageId.id));
    this.setState({ exist, currentPackageId });
  }

  isPackageMainPage() {
    const { pathname } = this.props;
    return pathname.split('/').filter(item => item.length > 0).length === 1;
  }

  render() {
    const { appName } = this.props;
    if (this.state.exist) {
      return (
        <div className="navbar-link">
          <Link to={`/${appName}/package-list/${this.state.currentPackageId}`}>
            <span className='glyphicon glyphicon-arrow-left'></span>
          </Link>
        </div>
      );
    } else if (!this.isPackageMainPage()) {
      return (
        <div className="navbar-link">
          <Link to={`/${appName}`}>
            <span className='glyphicon glyphicon-arrow-left'></span>
          </Link>
        </div>
      );
    } else {
      return null;
    }
  }
}

BackArrow.propTypes = {
  pathname: PropTypes.string.isRequired,
};

const mapStateToProps = state => {
  return {
    appName: state.common.generalSettings.meta.appName,
    nestedPackageIds: state.common.viewConfig.nestedPackageIds
  }
};

export default connect(mapStateToProps, {
  getNestedPackageIds: actions.getNestedPackageIds
})(BackArrow);
