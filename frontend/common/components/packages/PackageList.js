import React, { Component, PropTypes } from 'react';
import Loader from '../shared/loader/Loader';

class PackageList extends Component {
  componentWillMount() {
    this.props.fetchPackages(this.props.packageId);
  }

  componentDidMount() {
    this.refs.loader.runOnImageLoad();
  }

  render() {
    return (
      <div>
        <Loader ref="loader"/>
        <div className='row packages-container'>
          {this.props.packages.map(this.props.renderPackage)}
        </div>
      </div>
    );
  }
}

PackageList.propTypes = {
  packages: PropTypes.array.isRequired,
  renderPackage: PropTypes.func.isRequired,
  fetchPackages: PropTypes.func.isRequired
}

export default PackageList;
