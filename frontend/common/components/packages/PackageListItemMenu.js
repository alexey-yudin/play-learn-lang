import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import Translation from '../../../common/components/shared/translations/Translation';
import * as routeConstants from '../../../common/constants/routes';
import './styles.scss';

class PackageListItemMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      packtype: null
    };
  }

  componentDidMount() {
    this.setPacktype();
  }

  getAppName() {
    return location.pathname.split('/').filter(item => item.length > 0)[0];
  }

  setPacktype() {
    const { packageId, cardsConfig } = this.props;
    const config = cardsConfig.find(cardConfig => cardConfig.id === packageId);
    if (config) {
      const packtype = config.config.packtype;
      this.setState({ packtype });
    }
  }

  renderMemoryGameLink() {
    const { packageId, appName } = this.props;
    const { packtype } = this.state;
    if (packtype === 'notes') return null;
    return (
      <Link
        to={`/${routeConstants[appName]}/package/${packageId}/${routeConstants.memoryGame}`}
        className="package-button">
        <Translation path='package_menu.memory_game'/>
      </Link>
    );
  }

  renderQuizGameLink() {
    const { packageId, appName } = this.props;
    const { packtype } = this.state;
    if (packtype === 'notes') return null;
    return (
      <Link
        to={`/${routeConstants[appName]}/package/${packageId}/${routeConstants.quiz}`}
        className="package-button">
        <Translation path='package_menu.quiz'/>
      </Link>
    );
  }

  renderBingoQuiz() {
    if (this.getAppName() === routeConstants.alphakm) return null;
    const { packageId, appName } = this.props;
    const { packtype } = this.state;
    if (packtype === 'notes') return null;
    return (
      <Link
        to={`/${routeConstants[appName]}/package/${packageId}/${routeConstants.bingoQuiz}`}
        className="package-button">
        Bingo
      </Link>
    );
  }

  renderShowAllLink() {
    const { packageId, appName } = this.props;
    return (
      <Link
        to={`/${routeConstants[appName]}/package/${packageId}/${routeConstants.reviewCards}`}
        className="package-button">
        <Translation path='package_menu.show_all_cards'/>
      </Link>
    );
  }

  renderPlayCards() {
    const { packageId, appName } = this.props;
    return (
      <Link
        to={`/${routeConstants[appName]}/package/${packageId}/${routeConstants.cardsCarousel}`}
        className="package-button">
        <Translation path='package_menu.play_the_cards'/>
      </Link>
    );
  }

  renderTranslationQuiz() {
    const { packageId, appName } = this.props;
    const { packtype } = this.state;
    if (packtype === 'notes' || packtype === 'alphabet_letters') return null;
    return (
      <Link
        to={`/${routeConstants[appName]}/package/${packageId}/${routeConstants.translationQuiz}`}
        className="package-button">
        Translation quiz
      </Link>
    );
  }

  renderShuffledAbc() {
    if (this.getAppName() === routeConstants.alphakm) return null;
    const { packageId, appName } = this.props;
    return (
      <Link
        to={`/${routeConstants[appName]}/package/${packageId}/${routeConstants.shuffledAbc}`}
        className="package-button">
        Shuffled ABC
      </Link>
    );
  }

  render() {
    return (
      <div className='col-xs-12 col-sm-6 col-md-4'>
        <div className="package-item">
          <div className="package-item-menu">
            {this.renderShowAllLink()}
            {this.renderPlayCards()}
            {this.renderQuizGameLink()}
            {this.renderMemoryGameLink()}
            {this.renderTranslationQuiz()}
            {this.renderBingoQuiz()}
            {this.renderShuffledAbc()}
          </div>
        </div>
      </div>
    );
  }
}

PackageListItemMenu.propTypes = {
  packageId: PropTypes.string.isRequired,
  appName: PropTypes.string.isRequired
};

const mapStateToProps = state => {
  return {
    cardsConfig: state.common.viewConfig.cardsConfig
  }
};

export default connect(mapStateToProps)(PackageListItemMenu);
