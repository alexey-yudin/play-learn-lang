import React, { Component, PropTypes } from 'react';
import CardListItem from './CardListItem';
import AudioAnswersContainer from '../shared/game_audio/AudioAnswersContainer';
import ImagePreloader from './ImagePreloader';
import QuizInfoPanel from './QuizInfoPanel';
import ScoreResults from './ScoreResults';
import QuizHelper from './QuizHelper';
import ModesContainer from '../modes/ModesContainer';
import Loader from '../shared/loader/Loader';
import './styles.scss';

class QuizPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resultCard: undefined,
      cardsForStage: [],
      currentStage: 0,
      countRightAnswers: 0,
      isWrongAnswerForCurrentStage: false,
      previouslySelectedResultCardsIds: []
    };
    this.compareResultWithProposed = this.compareResultWithProposed.bind(this);
    this.renderCard = this.renderCard.bind(this);
    this.renderStage = this.renderStage.bind(this);
    this.renderFirstStage = this.renderFirstStage.bind(this);
    this.setIsWrongAnswerForCurrentStageAs = this.setIsWrongAnswerForCurrentStageAs.bind(this);
    this.getCountOfStages = this.getCountOfStages.bind(this);
    this.playAgain = this.playAgain.bind(this);
    this.resetScoreSettings = this.resetScoreSettings.bind(this);
    this.isCardPreviouslySelected = this.isCardPreviouslySelected.bind(this);

    this.quizHelper = new QuizHelper();
    this.getRandomNumber = this.quizHelper.getRandomNumber;
    this.shuffleArray = this.quizHelper.shuffleArray;
    this.getRandomCardsForStage = this.quizHelper.getRandomCardsForStage;
    this.isCardProper = this.quizHelper.isCardProper;
  }

  componentDidMount() {
    this.props.fetchCards(this.props.cardsId);
    this.refs.loader.runOnImageLoad();
  }

  componentWillReceiveProps(nextProps) {
    const cards = nextProps.cards;
    if (Array.isArray(cards)) {
      this.renderFirstStage(cards);
    }
  }

  isCardPreviouslySelected(cardId) {
    if (this.state.previouslySelectedResultCardsIds.indexOf(cardId) === -1) {
      return false;
    } else {
      return true;
    }
  }

  pushCardIdToPreviouslySelected(cardId) {
    this.setState({
      previouslySelectedResultCardsIds: [...this.state.previouslySelectedResultCardsIds, cardId]
    });
  }

  removeCardIdFromPreviousSelected(cardIdToRemoveIndex = 0, lengthOfArray) {
    if (this.state.previouslySelectedResultCardsIds.length >= lengthOfArray) {
      const previouslySelectedResultCardsIds = this.state.previouslySelectedResultCardsIds.filter((cardId, index) => {
        return index !== cardIdToRemoveIndex;
      });
      this.setState({ previouslySelectedResultCardsIds });
    }
  }

  chooseResultCard(cards) {
    this.removeCardIdFromPreviousSelected(0, 6);
    let randomNumber = this.getRandomNumber(0, cards.length - 1);
    if (this.isCardPreviouslySelected(cards[randomNumber].id) === true) {
      while (this.isCardPreviouslySelected(cards[randomNumber].id) === true) {
        randomNumber = this.getRandomNumber(0, cards.length - 1);
      }
    } else if (!this.isCardProper(cards[randomNumber])) {
      while (!this.isCardProper(cards[randomNumber])) {
        randomNumber = this.getRandomNumber(0, cards.length - 1);
      }
    }
    this.pushCardIdToPreviouslySelected(cards[randomNumber].id);
    return cards[randomNumber];
  }

  renderStage(cards, currentStage) {
    const resultCard = this.chooseResultCard(cards);
    const cardsCount = parseInt(this.props.quizSettings.cardsCount);
    const randomCards = this.getRandomCardsForStage(cards, cardsCount, resultCard.id);
    let cardsForStage = [];
    cardsForStage.push(resultCard);
    cardsForStage = cardsForStage.concat(randomCards);
    cardsForStage = this.shuffleArray(cardsForStage);
    this.setState({ resultCard, cardsForStage, currentStage });
  }

  renderFirstStage(cards) {
    this.renderStage(cards, 1);
  }

  renderNextStage(cards) {
    this.setIsWrongAnswerForCurrentStageAs(false);
    const countOfStages = this.getCountOfStages();
    const nextStage = this.state.currentStage + 1;
    if (nextStage <= countOfStages) {
      this.renderStage(cards, nextStage);
    } else {
      this.showResults();
    }
  }

  compareResultWithProposed(proposedCardId, eventTarget) {
    if (proposedCardId) {
      if (proposedCardId === this.state.resultCard.id) {
        this.refs['correct-wrong-answer'].playCorrectAnswer();
        this.showMessage('success-message', eventTarget);
        $('.quiz-cards-list').fadeTo(1500, 0);
        if (this.state.isWrongAnswerForCurrentStage === false) {
          this.incrementCountOfRightAnswers();
        }
        setTimeout(() => {
          $('.quiz-cards-list').fadeTo(500, 1);
          this.renderNextStage(this.props.cards);
        }, 2000);
      } else {
        this.refs['correct-wrong-answer'].playWrongAnswer();
        this.showMessage('wrong-message', eventTarget);
        this.setIsWrongAnswerForCurrentStageAs(true);
      }
    }
  }

  resetScoreSettings() {
    this.setState({
      countRightAnswers: 0,
      isWrongAnswerForCurrentStage: false
    });
  }

  playAgain() {
    $('.quiz-cards-list').show('slow');
    $('.quiz-info-panel').show('slow');
    $('#score-results').hide('slow');
    this.resetScoreSettings();
    this.renderFirstStage(this.props.cards);
  }

  getCountOfStages() {
    const levelsCount = parseInt(this.props.quizSettings.levelsCount);
    if (Array.isArray(this.props.cards)) {
      if (isNaN(levelsCount)) {
        return this.props.cards.length;
      }
    }
    return levelsCount;
  }

  incrementCountOfRightAnswers() {
    const countRightAnswers = this.state.countRightAnswers += 1;
    this.setState({
      countRightAnswers: countRightAnswers
    });
  }

  setIsWrongAnswerForCurrentStageAs(boolValue) {
    this.setState({
      isWrongAnswerForCurrentStage: boolValue
    });
  }

  getTargetPosition(target) {
    return $(target).position();
  }

  showMessage(id, target) {
    const targetPosition = this.getTargetPosition(target);
    const leftPosition = targetPosition.left + $(target).width() / 3 + 10;
    $(`#${id}`).css({
      top: targetPosition.top,
      left: leftPosition
    });
    $(`#${id}`).show('fast');
    setTimeout(() => {
      this.hideMessage(id);
    }, 700);
  }

  hideMessage(id) {
    $(`#${id}`).hide('fast');
  }

  showResults() {
    $('.quiz-cards-list').hide();
    $('.quiz-info-panel').hide();
    $('#score-results').show('fast');
  }

  renderCardNonImage(card, index) {
    return (
      <CardListItem
        imageExist={false}
        key={index}
        cardsForStage={this.state.cardsForStage}
        card={card}
        onClickCallback={this.compareResultWithProposed}
        appName={this.props.appName}
        pageId={this.props.pageId}
        mode={this.props.mode}
      />
    );
  }

  renderCardWithImage(card, index) {
    return (
      <CardListItem
        imageExist={true}
        key={index}
        cardsForStage={this.state.cardsForStage}
        card={card}
        onClickCallback={this.compareResultWithProposed}
        appName={this.props.appName}
        pageId={this.props.pageId}
        mode={this.props.mode}
      />
    );
  }

  renderCard(card, index) {
    if (typeof card.image.url_600 === 'string' && card.image.url_600.length > 0) {
      return this.renderCardWithImage(card, index);
    } else {
      return this.renderCardNonImage(card, index);
    }
  }

  render() {
    const { cardsId } = this.props;
    return (
      <div>
        <Loader ref="loader"/>
        <div className="row">
          <div className="col-sm-12 col-md-10 col-md-offset-1">
            <ModesContainer
              title={'Quiz'}
              imageName={'quiz-icon.png'}
              imageHoverName={'quiz-icon-hover.png'}
              paramsId={cardsId}
            />
          </div>
        </div>
        <div className='row'>
          <div className="col-sm-12 col-md-10 col-md-offset-1">
            <div className="quiz-container">
              <div className="quiz-cards-list" id='quiz-cards-list'>
                {this.state.cardsForStage.map(this.renderCard.bind(this))}
              </div>

              <ScoreResults
                countRightAnswers={this.state.countRightAnswers}
                countOfStages={this.getCountOfStages()}
                onClickHandler={this.playAgain}
                playAgainId='quiz-play-again-btn'
              />

              <div className="quiz-messages">
                <div className="wrong-message" id='wrong-message'>
                  <span className="glyphicon glyphicon-remove"></span>
                </div>
                <div className="success-message" id='success-message'>
                  <span className="glyphicon glyphicon-star"></span>
                </div>
              </div>

              <div className="quiz-stage-count">
                {this.state.currentStage} of {this.getCountOfStages()}
              </div>
            </div>

            <QuizInfoPanel
              resultCard={this.state.resultCard}
              cardsForStage={this.state.cardsForStage}
              currentStage={this.state.currentStage}
              countOfStages={this.getCountOfStages()}
              mode={this.props.mode}
              packtype={this.props.packConfig.packtype}
            />
            <AudioAnswersContainer ref='correct-wrong-answer'/>
            <ImagePreloader cards={this.props.cards}/>
          </div>
        </div>
      </div>
    );
  }
}

QuizPage.propTypes = {
  cards: PropTypes.array.isRequired,
  fetchCards: PropTypes.func.isRequired,
  cardsId: PropTypes.string.isRequired,
  quizSettings: PropTypes.object.isRequired,
  appName: PropTypes.string.isRequired,
  mode: PropTypes.string.isRequired,
  pageId: PropTypes.string,
  packConfig: PropTypes.object
};

export default QuizPage;
