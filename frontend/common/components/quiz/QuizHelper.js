class QuizHelper {
  constructor() {
    this.getRandomCardsForStage = this.getRandomCardsForStage.bind(this);
  }

  getRandomNumber(start, end) {
    return _.random(start, end);
  }

  shuffleArray(array) {
    return _.shuffle(array);
  }

  isCardProper(card) {
    if (card['cfg-note_char'] !== 'true' && card['cfg-between_char'] !== 'true') {
      return true;
    } else {
      return false;
    }
  }

  isCardUnique(card, resultCardId, cards) {
    if (card.id !== resultCardId && cards.indexOf(card) === -1) {
      return true;
    } else {
      return false;
    }
  }

  getRandomCardsForStage(cards, amount, resultCardId) {
    let randomCards = [];

    if (cards.length <= amount) {
      amount = cards.length - 1;
    }

    while (randomCards.length + 1 < amount) {
      const randomNumber = this.getRandomNumber(0, cards.length - 1);
      const randomCard = cards[randomNumber];
      if (this.isCardUnique(randomCard, resultCardId, randomCards) && this.isCardProper(randomCard)) {
        randomCards.push(randomCard);
      }
    }

    return randomCards;
  }
}

export default QuizHelper;
