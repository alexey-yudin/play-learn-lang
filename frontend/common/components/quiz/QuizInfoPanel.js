import React, { Component, PropTypes } from 'react';
import CorrectWord from './CorrectWord';

class QuizInfoPanel extends Component {
  render() {
    if (this.props.resultCard) {
      return (
        <div className='quiz-info-panel'>
          <div>
            <CorrectWord
              resultCard={this.props.resultCard}
              cardsForStage={this.props.cardsForStage}
              mode={this.props.mode}
              packtype={this.props.packtype}
            />
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}

QuizInfoPanel.propTypes = {
  resultCard: PropTypes.object,
  cardsForStage: PropTypes.array.isRequired,
  currentStage: PropTypes.number.isRequired,
  countOfStages: PropTypes.number.isRequired,
  mode: PropTypes.string.isRequired,
  packtype: PropTypes.string
};

export default QuizInfoPanel;
