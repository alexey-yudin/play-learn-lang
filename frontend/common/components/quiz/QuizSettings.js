import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions/quizSettings';
import { DEFAULT_VARIANTS_FOR_LEVEL_COUNT, DEFAULT_VARIANTS_FOR_CARDS_COUNT } from '../../constants/quiz.js';

import Translation from '../shared/translations/Translation';

class QuizSettings extends Component {
  constructor(props) {
    super(props);
    this.handleOnClickLevelCount = this.handleOnClickLevelCount.bind(this);
    this.handleOnClickCardsCount = this.handleOnClickCardsCount.bind(this);
    this.renderLevelVariants = this.renderLevelVariants.bind(this);
    this.renderCardsVariants = this.renderCardsVariants.bind(this);
  }

  handleOnClickLevelCount(event) {
    const { appName } = this.props;
    const count = event.target.dataset.count;
    const quizCardList = $('#quiz-cards-list');
    const playAgainBtn = $('#quiz-play-again-btn');
    if (quizCardList.length === 1) {
      quizCardList.fadeTo(0, false);
      setTimeout(() => {
        this.props.setCountOfLevels(count, appName);
        playAgainBtn.trigger('click');
        quizCardList.fadeTo(700, true);
      }, 100);
    } else {
      this.props.setCountOfLevels(count, appName);
    }
  }

  handleOnClickCardsCount(event) {
    const { appName } = this.props;
    const count = event.target.dataset.count;
    const quizCardList = $('#quiz-cards-list');
    const playAgainBtn = $('#quiz-play-again-btn');
    if (quizCardList.length === 1) {
      quizCardList.fadeTo(0, false);
      setTimeout(() => {
        this.props.setCountOfCards(count, appName);
        playAgainBtn.trigger('click');
        quizCardList.fadeTo(700, true);
      }, 100);
    } else {
      this.props.setCountOfCards(count, appName);
    }
  }

  setAsCheckedCountOfLevels(count) {
    if (typeof this.props.quizSettings === 'object') {
      if (this.props.quizSettings.levelsCount === count) {
        return true;
      } else {
        return false;
      }
    }
  }

  setAsCheckedCountOfCards(count) {
    if (typeof this.props.quizSettings === 'object') {
      if (this.props.quizSettings.cardsCount === count) {
        return true;
      } else {
        return false;
      }
    }
  }

  renderLevelVariants(variant, index) {
    return (
      <div key={index} className="settings-button-item">
        <div
          className="settings-button"
          onClick={this.handleOnClickLevelCount}
          data-count={variant}>
          <div className={`${this.setAsCheckedCountOfLevels(variant) ? 'settings-button-selected' : ''}`}>
          </div>
        </div>
        <div>
          {variant}
        </div>
      </div>
    );
  }

  renderCardsVariants(variant, index) {
    return (
      <div key={index} className="settings-button-item">
        <div
          className="settings-button"
          onClick={this.handleOnClickCardsCount}
          data-count={variant.count}>
          <div className={`${this.setAsCheckedCountOfCards(variant.count) ? 'settings-button-selected' : ''}`}>
          </div>
        </div>
        <div>
          {variant.name}
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="quiz-settings">
        <div className="quiz-settings-item">
          <div>
            <Translation path='settings.quiz_cards_count'/>
          </div>
          <div className="settings-buttons-container">
            {DEFAULT_VARIANTS_FOR_LEVEL_COUNT.map(this.renderLevelVariants)}
          </div>
        </div>

        <div className="quiz-settings-item">
          <div>
            <Translation path='settings.quiz_level'/>
          </div>
          <div className="settings-buttons-container">
            {DEFAULT_VARIANTS_FOR_CARDS_COUNT.map(this.renderCardsVariants)}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    quizSettings: state.common.quizSettings,
    appName: state.common.generalSettings.meta.appName
  }
}

export default connect(mapStateToProps, {
  setCountOfLevels: actions.setCountOfLevels,
  setCountOfCards: actions.setCountOfCards
})(QuizSettings);
