import React, { Component, PropTypes } from 'react';

class ImagePreloader extends Component {
  preloadImage(card, index) {
    return (
      <img key={index} src={card.image.url_600} className='quiz-image-preload'/>
    );
  }

  preloadAllOfTheImages() {
    let cardsArray = [];
    if (Array.isArray(this.props.cards)) {
      cardsArray = this.props.cards.map(this.preloadImage);
    }
    return cardsArray;
  }

  render() {
    return (
      <div className="quiz-images-preload-list">
        {this.preloadAllOfTheImages()}
      </div>
    );
  }
}

ImagePreloader.propTypes = {
  cards: PropTypes.array.isRequired
};

export default ImagePreloader;
