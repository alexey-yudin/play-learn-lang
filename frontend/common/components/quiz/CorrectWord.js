import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as constants from '../../constants/quiz';
import * as localityConstants from '../../constants/localities';
import RomanisationWithTranslation from '../shared/romanization/RomanisationWithTranslation';
import RomanisedTranslation from '../shared/romanization/RomanisedTranslation';
import LocalityImage from '../shared/language/LocalityImage';
import AudioPlayer from '../shared/generalAudio/AudioPlayer';
import PlayAudio from '../shared/helpers/PlayAudio';

class CorrectWord extends Component {
  constructor(props) {
    super(props);
    this.handleAudioPlayButton = this.handleAudioPlayButton.bind(this);
    this.playAudio = new PlayAudio()
  }

  handleAudioPlayButton() {
    const cardId = this.props.resultCard.id;
    this.playAudio.playAudio(`quiz-game-correct-word${cardId}`);
  }

  renderTranslation(localityType) {
    const { selectedLocality, resultCard, mode, packtype } = this.props;
    const foreignLocality = selectedLocality.foreign;
    if (foreignLocality === 'km') {
      if (mode === constants.QUIZ_MODE) {
        if (packtype === 'alphabet_letters') {
          return (
            <RomanisedTranslation
              localityType={localityType}
              translations={resultCard.translations}
            />
          );
        } else {
          return (
            <RomanisationWithTranslation
              translations={resultCard.translations}
              localityType={localityType}
            />
          );
        }
      } else if (mode === constants.TRANSLATION_QUIZ_MODE) {
        return (
          <RomanisationWithTranslation
            translations={resultCard.translations}
            localityType={localityType}
          />
        );
      }
    } else {
      return (
        <RomanisationWithTranslation
          translations={resultCard.translations}
          localityType={localityType}
        />
      );
    }
  }

  render() {
    const { mode, resultCard } = this.props;
    if (resultCard) {
      if (mode === constants.QUIZ_MODE) {
        return (
          <div className="correct-word">
            <div>
              <LocalityImage
                localityType={localityConstants.FOREIGN_LOCALITY}
                className={'locality-image'}
              />
            </div>
            <div>
              {this.renderTranslation(localityConstants.FOREIGN_LOCALITY)}
            </div>
            <div onClick={this.handleAudioPlayButton} className="image-voice">
              <img src="/resources/icons/voice-icon.png" alt=""/>
              <AudioPlayer
                translations={resultCard.translations}
                localityType={localityConstants.FOREIGN_LOCALITY}
                generalId='quiz-game-correct-word'
                cardId={resultCard.id}
                autoPlay={true}
              />
            </div>
          </div>
        );
      } else if (mode === constants.TRANSLATION_QUIZ_MODE) {
        return (
          <div className="correct-word">
            <div>
              <LocalityImage
                localityType={localityConstants.NATIVE_LOCALITY}
                className={'locality-image'}
              />
            </div>
            <div>
              {this.renderTranslation(localityConstants.NATIVE_LOCALITY)}
            </div>
            <div onClick={this.handleAudioPlayButton} className="image-voice">
              <img src="/resources/icons/voice-icon.png" alt=""/>
              <AudioPlayer
                translations={resultCard.translations}
                localityType={localityConstants.NATIVE_LOCALITY}
                generalId='quiz-game-correct-word'
                cardId={resultCard.id}
                autoPlay={true}
              />
            </div>
          </div>
        );
      }
    } else {
      return (
        <div></div>
      );
    }
  }
}

CorrectWord.propTypes = {
  resultCard: React.PropTypes.object,
  cardsForStage: React.PropTypes.array,
  mode: React.PropTypes.string.isRequired,
  packtype: React.PropTypes.string
};

function mapStateToProps(state) {
  return {
    selectedLocality: state.common.localities.selectedLocality
  }
}

export default connect(mapStateToProps)(CorrectWord);
