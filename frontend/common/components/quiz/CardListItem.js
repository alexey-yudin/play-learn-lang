import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import * as constants from '../../constants/quiz';
import * as localityConstants from '../../constants/localities';

import TextColor from '../shared/helpers/TextColor';
import LanguageTranslation from '../shared/language/LanguageTranslation';
import RomanisationWithTranslation from '../shared/romanization/RomanisationWithTranslation';

class CardListItem extends Component {
  constructor(props) {
    super(props);
    this.textColor = new TextColor();
  }

  componentDidMount() {
    const { onClickCallback, card } = this.props;
    $(`#quiz-card-item${card.id}`).click(event => {
      const cardId = event.target.dataset.cardId;
      if (cardId) {
        onClickCallback(cardId, event.target);
      }
    });
    $(`#quiz-card-item${card.id}`).on('click', '*', event => {
      const cardId = this.refs['quiz-card-item'].dataset.cardId;
      if (cardId) {
        onClickCallback(cardId, event.target);
      }
    });
  }

  getFontSizeStyle() {
    const { selectedLocality } = this.props;
    if (selectedLocality.foreign === 'km') {
      return 'font-size-40px';
    } else {
      return 'font-size-20px';
    }
  }

  render() {
    const { mode, card, pageId, selectedLocality, imageExist } = this.props;
    if (mode === constants.QUIZ_MODE) {
      if (imageExist) {
        return (
          <div ref='quiz-card-item' className="quiz-card-item"
            id={`quiz-card-item${card.id}`} data-card-id={card.id}>
            <img src={card.image.url_600} alt={card.id} className='quiz-card-image'/>
          </div>
        );
      } else {
        return (
          <div ref='quiz-card-item'
            className={`quiz-card-item ${this.getFontSizeStyle()} `}
            id={`quiz-card-item${card.id}`} data-card-id={card.id}>
            <LanguageTranslation
              localityType={localityConstants.NATIVE_LOCALITY}
              translations={card.translations}
              className={this.textColor.setColorForText(pageId)}
            />
          </div>
        );
      }
    } else if (mode === constants.TRANSLATION_QUIZ_MODE) {
        return (
          <div ref='quiz-card-item'
            className={`quiz-card-item ${this.getFontSizeStyle()} `}
            id={`quiz-card-item${card.id}`} data-card-id={card.id}>
            <RomanisationWithTranslation
              translations={card.translations}
              localityType={localityConstants.FOREIGN_LOCALITY}
            />
          </div>
        );
    }
  }
}

const mapStateToProps = state => {
  return {
    selectedLocality: state.common.localities.selectedLocality
  }
}

CardListItem.propTypes = {
  imageExist: PropTypes.bool.isRequired,
  cardsForStage: PropTypes.array.isRequired,
  card: PropTypes.object.isRequired,
  onClickCallback: PropTypes.func.isRequired,
  appName: PropTypes.string.isRequired,
  mode: PropTypes.string.isRequired
}

export default connect(mapStateToProps)(CardListItem);
