import React, { Component, PropTypes } from 'react';

class ScoreResults extends Component {
  render() {
    return (
      <div className="score-results" id='score-results'>
        <h3>Score: {this.props.countRightAnswers} of {this.props.countOfStages}</h3>
        <button className='btn btn-default' onClick={this.props.onClickHandler} id={this.props.playAgainId}>
          Play again
        </button>
      </div>
    );
  }
}

ScoreResults.propTypes = {
  countRightAnswers: PropTypes.number.isRequired,
  countOfStages: PropTypes.number.isRequired,
  onClickHandler: PropTypes.func.isRequired,
  playAgainId: PropTypes.string.isRequired
}

export default ScoreResults;
