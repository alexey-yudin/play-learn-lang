import React, { Component, PropTypes } from 'react';
import Loader from '../shared/loader/Loader';

class CardsListItemNotes extends Component {
  constructor(props) {
    super(props);
    this.handleSoundOnClick = this.handleSoundOnClick.bind(this);
    this.renderDefinitionLong = this.renderDefinitionLong.bind(this);
    this.getDefinitionLongInXml = this.getDefinitionLongInXml.bind(this);
  }

  getParsedXml(xmlString) {
    if (window.DOMParser) {
      const parser = new DOMParser();
      return parser.parseFromString(xmlString, 'text/xml');
    } else {
      const xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
      xmlDoc.async = false;
      return xmlDoc.loadXML(xmlString);
    }
  }

  getDefinitionLongInXml() {
    const { card } = this.props;
    const definitionsLong = card.translations[1].definitions_long.translation;
    const xmlText = `<xml>${definitionsLong}</xml>`;
    return this.getParsedXml(xmlText);
  }

  getTextData(element, key) {
    return (
      <span key={key}>
        {element}
      </span>
    );
  }

  handleSoundOnClick(event) {
    event.preventDefault();
    const key = event.target.innerHTML.trim();
    const { card } = this.props;
    const embeddedAudios = card.translations[1].definitions_long.embeddedAudios;
    const audioPlayer = this.refs['audio-player'];
    const loader = this.refs['loader'];
    loader.process();
    embeddedAudios.some(audio => {
      if (audio.key === key) {
        if (audio.audioUrl) {
          audioPlayer.src = decodeURIComponent(audio.audioUrl);
          audioPlayer.onloadeddata = event => {
            loader.finish();
            audioPlayer.play();
          }
        }
      }
    });
  }

  getSoundData(element, key) {
    const { card } = this.props;
    const embeddedAudios = card.translations[0].definitions_long.embeddedAudios;
    let romanization = '';
    let translation = '';
    embeddedAudios.forEach(audio => {
      if (audio.key === element) {
        romanization = audio.romanisedtranslation;
        translation = audio.translation;
      }
    });
    if (romanization && translation) {
      return (
        <span key={key}>
          <a href="#"
            onClick={this.handleSoundOnClick}
            className='color-blue'>{element}</a> &nbsp;
          <span className='color-gray'>[{romanization}]</span> &nbsp;
          <span className='color-light-gray'>({translation})</span>
        </span>
      );
    } else if (romanization) {
      return (
        <span key={key}>
          <a href="#"
            onClick={this.handleSoundOnClick}
            className='color-blue'>{element}</a> &nbsp;
          <span className='color-gray'>[{romanization}]</span> &nbsp;
        </span>
      );
    } else if (translation) {
        <span key={key}>
          <a href="#"
            onClick={this.handleSoundOnClick}
            className='color-blue'>{element}</a> &nbsp;
          <span className='color-light-gray'>({translation})</span>
        </span>
    } else {
      return (
        <span key={key}>
          <a href="#"
            onClick={this.handleSoundOnClick}
            className='color-blue'>{element}</a>
        </span>
      );
    }
  }

  getRandomNumber() {
    return Math.floor(Math.random() * 100000);
  }

  renderDefinitionLong() {
    const definitionsLongInXml = this.getDefinitionLongInXml();
    const elements = definitionsLongInXml.getElementsByTagName('xml')[0].childNodes;
    let definitionsLong = [];
    for (let i = 0; i < elements.length; i++) {
      if (elements[i].nodeName === 'rule') {
        let ruleChildNodes = elements[i].childNodes;
        for (let j = 0; j < ruleChildNodes.length; j++) {
          if (ruleChildNodes[j].data) {
            let element = this.getTextData(ruleChildNodes[j].data, this.getRandomNumber());
            definitionsLong.push(element);
          } else if (ruleChildNodes[j].innerHTML) {
            let element = this.getSoundData(ruleChildNodes[j].innerHTML, this.getRandomNumber());
            definitionsLong.push(element);
          }
        }
      } else {
        if (elements[i].data) {
          let element = this.getTextData(elements[i].data, i);
          definitionsLong.push(element);
        } else if (elements[i].innerHTML) {
          let element = this.getSoundData(elements[i].innerHTML, i);
          definitionsLong.push(element);
        }
      }
    }
    return (
      <div>
        {definitionsLong}
      </div>
    );
  }

  getCardIndex() {
    const { card, cards } = this.props;
    return cards.map(cardItem => cardItem.id).indexOf(card.id) + 1;
  }

  renderDefinitionShort() {
    const { card, cards } = this.props;
    const definitionsShort = card.translations[1].definitions_short.translation;
    if (definitionsShort) {
      return (
        <div>
          {definitionsShort}
        </div>
      );
    } else {
      return (
        <div className='font-weight-bold'>
          {this.getCardIndex()} / {cards.length}
        </div>
      );
    }
  }

  render() {
    const { card } = this.props;
    return (
      <div className='khmer-definitions font-size-15px'>
        <div className='khmer-definition-short'>
          {this.renderDefinitionShort()}
        </div>
        <div className="font-size-50px">
          {card.translations[1].translation.translation}
        </div>
        <div className='khmer-definition-long'>
          {this.renderDefinitionLong()}
        </div>
        <audio src="" ref='audio-player'></audio>
        <Loader ref='loader'/>
      </div>
    );
  }
}

CardsListItemNotes.propTypes = {
  card: PropTypes.object.isRequired,
  cards: PropTypes.array.isRequired
}

export default CardsListItemNotes;
