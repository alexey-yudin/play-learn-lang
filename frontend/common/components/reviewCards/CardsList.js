import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { clearReviewCardsList } from '../../actions/reviewCards';
import ModesContainer from '../modes/ModesContainer';
import Loader from '../shared/loader/Loader';

class CardsList extends Component {
  componentWillMount() {
    this.props.fetchCards(this.props.cardsId);
  }

  componentWillUnmount() {
    this.props.clearReviewCardsList();
  }

  componentDidMount() {
    this.refs.loader.runOnImageLoad();
  }

  render() {
    const { cardsId } = this.props;
    return (
      <div>
        <Loader ref="loader"/>
        <ModesContainer
          title={'Review cards'}
          imageSrc={'/resources/icons/'}
          imageName={'show cards.png'}
          imageHoverName={'show cards.png'}
          paramsId={cardsId}
        />
        <div className='row'>
          {this.props.cards.map(this.props.renderCard)}
        </div>
      </div>
    );
  }
}

CardsList.propTypes = {
  fetchCards: PropTypes.func.isRequired,
  renderCard: PropTypes.func.isRequired,
  cardsId: PropTypes.string.isRequired,
  cards: PropTypes.array.isRequired
};

export default connect(null, {
  clearReviewCardsList
})(CardsList);
