import React, { Component } from 'react';
import { Link } from 'react-router';
import BackArrow from '../../../common/components/backArrow/BackArrow';
import SettingsContainer from '../../../common/components/settings/SettingsContainer';
import TranslationsContainer from '../../../common/components/translations/TranslationsContainer';
import LocalitiesDropdownList from '../../../common/components/localities/LocalitiesDropdownList';
import './style.scss';

class NavBar extends Component {
  render() {
    const { location } = this.props;
    return (
      <nav className="header">
        <div className="header-logo">
          <div className="navbar-item">
            <Link to="/" className="navbar-brand">
              <img src="/assets/learn-lang.png" alt="logo" className='logo-image'/>
            </Link>
            <Link to="/" className="logo-text">lyrebird</Link>
            <BackArrow pathname={location.pathname}/>
          </div>
        </div>
        <div className="header-localities">
          <LocalitiesDropdownList/>
        </div>
        <div className="navbar-item">
          <SettingsContainer/>
          <TranslationsContainer/>
        </div>
      </nav>
    );
  }
}

export default NavBar;
