import React, { Component } from 'react';
import { connect } from 'react-redux';

import CorrectWrongAnswer from './CorrectWrongAnswer';

class AudioAnswersContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      correctAnswer: 'correct-answer-audio',
      wrongAnswer: 'wrong-answer-audio'
    }
    this.playCorrectAnswer = this.playCorrectAnswer.bind(this);
    this.playWrongAnswer = this.playWrongAnswer.bind(this);
    this.stopCorrectAnswer = this.stopCorrectAnswer.bind(this);
    this.stopWrongAnswer = this.stopWrongAnswer.bind(this);
    this.stopAudio = this.stopAudio.bind(this);
  }

  resetAudioSrc(audio, src) {
    audio.attr('src', '');
    audio.attr('src', src);
  }

  playCorrectAnswer() {
    this.stopAudio();
    $(`#${this.state.correctAnswer}`).trigger('play');
  }

  playWrongAnswer() {
    this.stopAudio();
    $(`#${this.state.wrongAnswer}`).trigger('play');
  }

  stopCorrectAnswer() {
    const audio = $(`#${this.state.correctAnswer}`);
    const audioSrc = audio.attr('src');
    this.resetAudioSrc(audio, audioSrc);
  }

  stopWrongAnswer() {
    const audio = $(`#${this.state.wrongAnswer}`);
    const audioSrc = audio.attr('src');
    this.resetAudioSrc(audio, audioSrc);
  }

  stopAudio() {
    this.stopCorrectAnswer();
    this.stopWrongAnswer();
  }

  render() {
    return (
      <CorrectWrongAnswer correctAnswerId={this.state.correctAnswer} wrongAnswerId={this.state.wrongAnswer}/>
    );
  }
}

export default AudioAnswersContainer;
