import React, { Component } from 'react';
import { connect } from 'react-redux';

class CorrectAnswer extends Component {
  constructor(props) {
    super(props);
    this.setVolume = this.setVolume.bind(this);
  }

  componentDidMount() {
    this.setVolume(0.1);
  }

  componentDidUpdate() {
    this.setVolume(0.1);
  }

  setVolume(volumeValue) {
    const correctAnswer = this.refs['correctAnswer'];
    const wrongAnswer = this.refs['wrongAnswer'];
    if (correctAnswer && wrongAnswer) {
      correctAnswer.volume = volumeValue;
      wrongAnswer.volume = volumeValue;
    }
  }

  render() {
    if (this.props.audioSettingsEnable) {
      return (
        <div>
          <audio src='/correctanswer.mp3' ref='correctAnswer' id={this.props.correctAnswerId}></audio>
          <audio src='/wronganswer.mp3' ref='wrongAnswer' id={this.props.wrongAnswerId}></audio>
        </div>
      );
    } else {
      return (
        <div></div>
      );
    }
  }
}

CorrectAnswer.propTypes = {
  correctAnswerId: React.PropTypes.string.isRequired,
  wrongAnswerId: React.PropTypes.string.isRequired
}

function mapStateToProps(state) {
  return {
    audioSettingsEnable: state.common.generalSettings.gameAudio
  }
}

export default connect(mapStateToProps)(CorrectAnswer);
