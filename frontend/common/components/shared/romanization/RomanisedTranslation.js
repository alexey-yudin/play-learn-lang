import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as localityConstants from '../../../constants/localities';

class RomanisedTranslation extends Component {
  renderRomanisation() {
    const { selectedLocality, translations, localityType, romanisedEnabled } = this.props;
    let romanisedTranslation;

    if (localityType === localityConstants.FOREIGN_LOCALITY) {
      translations.forEach(translationItem => {
        if (translationItem.language === selectedLocality.foreign) {
          if (translationItem.translation.romanisedtranslation) {
            romanisedTranslation = translationItem.translation.romanisedtranslation;
          }
        }
      });
    } else if (localityType === localityConstants.NATIVE_LOCALITY) {
      translations.forEach(translationItem => {
        if (translationItem.language === selectedLocality.native) {
          if (translationItem.translation.romanisedtranslation) {
            romanisedTranslation = translationItem.translation.romanisedtranslation;
          }
        }
      });
    }

    if (romanisedTranslation && romanisedEnabled) {
      romanisedTranslation = romanisedTranslation.replace('@or', '/');
      return (
        <span>({romanisedTranslation})</span>
      );
    }
  }

  render() {
    return (
      <span>
        {this.renderRomanisation()}
      </span>
    );
  }
}

RomanisedTranslation.propTypes = {
  translations: PropTypes.array.isRequired,
  localityType: PropTypes.string.isRequired
}

function mapStateToProps(state) {
  return {
    selectedLocality: state.common.localities.selectedLocality,
    romanisedEnabled: state.common.generalSettings.romanisation
  }
}

export default connect(mapStateToProps)(RomanisedTranslation);
