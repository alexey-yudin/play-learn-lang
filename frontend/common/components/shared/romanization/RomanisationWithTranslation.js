import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import * as localityConstants from '../../../constants/localities';
import RomanisedTranslation from './RomanisedTranslation';
import LanguageTranslation from '../language/LanguageTranslation';

class RomanisationWithTranslation extends Component {
  romanisationExist() {
    const { selectedLocality, translations, localityType } = this.props;
    let exist = false;

    if (localityType === localityConstants.FOREIGN_LOCALITY) {
      translations.forEach(translationItem => {
        if (translationItem.language === selectedLocality.foreign) {
          if (translationItem.translation.romanisedtranslation) {
            exist = true;
          }
        }
      });
    } else if (localityType === localityConstants.NATIVE_LOCALITY) {
      translations.forEach(translationItem => {
        if (translationItem.language === selectedLocality.native) {
          if (translationItem.translation.romanisedtranslation) {
            exist = true;
          }
        }
      });
    }

    return exist;
  }

  render() {
    const { translations, romanisedEnabled, localityType, selectedLocality } = this.props;
    if (this.romanisationExist() && romanisedEnabled) {
      return (
        <div style={{display: 'inline'}}>
          <table style={{display: 'inline'}}>
            <tbody>
              <tr>
                <td>
                  <LanguageTranslation
                    localityType={localityType}
                    translations={translations}
                  />
                </td>
              </tr>
              <tr>
                <td className='font-size-15px'>
                  <RomanisedTranslation
                    localityType={localityType}
                    translations={translations}
                  />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      );
    } else {
      return (
        <LanguageTranslation
          localityType={localityType}
          translations={translations}
        />
      );
    }
  }
}

RomanisationWithTranslation.propTypes = {
  translations: PropTypes.array.isRequired,
  localityType: PropTypes.string.isRequired
}

function mapStateToProps(state) {
  return {
    selectedLocality: state.common.localities.selectedLocality,
    romanisedEnabled: state.common.generalSettings.romanisation
  }
}

export default connect(mapStateToProps)(RomanisationWithTranslation);
