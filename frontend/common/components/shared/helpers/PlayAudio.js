class PlayAudio {
  playAudio(audioId) {
    const audio = $(`#${audioId}`);
    const audioSrc = audio.attr('src');
    audio.attr('src', '');
    audio.attr('src', audioSrc);
    audio.trigger('play');
  }
}

export default PlayAudio;
