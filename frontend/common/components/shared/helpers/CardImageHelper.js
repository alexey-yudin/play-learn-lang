class CardImageHelper {
  cardHasImage(card) {
    if (card && card.image && typeof card.image.url_600 === 'string' && card.image.url_600.length > 0) {
      return true;
    } else {
      return false;
    }
  }
}

export default CardImageHelper;
