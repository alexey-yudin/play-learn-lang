class TextColor {
  setColorForText(pageId) {
    if (pageId.indexOf('consonants_sub') > -1) {
      return 'color-green';
    } else if (pageId.indexOf('consonants') > -1) {
      return 'color-blue';
    } else if (pageId.indexOf('vowels_independent') > -1) {
      return 'color-red';
    } else if (pageId.indexOf('vowels_dependant_consonant') > -1) {
      return 'color-blue';
    } else if (pageId.indexOf('vowels') > -1) {
      return 'color-yellow';
    } else if (pageId.indexOf('diacritics') > -1) {
      return 'color-dark-red';
    } else if (pageId.indexOf('numbers') > -1) {
      return 'color-pink';
    } else {
      return 'color-default';
    }
  }
}

export default TextColor;
