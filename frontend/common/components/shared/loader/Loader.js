import React, { Component } from 'react';
import './styles.scss';

class Loader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      process: false,
      finish: false
    };
    this.runOnImageLoad = this.runOnImageLoad.bind(this);
  }

  runOnImageLoad() {
    this.process();
    setTimeout(() => {
      let count = 0;
      let countOfCompleteImages = 0;
      let countOfFlagImages = 0;
      const images = $('.container').find('img');
      const countOfImages = images.length;
      images.each((index, image) => {
        if (image.complete) {
          countOfCompleteImages++;
          if ($(image).height() <= 30) {
            countOfFlagImages++;
          }
        }
        $(image).load(() => {
          count++;
          if (count === countOfImages) {
            this.finish();
          }
        });
      });
      if (countOfCompleteImages > countOfImages - countOfFlagImages) {
        this.finish();
      }
      if (countOfCompleteImages === countOfImages) {
        this.finish();
      }
    }, 150);
    setTimeout(() => {
      this.finish();
    }, 8000);
  }

  process() {
    this.setState({ process: true, finish: false });
  }

  finish() {
    this.setState({ process: false, finish: true });
  }

  getClassName() {
    if (this.state.process) {
      return 'process';
    } else if (this.state.finish) {
      return 'finish';
    }
  }

  render() {
    return (
      <div className={`loader ${this.getClassName()}`}>
      </div>
    );
  }
}

export default Loader;
