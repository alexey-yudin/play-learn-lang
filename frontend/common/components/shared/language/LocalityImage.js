import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as localityConstants from '../../../constants/localities';

class LocalityImage extends Component {
  renderLocalityImage() {
    const { localityType, foreignLocalities, nativeLocalities, selectedLocality, className } = this.props;
    let localityImageUrl;
    if (localityType === localityConstants.FOREIGN_LOCALITY) {
      foreignLocalities.forEach(locality => {
        if (locality.id === selectedLocality.foreign) {
          localityImageUrl = locality.imageUrl;
        }
      });
    } else if (localityType === localityConstants.NATIVE_LOCALITY) {
      nativeLocalities.forEach(locality => {
        if (locality.id === selectedLocality.native) {
          localityImageUrl = locality.imageUrl;
        }
      });
    }

    if (localityImageUrl) {
      return <img src={localityImageUrl} alt='image' className={className}/>
    } else {
      return null;
    }
  }

  render() {
    return <span>{this.renderLocalityImage()}</span>
  }
}

LocalityImage.propTypes = {
  localityType: PropTypes.string.isRequired,
  className: PropTypes.string
}

const mapStateToProps = state => {
  return {
    foreignLocalities: state.common.localities.localities.foreign,
    nativeLocalities: state.common.localities.localities.native,
    selectedLocality: state.common.localities.selectedLocality
  }
}

export default connect(mapStateToProps)(LocalityImage);
