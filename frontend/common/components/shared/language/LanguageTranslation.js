import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as localityConstants from '../../../constants/localities';

class LanguageTranslation extends Component {
  renderLanguage() {
    const { localityType, translations, selectedLocality, upperCaseEnabled, isPackage } = this.props;
    let languageTranslation = '';

    if (localityType === localityConstants.FOREIGN_LOCALITY) {
      translations.forEach(translationItem => {
        if (translationItem.language === selectedLocality.foreign) {
          if (isPackage) {
            languageTranslation = translationItem.translation;
          } else {
            languageTranslation = translationItem.translation.translation;
          }
        }
      });
    } else if (localityType === localityConstants.NATIVE_LOCALITY) {
      translations.forEach(translationItem => {
        if (translationItem.language === selectedLocality.native) {
          if (isPackage) {
            languageTranslation = translationItem.translation;
          } else {
            languageTranslation = translationItem.translation.translation;
          }
        }
      });
    }

    return upperCaseEnabled ? languageTranslation.toUpperCase() : languageTranslation;
  }

  render() {
    const { className } = this.props;
    return (
      <span className={className}>
        {this.renderLanguage()}
      </span>
    );
  }
}

LanguageTranslation.propTypes = {
  translations: PropTypes.array.isRequired,
  localityType: PropTypes.string.isRequired,
  className: PropTypes.string,
  isPackage: PropTypes.bool
}

const mapStateToProps = state => {
  return {
    upperCaseEnabled: state.common.generalSettings.upperCase,
    selectedLocality: state.common.localities.selectedLocality
  }
}

export default connect(mapStateToProps)(LanguageTranslation);
