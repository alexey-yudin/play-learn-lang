import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

class Translation extends Component {
  parseThePath(path) {
    if (path) {
      path = path.split('.');
      return path;
    }
  }

  getTheValueFromTheParsedPath(parsedPath) {
    const selectedTranslation = this.props.selectedTranslation;
    if (selectedTranslation && Array.isArray(parsedPath)) {
      if (parsedPath.length === 2) {
        return selectedTranslation[parsedPath[0]][parsedPath[1]];
      } else if (parsedPath.length === 1) {
        if (isNaN(parseInt(parsedPath[0]))) {
          return selectedTranslation[parsedPath[0]];
        } else {
          return parsedPath[0];
        }
      }
    }
  }

  render() {
    if (this.props.selectedTranslation.id) {
      return (
        <span>
          {this.getTheValueFromTheParsedPath(this.parseThePath(this.props.path))}
        </span>
      );
    } else {
      return (
        <span></span>
      );
    }
  }
}

Translation.propTypes = {
  path: PropTypes.string.isRequired
}

function mapStateToProps(state) {
  return {
    selectedTranslation: state.common.translation.selectedTranslation
  }
}

export default connect(mapStateToProps)(Translation);
