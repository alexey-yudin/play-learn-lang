import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as localityConstants from '../../../constants/localities';

class AudioPlayer extends Component {
  getAudioSrc(lang, cardTranslations) {
    const { selectedLocality, translations, localityType } = this.props;
    let audioUrl = '';

    if (localityType === localityConstants.FOREIGN_LOCALITY) {
      translations.forEach(translationItem => {
        if (translationItem.language === selectedLocality.foreign) {
          if (translationItem.translation.audio) {
            audioUrl = translationItem.translation.audio.audioUrl;
          }
        }
      });
    } else if (localityType === localityConstants.NATIVE_LOCALITY) {
      translations.forEach(translationItem => {
        if (translationItem.language === selectedLocality.native) {
          if (translationItem.translation.audio) {
            audioUrl = translationItem.translation.audio.audioUrl;
          }
        }
      });
    }

    return decodeURIComponent(audioUrl);
  }

  render() {
    const { generalAudioEnabled, autoPlay, cardId, generalId } = this.props;
    if (generalAudioEnabled) {
      return (
        <audio src={this.getAudioSrc()} autoPlay={autoPlay} id={`${generalId}${cardId}`}></audio>
      );
    } else {
      return null;
    }
  }
}

AudioPlayer.propTypes = {
  translations: PropTypes.array.isRequired,
  localityType: PropTypes.string.isRequired,
  generalId: PropTypes.string.isRequired,
  cardId: PropTypes.string.isRequired,
  autoPlay: PropTypes.bool
}

function mapStateToProps(state) {
  return {
    selectedLocality: state.common.localities.selectedLocality,
    generalAudioEnabled: state.common.generalSettings.generalAudio
  }
}

export default connect(mapStateToProps)(AudioPlayer);
