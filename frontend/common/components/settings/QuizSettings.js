import React, { Component } from 'react';

import QuizSettings from '../quiz/QuizSettings';
import Translation from '../shared/translations/Translation';

class QuizSettingsContainer extends Component {
  render() {
    return (
      <div className='quiz-settings-container'>
        <div className="quiz-settings-title">
          <Translation path='settings.quiz'/>
        </div>
        <div>
          <QuizSettings/>
        </div>
      </div>
    );
  }
}

export default QuizSettingsContainer;
