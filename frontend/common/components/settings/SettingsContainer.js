import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/generalSettings';
import * as quizSettingsActions from '../../actions/quizSettings'
import * as memoryGameSettings from '../../actions/memoryGameSettings';
import * as bingoSettingsActions from '../../actions/bingoQuiz';
import QuizSettings from './QuizSettings';
import BingoQuizSettings from './BingoQuizSettings';
import MemoryGameSettings from './MemoryGameSettings';
import GameAudioSettings from './GameAudioSettings';
import RomanisedSettings from './RomanisedSettings';
import GeneralAudioSettings from './GeneralAudioSettings';
import AuthSettings from './AuthSettings';
import UpperCase from './UpperCase';
import Translation from '../shared/translations/Translation';
import './styles.scss';

class SettingsContainer extends Component {
  constructor(props) {
    super(props);
    this.toggleDropdown = this.toggleDropdown.bind(this);
  }

  componentWillMount() {
    const { appName } = this.props;
    this.props.setDefaultSettings(appName);
    this.props.setDefaultQuizSettings(appName);
    this.props.setDefaultBingoQuizSize(appName);
    this.props.setDefaultMemoryGameSettings(appName);
  }

  componentDidMount() {
    this.closeOnOutsideClick();
  }

  closeOnOutsideClick() {
    $(document).on('click', event => {
      const target = event.target;
      if (!$(target).closest(this.refs.dropdown).length) {
        this.closeDropdown();
      }
    });
  }

  closeDropdown() {
    $(this.refs.dropdown).removeClass('open');
  }

  toggleDropdown() {
    $(this.refs.dropdown).toggleClass('open');
  }

  getLinkStyle() {
    return {
      backgroundImage: 'url("/resources/icons/Settings.png")',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'left top',
      backgroundSize: '20px 20px'
    }
  }

  render() {
    return (
      <div className="dropdown" ref="dropdown">
        <div
          style={this.getLinkStyle()}
          onClick={this.toggleDropdown}
          className="settings-link">
          <Translation path='settings.settings'/>
        </div>
        <ul className="dropdown-menu pull-right settings-container" style={{padding: '30px'}}>
          <div className="triangle" style={{right: '43%'}}></div>
          <QuizSettings/>
          <BingoQuizSettings/>
          <MemoryGameSettings/>
          <GameAudioSettings/>
          <RomanisedSettings/>
          <GeneralAudioSettings/>
          <UpperCase/>
          <AuthSettings/>
          <div className="pull-right">
            {this.props.appVersion.$t}
          </div>
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    appName: state.common.generalSettings.meta.appName,
    appVersion: state.common.generalSettings.meta.appVersion
  }
};

export default connect(mapStateToProps, {
  setDefaultSettings: actions.setDefaultSettings,
  setDefaultQuizSettings: quizSettingsActions.setDefaultSettings,
  setDefaultMemoryGameSettings: memoryGameSettings.setDefaultSettings,
  setDefaultBingoQuizSize: bingoSettingsActions.setDefaultSize
})(SettingsContainer);
