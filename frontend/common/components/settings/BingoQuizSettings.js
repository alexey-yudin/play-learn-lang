import React, { Component } from 'react';
import BingoQuizSettings from '../bingoQuiz/BingoQuizSettings';
import Translation from '../shared/translations/Translation';

class BingoQuizSettingsContainer extends Component {
  render() {
    return (
      <div className="settings-menu-element" style={{marginBottom: '30px'}}>
        <div className="settings-title">
          Bingo Quiz
        </div>
        <BingoQuizSettings/>
      </div>
    );
  }
}

export default BingoQuizSettingsContainer;
