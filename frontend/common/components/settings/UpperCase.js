import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions/generalSettings';

class UpperCase extends Component {
  constructor(props) {
    super(props);
    this.changeUpperCase = this.changeUpperCase.bind(this);
  }

  changeUpperCase() {
    const { upperCaseEnabled, appName } = this.props;
    if (upperCaseEnabled) this.props.disableUpperCase(appName);
    else this.props.enableUpperCase(appName);
  }

  render() {
    const { upperCaseEnabled } = this.props;
    return (
      <div className="settings-item">
        <div className="settings-title">
          Upper case
        </div>
        <label className="audio-settings-switch">
          <input type="checkbox" checked={upperCaseEnabled} onChange={this.changeUpperCase}/>
          <div className="audio-settings-slider"></div>
        </label>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    appName: state.common.generalSettings.meta.appName,
    upperCaseEnabled: state.common.generalSettings.upperCase
  }
}

export default connect(mapStateToProps, {
  enableUpperCase: actions.enableUpperCase,
  disableUpperCase: actions.disableUpperCase
})(UpperCase);
