import React, { Component } from 'react';

import MemoryGameSettings from '../memoryGame/MemoryGameSettings';
import Translation from '../shared/translations/Translation';

class MemoryGameSettingsContainer extends Component {
  render() {
    return (
      <div className='settings-menu-element' style={{marginBottom: '30px'}}>
        <div className="settings-title">
          <Translation path='settings.memory_game'/>
        </div>
        <MemoryGameSettings/>
      </div>
    );
  }
}

export default MemoryGameSettingsContainer;
