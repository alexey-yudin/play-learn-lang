import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions/generalSettings';
import Translation from '../shared/translations/Translation';

class RomanisedSettings extends Component {
  constructor(props) {
    super(props);
    this.toggleRomanised = this.toggleRomanised.bind(this);
  }

  toggleRomanised() {
    const { romanisedEnabled, appName } = this.props;
    if (romanisedEnabled) this.props.disableRomanised(appName);
    else this.props.enableRomanised(appName);
  }

  render() {
    const { romanisedEnabled } = this.props;
    return (
      <div className="settings-item">
        <div className="settings-title">
          <Translation path='settings.romanised'/>
        </div>
        <label className="audio-settings-switch">
          <input type="checkbox" checked={romanisedEnabled} onChange={this.toggleRomanised}/>
          <div className="audio-settings-slider"></div>
        </label>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    appName: state.common.generalSettings.meta.appName,
    romanisedEnabled: state.common.generalSettings.romanisation
  }
}

export default connect(mapStateToProps, {
  enableRomanised: actions.enableRomanised,
  disableRomanised: actions.disableRomanised
})(RomanisedSettings);
