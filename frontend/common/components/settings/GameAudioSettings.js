import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions/generalSettings';
import Translation from '../shared/translations/Translation';

class GameAudioSettings extends Component {
  constructor(props) {
    super(props);
    this.changeAudio = this.changeAudio.bind(this);
  }

  changeAudio() {
    const { audioSettingsEnabled, appName } = this.props;
    if (audioSettingsEnabled) this.props.disableAudioAnswers(appName);
    else this.props.enableAudioAnswers(appName);
  }

  render() {
    const { audioSettingsEnabled } = this.props;
    return (
      <div className="settings-item">
        <div className="settings-title">
          <Translation path='settings.game_audio'/>
        </div>
        <label className="audio-settings-switch">
          <input type="checkbox" checked={audioSettingsEnabled} onChange={this.changeAudio}/>
          <div className="audio-settings-slider"></div>
        </label>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    appName: state.common.generalSettings.meta.appName,
    audioSettingsEnabled: state.common.generalSettings.gameAudio
  }
}

export default connect(mapStateToProps, {
  enableAudioAnswers: actions.enableAudioAnswers,
  disableAudioAnswers: actions.disableAudioAnswers
})(GameAudioSettings);
