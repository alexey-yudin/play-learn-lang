import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions/generalSettings';
import Translation from '../shared/translations/Translation';

class GeneralAudioSettings extends Component {
  constructor(props) {
    super(props);
    this.changeGeneralAudio = this.changeGeneralAudio.bind(this);
  }

  changeGeneralAudio() {
    const { generalAudioEnabled, appName } = this.props;
    if (generalAudioEnabled) this.props.disableGeneralAudio(appName);
    else this.props.enableGeneralAudio(appName);
  }

  render() {
    const { generalAudioEnabled } = this.props;
    return (
      <div className="settings-item">
        <div className="settings-title">
          <Translation path='settings.general_audio'/>
        </div>
        <label className="audio-settings-switch">
          <input type="checkbox" checked={generalAudioEnabled} onChange={this.changeGeneralAudio}/>
          <div className="audio-settings-slider"></div>
        </label>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    appName: state.common.generalSettings.meta.appName,
    generalAudioEnabled: state.common.generalSettings.generalAudio
  }
}

export default connect(mapStateToProps, {
  enableGeneralAudio: actions.enableGeneralAudio,
  disableGeneralAudio: actions.disableGeneralAudio
})(GeneralAudioSettings);
