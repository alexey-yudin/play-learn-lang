import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as authActions from '../../actions/auth';
import {browserHistory} from 'react-router';
import * as authSelectors from '../../selectors/auth';

class AuthSettings extends Component {
  logout = event => {
    event.preventDefault();
    this.props.logout();
    browserHistory.push('/');
  };

  renderImage() {
    const {isAuthenticated, user} = this.props;
    if (!isAuthenticated || !user) return;

    if (user.photoURL) {
      return (
        <div>
          <img className="user-photo" src={user.photoURL} alt=""/>
        </div>
      );
    } else {
      return null;
    }
  }

  renderUserName() {
    const {isAuthenticated, user} = this.props;
    if (!isAuthenticated) return;

    if (user.displayName) {
      return (
        <div className="user-info settings-item">
          <div>
            Logged in as {user.displayName}
          </div>
          {this.renderImage()}
        </div>
      );
    } else {
      return (
        <div className="user-info">
          Logged in
        </div>
      );
    }
  }

  render() {
    return (
      <div className="user-info-container">
        {this.renderUserName()}
        <a href="#" style={{color: '#000'}} onClick={this.logout}>Logout</a>
      </div>
    );
  }
}

export default connect(state => ({
  isAuthenticated: authSelectors.getIsAuthenticated(state),
  user: authSelectors.getUser(state)
}), {
  logout: authActions.logout
})(AuthSettings);
