import React, { Component } from 'react';
import {connect} from 'react-redux';
import * as authActions from '../../actions/auth';
import * as firebase from 'firebase';
import './styles.scss';

class Auth extends Component {
  constructor(props) {
    super(props);
    this.facebookProvider = null;
    this.googleProvider = null;
    this.twitterProvider = null;
  }

  componentWillMount() {
    this.facebookProvider = new firebase.auth.FacebookAuthProvider();
    this.googleProvider = new firebase.auth.GoogleAuthProvider();
    this.twitterProvider = new firebase.auth.TwitterAuthProvider();
  }

  twitterLogin = () => {
    this.props.socialLogin(this.twitterProvider);
  };

  googleLogin = () => {
    this.props.socialLogin(this.googleProvider);
  };

  facebookLogin = () => {
    this.props.socialLogin(this.facebookProvider);
  }

  render() {
    return (
      <div className="auth">
        <div className="auth-buttons">
          <button type="button" className="btn btn-default social-btn google-btn" onClick={this.googleLogin}>
            Login with google
          </button>
          <button type="button" className="btn btn-default social-btn twitter-btn" onClick={this.twitterLogin}>
            Login with twitter
          </button>
          <button type="button" className="btn btn-default social-btn facebook-btn" onClick={this.facebookLogin}>
            Login with facebook
          </button>
        </div>
      </div>
    );
  }
}

export default connect(null, {
  socialLogin: authActions.socialLogin
})(Auth);
