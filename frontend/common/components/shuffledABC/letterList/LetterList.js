import React, { Component, PropTypes } from 'react';
import LetterListItem from './LetterListItem';
import { DropTarget } from 'react-dnd';

const letterListTarget = {
  drop(props, monitor, component) {
    const item = monitor.getItem();
    const offset = monitor.getClientOffset();
    props.removeFromDropped(item.letterItem);
    if (item.isHolder) props.replaceLetterItem(item.letterItem, offset);
  }
};

const collect = (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  getItem: monitor.getItem()
});

class LetterList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: null,
      height: null
    };
    this.item = null;
    this.renderLetter = this.renderLetter.bind(this);
  }

  componentDidMount() {
    const width = $(this.item).width() - 50;
    const height = $(this.item).height() - 50;
    this.setState({ width, height });
  }

  renderLetter(letterItem, index) {
    if (!this.state.width && !this.state.height) return null;
    return (
      <LetterListItem
        key={index}
        containerWidth={this.state.width}
        containerHeight={this.state.height}
        letterItem={letterItem}
        isVisible={!letterItem.isDropped}
        label={this.props.label}
        removeOffset={this.props.removeOffset}
      />
    );
  }

  render() {
    const { label, connectDropTarget, isOver, getItem, letters } = this.props;
    if (!label) return null;
    return connectDropTarget(
      <div ref={item => this.item = item} className="letter-list">
        {letters.map(this.renderLetter)}
      </div>
    );
  }
}

LetterList.propTypes = {
  label: PropTypes.string.isRequired,
  letters: PropTypes.array.isRequired,
  removeFromDropped: PropTypes.func.isRequired,
  replaceLetterItem: PropTypes.func.isRequired,
  removeOffset: PropTypes.func.isRequired
};

export default DropTarget('letter', letterListTarget, collect)(LetterList);
