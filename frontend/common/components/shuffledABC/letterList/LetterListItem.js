import React, { Component, PropTypes } from 'react';
import { createDragPreview } from 'react-dnd-text-dragpreview';
import { DragSource } from 'react-dnd';

const letterSource = {
  beginDrag(props) {
    return props;
  }
};

const collect = (connect, monitor) => {
  return {
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging(),
    didDrop: monitor.didDrop(),
    dropResult: monitor.getDropResult()
  };
};

const dragPreviewStyle = {
  backgroundColor: '#fff',
  borderColor: '#000',
  color: '#000',
  fontSize: 38,
  paddingTop: 17,
  paddingRight: 12,
  paddingBottom: 17,
  paddingLeft: 12
}

class LetterListItem extends Component {
  constructor(props) {
    super(props);
    this.item = null;
  }

  componentDidMount() {
    const { letterItem, containerWidth, containerHeight } = this.props;
    this.dragPreview = createDragPreview(letterItem.letter, dragPreviewStyle);
    this.props.connectDragPreview(this.dragPreview);
    this.setLeftAndTop();
    const outerWidth = $(document).width() - containerWidth;
    const outerHeight = $(document).height() - containerHeight;
    $(this.item).on('dragend', event => {
      if (!this.item) return;
      this.item.style.left = `${event.clientX - outerWidth / 2}px`;
      this.item.style.top = `${event.clientY - outerHeight / 2 - 20}px`;
    });
  }

  componentWillReceiveProps(nextProps, props) {
    if (nextProps.label !== this.props.label) {
      this.setLeftAndTop();
    }
  }

  componentDidUpdate() {
    const { letterItem, containerWidth, containerHeight } = this.props;
    this.dragPreview = createDragPreview(letterItem.letter, dragPreviewStyle, this.dragPreview);
    if (letterItem.offset) {
      const outerWidth = $(document).width() - containerWidth;
      const outerHeight = $(document).height() - containerHeight;
      this.item.style.left = `${letterItem.offset.x - outerWidth / 2}px`;
      this.item.style.top = `${letterItem.offset.y - outerHeight / 2 - 20}px`;
      this.props.removeOffset();
    }
  }

  setLeftAndTop() {
    const { containerWidth, containerHeight } = this.props;
    let randomLeft = Math.floor(Math.random() * parseInt(containerWidth) - 10 + 1) + 10;
    let randomTop = Math.floor(Math.random() * parseInt(containerHeight) - 10 + 1);
    if (randomTop + 70 > containerHeight) {
      randomTop -= 20;
    }
    this.item.style.left = `${randomLeft}px`;
    this.item.style.top = `${randomTop}px`;
  }

  getDisplayStyle() {
    const { isVisible } = this.props;
    if (!isVisible) {
      return {
        display: 'none'
      }
    }
  }

  render() {
    const { letterItem, connectDragSource, isDragging } = this.props;
    if (isDragging) {
      return connectDragSource(
        <div
          style={{opacity: '0'}}
          ref={item => this.item = item}
          className="letter-list__item">
          {letterItem.letter}
        </div>
      );
    } else {
      return connectDragSource(
        <div
          style={this.getDisplayStyle()}
          ref={item => this.item = item}
          className="letter-list__item">
          {letterItem.letter}
        </div>
      );
    }
  }
}

LetterListItem.propTypes = {
  letterItem: PropTypes.object.isRequired,
  containerWidth: PropTypes.number.isRequired,
  containerHeight: PropTypes.number.isRequired,
  isVisible: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired,
  removeOffset: PropTypes.func.isRequired
};

export default DragSource('letter', letterSource, collect)(LetterListItem);
