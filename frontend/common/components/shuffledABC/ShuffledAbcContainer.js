import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ShuffledAbcPreview from './preview/ShuffledAbcPreview';
import LetterHolderList from './letterHolder/LetterHolderList';
import LetterList from './letterList/LetterList';
import ModesContainer from '../modes/ModesContainer';
import ShuffledAbcMenu from './ShuffledAbcMenu';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import './styles.scss';

class ShuffledAbcContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      card: null,
      label: null,
      labelLength: 0,
      countOfCorrectLetters: 0,
      letters: [],
      droppedIndexes: [],
      currentRandomNumber: null
    };
    this.restartGame = this.restartGame.bind(this);
    this.isLabelCorrect = this.isLabelCorrect.bind(this);
    this.addToDropped = this.addToDropped.bind(this);
    this.removeFromDropped = this.removeFromDropped.bind(this);
    this.replaceLetterItem = this.replaceLetterItem.bind(this);
    this.removeOffset = this.removeOffset.bind(this);
  }

  componentWillMount() {
    this.props.fetchCards(this.props.cardsId);
    $('body').animate({ scrollTop: $(document).height() }, 'slow');
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.cards && nextProps.cards.length > 0 && !this.state.card) {
      this.startGame(nextProps.cards);
    }
  }

  componentDidUpdate() {
    if (this.isLabelCorrect() && this.state.labelLength > 0) {
      this.restartGame();
    }
  }

  getRandomCard(cards) {
    if (!cards) return;
    const { currentRandomNumber } = this.state;
    let randomNumber = _.random(0, cards.length - 1);
    while (currentRandomNumber === randomNumber) {
      randomNumber = _.random(0, cards.length - 1);
    }
    this.setState({ currentRandomNumber: randomNumber });
    return cards[randomNumber];
  }

  getLabelLength(label) {
    return label.length;
  }

  startGame(cards) {
    const card = this.getRandomCard(cards);
    const label = this.getLabel(card);
    const letters = this.getLetters(card);
    const labelLength = this.getLabelLength(letters);
    this.setState({ card, label, labelLength, letters,
      countOfCorrectLetters: 0, droppedIndexes: [], offset: null });
  }

  restartGame() {
    this.startGame(this.props.cards);
  }

  isLabelCorrect() {
    return this.state.labelLength === this.state.countOfCorrectLetters;
  }

  getLabel(card) {
    const { selectedLocality } = this.props;
    const cardItem = card.translations.find(translation => translation.language === selectedLocality.foreign);
    if (this.isArabic()) return this.reverseArabic(cardItem.translation.translation);
    return cardItem.translation.translation;
  }

  getLetters(card) {
    const { selectedLocality } = this.props;
    const cardItem = card.translations.find(translation => translation.language === selectedLocality.foreign);
    if (cardItem.languageUsesArticles === 'true') {
      const article = cardItem.translation.translation_article;
      const labelWithouArticle = cardItem.translation.translation_without_article;
      if (article) {
        if (this.labelHasManyWords(labelWithouArticle)) {
          const labelArray = [article].concat(labelWithouArticle.split(' '));
          return this.getLettersDefaultValues(labelArray);
        } else {
          const labelArray = [article].concat(labelWithouArticle.split(''));
          return this.getLettersDefaultValues(labelArray);
        }
      } else {
        return this.createLabel(labelWithouArticle);
      }
    } else {
      const label = cardItem.translation.translation;
      if (this.isArabic()) return this.createLabel(this.reverseArabic(label));
      return this.createLabel(label);
    }
  }

  createLabel(label) {
    if (this.labelHasManyWords(label)) {
      return this.getLettersDefaultValues(label.split(' '));
    } else {
      return this.getLettersDefaultValues(label.split(''));
    }
  }

  getLettersDefaultValues(labelArray) {
    return labelArray.map((letter, index) => ({ letter, index, isDropped: false, droppedIndex: null }));
  }

  labelHasManyWords(label) {
    return label.split(' ').length > 1;
  }

  addToDropped(letterItem, droppedIndex) {
    const letters = this.state.letters.map((letter, index) => {
      if (letterItem.index === index) {
        letter.isDropped = true;
        letter.droppedIndex = droppedIndex;
      }
      return letter;
    });
    let countOfCorrectLetters = this.state.countOfCorrectLetters;
    if (letterItem.index === droppedIndex) {
      countOfCorrectLetters += 1;
    }
    const droppedIndexes = this.state.droppedIndexes.concat(droppedIndex);
    this.setState({ letters, droppedIndexes, countOfCorrectLetters });
  }

  removeFromDropped(letterItem) {
    let countOfCorrectLetters = this.state.countOfCorrectLetters;
    if (letterItem.index === letterItem.droppedIndex) {
      countOfCorrectLetters -= 1;
    }
    const letters = this.state.letters.map((letter, index) => {
      if (letterItem.index === index) {
        letter.isDropped = false;
        letter.droppedIndex = null;
      }
      return letter;
    });
    const droppedIndexes = this.state.droppedIndexes.filter(index => index !== letterItem.index);
    this.setState({ letters, droppedIndexes, countOfCorrectLetters });
  }

  replaceLetterItem(letterItem, offset) {
    const letters = this.state.letters.map(letter => {
      if (letterItem.index === letter.index) {
        letter.offset = offset
      }
      return letter;
    });
    this.setState({ letters });
  }

  removeOffset() {
    const letters = this.state.letters.map(letter => {
      letter.offset = null;
      return letter;
    });
    this.setState({ letters });
  }

  isArabic() {
    const { selectedLocality } = this.props;
    return selectedLocality.foreign === 'ar';
  }

  reverseArabic(translation) {
    return translation.split('').reverse().join('');
  }

  render() {
    const { cardsId } = this.props;
    if (!this.state.card && !this.state.label) return null;
    return (
      <div>
        <div className="row">
          <div className="col-sm-12 col-md-10 col-md-offset-1">
            <ModesContainer
              title={'Shuffled ABC'}
              imageName={'memory game icon.png'}
              imageHoverName={'memory game icon.png'}
              paramsId={cardsId}
            />
          </div>
        </div>

        <div className="row">
          <div className="col-sm-12 col-md-10 col-md-offset-1">
            <div className="shuffledAbc-container">
              <ShuffledAbcMenu
                card={this.state.card}
                restartGame={this.restartGame}
              />
              <ShuffledAbcPreview
                card={this.state.card}
              />
              <LetterList
                label={this.state.label}
                isLabelCorrect={this.isLabelCorrect}
                letters={this.state.letters}
                removeFromDropped={this.removeFromDropped}
                replaceLetterItem={this.replaceLetterItem}
                removeOffset={this.removeOffset}
              />
              <LetterHolderList
                label={this.state.label}
                restartGame={this.restartGame}
                addToDropped={this.addToDropped}
                removeFromDropped={this.removeFromDropped}
                letters={this.state.letters}
                droppedIndexes={this.state.droppedIndexes}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ShuffledAbcContainer.propTypes = {
  cards: PropTypes.array.isRequired,
  fetchCards: PropTypes.func.isRequired,
  cardsId: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
  selectedLocality: state.common.localities.selectedLocality
});

export default DragDropContext(HTML5Backend)(connect(mapStateToProps)(ShuffledAbcContainer));
