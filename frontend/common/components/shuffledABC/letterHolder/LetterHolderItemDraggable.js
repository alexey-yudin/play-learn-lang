import React, { Component, PropTypes } from 'react';
import { createDragPreview } from 'react-dnd-text-dragpreview';
import { DragSource } from 'react-dnd';

const letterSource = {
  beginDrag(props) {
    return props;
  }
};

const collect = (connect, monitor) => {
  return {
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging(),
    didDrop: monitor.didDrop(),
    dropResult: monitor.getDropResult()
  };
};

const dragPreviewStyle = {
  backgroundColor: '#fff',
  borderColor: '#000',
  color: '#000',
  fontSize: 38,
  paddingTop: 17,
  paddingRight: 12,
  paddingBottom: 17,
  paddingLeft: 12
}

class LetterHolderItemDraggable extends Component {
  componentDidMount() {
    const { letterItem } = this.props;
    this.dragPreview = createDragPreview(letterItem.letter, dragPreviewStyle);
    this.props.connectDragPreview(this.dragPreview);
  }

  componentDidUpdate() {
    const { letterItem } = this.props;
    this.dragPreview = createDragPreview(letterItem.letter, dragPreviewStyle, this.dragPreview);
  }

  render() {
    const { letterItem, connectDragSource, isDragging } = this.props;
    if (isDragging) {
      return connectDragSource(
        <div
          style={{opacity: '0'}}
          ref={item => this.item = item}
          className="letter-list-holder__item">
          {letterItem.letter}
        </div>
      );
    } else {
      return connectDragSource(
        <div
          ref={item => this.item = item}
          className="letter-list-holder__item">
          {letterItem.letter}
        </div>
      );
    }
  }
}

LetterHolderItemDraggable.propTypes = {
  letterItem: PropTypes.object.isRequired,
  isHolder: PropTypes.bool.isRequired
};

export default DragSource('letter', letterSource, collect)(LetterHolderItemDraggable);
