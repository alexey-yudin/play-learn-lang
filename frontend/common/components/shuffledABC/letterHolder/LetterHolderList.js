import React, { Component, PropTypes } from 'react';
import LetterHolderListItem from './LetterHolderListItem';
import LetterListItem from '../letterList/LetterListItem';

class LetterHolderList extends Component {
  renderLetterItem(letterItem, index) {
    return (
      <LetterHolderListItem
        key={index}
        index={index}
        letterItem={letterItem}
        addToDropped={this.props.addToDropped}
        removeFromDropped={this.props.removeFromDropped}
        droppedIndexes={this.props.droppedIndexes}
        letters={this.props.letters}
        label={this.props.label}
      />
    );
  }

  render() {
    const { label, letters } = this.props;
    return (
      <div className="letter-holder-list">
        {letters.map(this.renderLetterItem.bind(this))}
      </div>
    );
  }
}

LetterHolderList.propTypes = {
  label: PropTypes.string.isRequired,
  addToDropped: PropTypes.func.isRequired,
  removeFromDropped: PropTypes.func.isRequired,
  letters: PropTypes.array.isRequired,
  droppedIndexes: PropTypes.array.isRequired
};

export default LetterHolderList;
