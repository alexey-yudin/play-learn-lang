import React, { Component, PropTypes } from 'react';
import LetterHolderListDraggable from './LetterHolderItemDraggable';
import LetterListItem from '../letterList/LetterListItem';
import { DropTarget } from 'react-dnd';

const letterHolderTarget = {
  drop(props, monitor) {
    const item = monitor.getItem();
    props.removeFromDropped(item.letterItem);
    props.addToDropped(item.letterItem, props.index);
  }
};

const collect = (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  dropResult: monitor.getDropResult(),
  getItem: monitor.getItem(),
  didDrop: monitor.didDrop()
});

class LetterHolderListItem extends Component {
  render() {
    const { letterItem, connectDropTarget, isOver, getItem, index, didDrop } = this.props;
    let borderColor = 'black';
    if (isOver) {
      if (getItem.letterItem.letter === letterItem.letter) borderColor = 'green';
      else borderColor = 'red';
    }

    const droppedLetter = this.props.letters.find(_letter => _letter.droppedIndex === index);
    if (droppedLetter) {
      let borderColor = 'black';
      if (letterItem.letter === droppedLetter.letter) {
        borderColor = 'green';
      } else {
        borderColor = 'red';
      }
      return (
        <div className="letter-holder-list__item" style={{ borderColor }}>
          <LetterHolderListDraggable
            letterItem={droppedLetter}
            isHolder={true}
          />
        </div>
      );
      // after drop replace the letter item
      // pass the proper offset
    } else {
      return connectDropTarget(
        <div className="letter-holder-list__item dropable" style={{ borderColor }}>
          <div className="letter">
          </div>
        </div>
      );
    }
  }
}

LetterHolderListItem.propTypes = {
  letterItem: PropTypes.object.isRequired,
  addToDropped: PropTypes.func.isRequired,
  removeFromDropped: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  droppedIndexes: PropTypes.array.isRequired,
  letters: PropTypes.array.isRequired,
  label: PropTypes.string.isRequired
};

export default DropTarget('letter', letterHolderTarget, collect)(LetterHolderListItem);
