import React, { Component, PropTypes } from 'react';
import * as localityConstants from '../../constants/localities';
import PlayAudio from '../shared/helpers/PlayAudio';
import LocalityImage from '../shared/language/LocalityImage';
import AudioPlayer from '../shared/generalAudio/AudioPlayer';

class ShuffledAbcMenu extends Component {
  constructor(props) {
    super(props);
    this.playAudio = this.playAudio.bind(this);
    this.audioPlayer = new PlayAudio()
  }

  playAudio() {
    const { card } = this.props;
    this.audioPlayer.playAudio(`shuffled-abc-correct-word${[card.id].join('')}`);
  }

  render() {
    const { card } = this.props;
    return (
      <div className="shuffled-abc__menu">
        <div className="correct-word">
          <div>
            <LocalityImage
              localityType={localityConstants.FOREIGN_LOCALITY}
              className={'locality-image'}
            />
          </div>
          <div onClick={this.playAudio} className="image-voice">
            <img src="/resources/icons/voice-icon.png" alt=""/>
            <AudioPlayer
              translations={card.translations}
              localityType={localityConstants.FOREIGN_LOCALITY}
              generalId='shuffled-abc-correct-word'
              cardId={card.id}
              autoPlay={true}
            />
          </div>
        </div>
        <div>
            <button type='button' onClick={this.props.restartGame} className='btn btn-default'>
              <i className="glyphicon glyphicon-repeat"></i>
            </button>
        </div>
      </div>
    );
  }
}

ShuffledAbcMenu.propTypes = {
  restartGame: PropTypes.func.isRequired,
  card: PropTypes.object.isRequired
};

export default ShuffledAbcMenu;
