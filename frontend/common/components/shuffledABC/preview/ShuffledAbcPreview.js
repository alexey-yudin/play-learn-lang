import React, { Component, PropTypes } from 'react';
import CardImageHelper from '../../shared/helpers/CardImageHelper';
import RomanisationWithTranslation from '../../shared/romanization/RomanisationWithTranslation';
import * as localityConstants from '../../../constants/localities';

class ShuffledAbcPreview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageExist: null
    };
    this.imageHelper = new CardImageHelper();
  }

  componentDidMount() {
    const { card } = this.props;
    const imageExist = this.imageHelper.cardHasImage(card);
    this.setState({ imageExist });
  }

  render() {
    const { card } = this.props;
    if (this.state.imageExist) {
      return (
        <div className="shuffled-abc-preview">
          <img src={card.image.url_600}/>
        </div>
      );
    } else {
      return (
        <div className="shuffled-abc-preview-text">
          <RomanisationWithTranslation
            translations={card.translations}
            localityType={localityConstants.NATIVE_LOCALITY}
          />
        </div>
      );
    }
  }
}

ShuffledAbcPreview.propTypes = {
  card: PropTypes.object.isRequired
};

export default ShuffledAbcPreview;
