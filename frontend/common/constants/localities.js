export const FETCH_LOCALITIES = 'localities/FETCH_LOCALITIES';

export const SET_DEFAULT_SELECTED_LOCALITIES = 'localities/SET_DEFAULT_SELECTED_LOCALITIES';

export const CLEAR_SELECTED_LOCALITIES = 'localities/CLEAR_SELECTED_LOCALITIES';

export const CLEAR_LOCALITIES = 'localities/CLEAR_LOCALITIES';

export const SET_FOREIGN_LOCALITY = 'localities/SET_FOREIGN_LOCALITY';

export const SET_NATIVE_LOCALITY = 'localities/SET_NATIVE_LOCALITY';

export const DEFAULT_FOREIGN_LOCALITY = 'localities/en_us';

export const DEFAULT_NATIVE_LOCALITY = 'localities/es_es';

export const FOREIGN_LOCALITY = 'localities/FOREIGN_LOCALITY';

export const NATIVE_LOCALITY = 'localities/NATIVE_LOCALITY';

export const DISABLE_NATIVE_LOCALITY = 'localities/DISABLE_NATIVE_LOCALITY';
