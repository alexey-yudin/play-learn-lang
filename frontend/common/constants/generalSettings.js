export const defaultState = {
  gameAudio: true,
  romanisation: true,
  generalAudio: true,
  upperCase: false,
  meta: {
    appName: '',
    appVersion: ''
  }
};

// Audio answers
export const ENABLE_AUDIO_ANSWERS = 'generalSettings/ENABLE_AUDIO_ANSWERS';

export const DISABLE_AUDIO_ANSWERS = 'generalSettings/DISABLE_AUDIO_ANSWERS';

// Romanised
export const ENABLE_ROMANISED = 'generalSettings/ENABLE_ROMANISED';

export const DISABLE_ROMANISED = 'generalSettings/DISABLE_ROMANISED';

// General audio
export const ENABLE_GENERAL_AUDIO = 'generalSettings/ENABLE_GENERAL_AUDIO';

export const DISABLE_GENERAL_AUDIO = 'generalSettings/DISABLE_GENERAL_AUDIO';

// Upper case
export const ENABLE_UPPER_CASE = 'generalSettings/ENABLE_UPPER_CASE';

export const DISABLE_UPPER_CASE = 'generalSettings/DISABLE_UPPER_CASE';

// App name
export const SET_APP_NAME = 'generalSettings/SET_APP_NAME';

// Default
export const SET_DEFAULT_SETTINGS = 'generalSettings/SET_DEFAULT_SETTINGS';

// Meta info
export const FETCH_META_INFO_SUCCESS = 'generalSettings/FETCH_META_INFO_SUCCESS';

export const FETCH_META_INFO_ERROR = 'generalSettings/FETCH_META_INFO_ERROR';

// Set settings
export const SET_GENERAL_SETTINGS = 'generalSettings/SET_GENERAL_SETTINGS';
