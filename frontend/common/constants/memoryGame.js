export const DEFAULT_AMOUNT_OF_CARDS = '12';

export const SET_DEFAULT_SETTINGS = 'memoryGame/SET_DEFAULT_SETTINGS';

export const SET_MEMORY_GAME_SETTINGS = 'memoryGame/SET_MEMORY_GAME_SETTINGS';

export const SET_AMOUNT_OF_CARDS = 'SET_AMOUNT_OF_CARDS';

export const DEFAULT_VARIANTS_FOR = [
  {
    name: '3x2',
    amount: '6'
  }, {
    name: '4x3',
    amount: '12'
  }, {
    name: '5x4',
    amount: '20'
  }, {
    name: '6x5',
    amount: '30'
  }
];

export const defaultState = { amount: DEFAULT_AMOUNT_OF_CARDS };
