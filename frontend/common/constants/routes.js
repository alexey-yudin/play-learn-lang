// App names
export const alphakm = 'alphakm';
export const languages = 'languages';

// Mode names
export const quiz = 'quiz';
export const memoryGame = 'memory-game';
export const reviewCards = 'review-cards';
export const cardsCarousel = 'cards-carousel';
export const translationQuiz = 'translation-quiz';
export const bingoQuiz = 'bingo';
export const shuffledAbc = 'shuffled-abc';
