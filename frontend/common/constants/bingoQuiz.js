export const SET_GRID_SIZE = 'bingoQuiz/SET_GRID_SIZE';

export const SET_DEFAULT_GRID_SIZE = 'bingoQuiz/DEFAULT_GRID_SIZE'

export const SET_BINGO_QUIZ_SETTINGS = 'bingoQuiz/SET_BINGO_QUIZ_SETTINGS';

export const DEFAULT_GRID_SIZE = 9;

export const GRID_SIZES = [
  {
    label: '3x3',
    value: 9,
    rowLength: 3
  }, {
    label: '4x4',
    value: 16,
    rowLength: 4
  }, {
    label: '5x5',
    value: 25,
    rowLength: 5
  }
];

export const defaultState = {
  value: DEFAULT_GRID_SIZE
};