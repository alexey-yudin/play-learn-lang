export const DEFAULT_COUNT_OF_LEVELS = '10';

export const SET_DEFAULT_SETTINGS = 'quiz/SET_DEFAULT_SETTINGS';

export const SET_QUIZ_SETTINGS = 'quiz/SET_QUIZ_SETTINGS';

export const DEFAULT_COUNT_OF_CARDS = {
  name: 'Standard',
  translation: 'settings.quiz_button_standard',
  count: '6'
};

export const DEFAULT_STATE_FOR_QUIZ_SETTINGS = {
  levelsCount: DEFAULT_COUNT_OF_LEVELS,
  cardsCount: DEFAULT_COUNT_OF_CARDS.count
}

export const QUIZ_MODE = 'QUIZ_MODE';

export const TRANSLATION_QUIZ_MODE = 'TRANSLATION_QUIZ_MODE';

export const SET_COUNT_OF_LEVELS = 'SET_COUNT_OF_LEVELS';

export const SET_COUNT_OF_CARDS = 'SET_COUNT_OF_CARDS';

export const DEFAULT_VARIANTS_FOR_LEVEL_COUNT = ['5', '10', 'All'];

export const DEFAULT_VARIANTS_FOR_CARDS_COUNT = [
  {
    name: 'Easy',
    translation: 'settings.quiz_button_easy',
    count: '3'
  },
  {
    name: 'Standard',
    translation: 'settings.quiz_button_standard',
    count: '6'
  },
  {
    name: 'Hard',
    translation: 'settings.quiz_button_hard',
    count: '9'
  }
];
