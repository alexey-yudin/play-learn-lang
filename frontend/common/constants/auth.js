export const SOCIAL_LOGIN_SUCCESS = 'auth/SOCIAL_LOGIN_SUCCESS';
export const SOCIAL_LOGIN_ERROR = 'auth/SOCIAL_LOGIN_ERROR';
export const LOGOUT = 'auth/LOGOUT';
