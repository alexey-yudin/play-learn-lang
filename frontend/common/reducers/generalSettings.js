import * as constants from '../constants/generalSettings';
import settingsService from '../services/settings';

const defaultState = constants.defaultState;

export default (state = defaultState, action) => {
  switch(action.type) {
    case constants.SET_APP_NAME:
    {
      const newState = {...state, meta: {...state.meta, appName: action.appName}};
      return newState;
    }

    case constants.SET_DEFAULT_SETTINGS:
    {
      const settingsFromStorage = JSON.parse(localStorage.getItem(`${action.appName}/generalSettings`));
      if (settingsFromStorage) {
        return {...state,
          gameAudio: settingsFromStorage.gameAudio,
          romanisation: settingsFromStorage.romanisation,
          generalAudio: settingsFromStorage.generalAudio,
          upperCase: settingsFromStorage.upperCase
        };
      } else {
        return {...state,
          gameAudio: defaultState.gameAudio,
          romanisation: defaultState.romanisation,
          generalAudio: defaultState.generalAudio,
          upperCase: defaultState.upperCase
        };
      }
    }

    // Game audio
    case constants.ENABLE_AUDIO_ANSWERS:
      return {...state, gameAudio: action.gameAudio};

    case constants.DISABLE_AUDIO_ANSWERS:
      return {...state, gameAudio: action.gameAudio};

    // Romanisation
    case constants.ENABLE_ROMANISED:
      return {...state, romanisation: action.romanisation};

    case constants.DISABLE_ROMANISED:
      return {...state, romanisation: action.romanisation};

    // General audio
    case constants.ENABLE_GENERAL_AUDIO:
      return {...state, generalAudio: action.generalAudio};

    case constants.DISABLE_GENERAL_AUDIO:
      return {...state, generalAudio: action.generalAudio};

    // Upper case
    case constants.ENABLE_UPPER_CASE:
      return {...state, upperCase: action.upperCase};

    case constants.DISABLE_UPPER_CASE:
      return {...state, upperCase: action.upperCase};

    // Fetch meta info
    case constants.FETCH_META_INFO_SUCCESS:
    {
      const appVersion = action.metaInfo.find(metaInfo => metaInfo.name === 'app_version');
      if (appVersion) {
        return {...state, meta: {...state.meta, appVersion}};
      } else {
        return state;
      }
    }

    case constants.FETCH_META_INFO_ERROR:
      return state;

    // Set settings
    case constants.SET_GENERAL_SETTINGS:
    {
      const general = action.settings;
      return {
        ...state,
        gameAudio: general.gameAudio,
        romanisation: general.romanisation,
        generalAudio: general.generalAudio,
        upperCase: general.upperCase
      };
    }

    default:
      return state;
  }
}
