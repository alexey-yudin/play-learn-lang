import * as constants from '../constants/quiz';

const defaultState = constants.DEFAULT_STATE_FOR_QUIZ_SETTINGS;
export default (state = defaultState, action) => {
  switch(action.type) {
    case constants.SET_DEFAULT_SETTINGS:
    {
      const settingsFromStorage = JSON.parse(localStorage.getItem(`${action.appName}/quizSettings`));
      if (settingsFromStorage) {
        return settingsFromStorage;
      } else {
        return defaultState;
      }
    }

    case constants.SET_COUNT_OF_LEVELS:
      return {...state, levelsCount: action.levelsCount};

    case constants.SET_COUNT_OF_CARDS:
      return {...state, cardsCount: action.cardsCount};

    case constants.SET_QUIZ_SETTINGS:
    {
      const quizSettings = action.settings;
      return {
        ...state,
        levelsCount: quizSettings.levelsCount,
        cardsCount: quizSettings.cardsCount
      };
    }

    default:
      return state;
  }
}
