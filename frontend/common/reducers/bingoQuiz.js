import * as constants from '../constants/bingoQuiz';

const defaultState = constants.defaultState;

export default (state = defaultState, action) => {
  switch(action.type) {
    case constants.SET_DEFAULT_GRID_SIZE:
    {
      const settingsFromStorage = JSON.parse(localStorage.getItem(`${action.appName}/bingoQuizSettings`));
      if (settingsFromStorage) {
        return settingsFromStorage;
      } else {
        return defaultState;
      }
    }

    case constants.SET_GRID_SIZE:
      return {...state, value: action.value};

    case constants.SET_BINGO_QUIZ_SETTINGS:
    {
      const quizSetting = action.settings;
      return {
        ...state,
        value: quizSetting.value
      };
    }

    default:
      return state;
  }
};
