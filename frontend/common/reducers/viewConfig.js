import * as constants from '../constants/viewConfig';

const initialState = {
  nestedPackageIds: [],
  cardsConfig: []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case constants.GET_NESTED_PACKAGE_IDS_SUCCESS:
      return {...state, nestedPackageIds: action.nestedIds};

    case constants.GET_NESTED_PACKAGE_IDS_ERROR:
      return state;

    case constants.GET_CARDS_CONFIG_SUCCESS:
      return {...state, cardsConfig: action.cardsConfig};

    case constants.GET_CARDS_CONFIG_ERROR:
      return state;

    default:
      return state;
  }
}
