import * as constants from '../constants/auth';

const defaultState = {
  isAuthenticated: false,
  token: null,
  user: null,
  error: {
    errorCode: null,
    errorMessage: null,
    credential: null
  }
};

export default (state = defaultState, action) => {
  switch(action.type) {
    case constants.SOCIAL_LOGIN_SUCCESS:
      return {...state,
        isAuthenticated: true,
        token: action.payload.token,
        user: action.payload.user
      };

    case constants.SOCIAL_LOGIN_ERROR:
      return {...state,
        isAuthenticated: false,
        token: null,
        user: null,
        error: {
          ...state.error,
          errorCode: action.payload.errorCode,
          errorMessage: action.payload.errorMessage,
          credential: action.payload.credential
        }
      };
    
    case constants.LOGOUT:
      return {...state,
        isAuthenticated: false,
        token: null,
        user: null
      };

    default:
      return state;
  }
}
