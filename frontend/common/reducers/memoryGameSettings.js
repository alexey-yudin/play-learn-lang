import * as constants from '../constants/memoryGame';

const defaultState = constants.defaultState;

export default (state = defaultState, action) => {
  switch(action.type) {
    case constants.SET_DEFAULT_SETTINGS:
    {
      const settingsFromStorage = JSON.parse(localStorage.getItem(`${action.appName}/memoryGameSettings`));
      if (settingsFromStorage) {
        return settingsFromStorage;
      } else {
        return defaultState;
      }
    }

    case constants.SET_AMOUNT_OF_CARDS:
      return {...state, amount: action.amount};

    case constants.SET_MEMORY_GAME_SETTINGS:
    {
      const memoryGameSettings = action.settings;
      return {
        ...state,
        amount: memoryGameSettings.amount
      };
    }

    default:
      return state;
  }
}
