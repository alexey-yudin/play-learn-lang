import * as constants from '../constants/localities';

const defaultState = {
  localities: {
    foreign: [],
    native: []
  },
  selectedLocality: {
    foreign: '',
    native: ''
  }
}

export default (state = defaultState, action) => {
  switch(action.type) {
    case constants.FETCH_LOCALITIES:
    {
      return {...state, localities: {...state.localities,
        foreign: action.localities.foreign,
        native: action.localities.native
      }};
    }

    case constants.SET_DEFAULT_SELECTED_LOCALITIES:
    {
      let foreignLocalityId = '', nativeLocalityId = '';
      const localityFromStorage = JSON.parse(localStorage.getItem(`${action.appName}/localities`));
      if (localityFromStorage && localityFromStorage.foreign && localityFromStorage.native) {
        foreignLocalityId = localityFromStorage.foreign;
        nativeLocalityId = localityFromStorage.native;
      } else {
        const foreignLocality = state.localities.foreign.find(locality => locality.isdefault === 'true');
        const nativeLocality = state.localities.native.find(locality => locality.isdefault === 'true');
        if (foreignLocality) foreignLocalityId = foreignLocality.id;
        else foreignLocalityId = state.localities.foreign[0].id;
        if (nativeLocality) nativeLocalityId = nativeLocality.id;
        else nativeLocalityId = state.localities.native[0].id;
      }
      return {...state, selectedLocality: {...state.selectedLocality,
        foreign: foreignLocalityId,
        native: nativeLocalityId
      }};
    }

    case constants.CLEAR_SELECTED_LOCALITIES:
    {
      return {...state, selectedLocality: {...state.selectedLocality, foreign: '', native: ''}};
    }

    case constants.CLEAR_LOCALITIES:
    {
      return {...state, localities: {...state.localities, native: [], foreign: []}};
    }

    case constants.SET_FOREIGN_LOCALITY:
    {
      const selectedLocality = {...state.selectedLocality, foreign: action.localityId};
      localStorage.setItem(`${action.appName}/localities`, JSON.stringify(selectedLocality));
      return {...state, selectedLocality};
    }

    case constants.SET_NATIVE_LOCALITY:
    {
      const selectedLocality = {...state.selectedLocality, native: action.localityId};
      localStorage.setItem(`${action.appName}/localities`, JSON.stringify(selectedLocality));
      return {...state, selectedLocality};
    }

    case constants.DISABLE_NATIVE_LOCALITY:
    {
      const selectedLocality = {...state.selectedLocality, native: undefined};
      localStorage.setItem(`${action.appName}/localities`, JSON.stringify(selectedLocality));
      return {...state, selectedLocality};
    }

    default:
      return state;
  }
}
