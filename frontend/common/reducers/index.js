import { combineReducers } from 'redux';

import memoryGameSettings from './memoryGameSettings';
import quizSettings from './quizSettings';
import generalSettings from './generalSettings';
import translation from './translation';
import localities from './localities';
import viewConfig from './viewConfig';
import bingoQuiz from './bingoQuiz';
import auth from './auth';

const rootReducer = combineReducers({
  memoryGameSettings,
  quizSettings,
  generalSettings,
  translation,
  localities,
  viewConfig,
  bingoQuiz,
  auth
});

export default rootReducer;
