import * as constants from '../constants/translation';

const defaultState = {
  translations: [],
  selectedTranslation: {}
}

export default (state = defaultState, action) => {
  switch(action.type) {
    case constants.FETCH_TRANSLATIONS_SUCCESS:
    {
      const newState = {...state, translations: action.translations};
      localStorage.setItem('translations', JSON.stringify(newState));
      return newState;
    }

    case constants.SET_DEFAULT_TRANSLATION:
    {
      const translationFromStorage = JSON.parse(localStorage.getItem('translations'));
      if (translationFromStorage) {
        return translationFromStorage;
      } else {
        return state;
      }
    }

    case constants.SET_TRANSLATION:
    {
      const selectedTranslation = state.translations.filter((translation) => {
        if (translation.id === action.id) {
          return translation;
        }
      });
      const newState = {...state, selectedTranslation: selectedTranslation[0] };
      localStorage.setItem('translations', JSON.stringify(newState));
      return newState;
    }

    default:
      return state;
  }
}
