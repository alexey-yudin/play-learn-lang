export const getGeneralSettings = state => state.common.generalSettings;
export const getQuizSettings = state => state.common.quizSettings;
export const getBingoQuizSettings = state => state.common.bingoQuiz;
export const getMemoryGameSettings = state => state.common.memoryGameSettings;
