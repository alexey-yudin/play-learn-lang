export const getIsAuthenticated = state => state.common.auth.isAuthenticated;
export const getUser = state => state.common.auth.user;
