export const getAppName = state => state.common.generalSettings.meta.appName;
export const getMetaInfo = state => state.common.generalSettings.meta;