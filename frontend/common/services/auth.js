import * as firebase from 'firebase';

class AuthService {
  setSession(credentials) {
    localStorage.setItem('auth', JSON.stringify(credentials));
  }

  getSession() {
    return JSON.parse(localStorage.getItem('auth'));
  }

  clearSession() {
    localStorage.removeItem('auth');
  }
}

export default new AuthService;
