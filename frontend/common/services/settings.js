import * as firebase from 'firebase';
import * as settingsSelectors from '../selectors/settings';
import * as metaInfoSelectors from '../selectors/metaInfo';

class SettingsService {
  saveSettingsToDb(settings, appName) {
    return this.getSettingsFromDb()
      .then(userObject => {
        if (!userObject) {
          const currentUser = firebase.auth().currentUser;
          const userObject = {
            username: currentUser.displayName,
            email: currentUser.email,
            uid: currentUser.uid,
            settings: {}
          };
          userObject.settings[appName] = {
            general: settings.general,
            quiz: settings.quiz,
            bingoQuiz: settings.bingoQuiz,
            memoryGame: settings.memoryGame
          };
          return firebase.database().ref(`users/${currentUser.uid}`).set(userObject);
        } else {
          userObject.settings[appName] = {
            general: settings.general,
            quiz: settings.quiz,
            bingoQuiz: settings.bingoQuiz,
            memoryGame: settings.memoryGame
          };

          return firebase.database().ref(`users/${userObject.uid}`).set(userObject);
        }
      }).catch(error => console.error);

  }

  getSettingsFromDb() {
    const currentUser = firebase.auth().currentUser;
    if (!currentUser) Promise.resolve();
    return firebase.database().ref(`users/${currentUser.uid}`).once('value')
      .then(snap => snap.val())
      .catch(error => error);
  }

  saveGeneralSettingsToDb(generalSettings, state) {
    const appName = metaInfoSelectors.getAppName(state);
    const settings = {
      ...this.getSettings(state),
      general: {...generalSettings, meta: null},
    };

    this.saveSettingsToDb(settings, appName)
      .then(() => console.info('saved successfully'))
      .catch(error => console.error(error));
  }

  saveQuizSettingsToDb(quizSettings, state) {
    const appName = metaInfoSelectors.getAppName(state);
    const currentSettings = this.getSettings(state);
    const settings = {
      ...currentSettings,
      general: this.normalizeGeneralSettings(currentSettings.general),
      quiz: quizSettings
    };

    this.saveSettingsToDb(settings, appName)
      .then(() => console.info('saved successfully'))
      .catch(error => console.error(error));
  }

  saveBingoQuizToDb(bingoQuizSettings, state) {
    const appName = metaInfoSelectors.getAppName(state);
    const currentSettings = this.getSettings(state);
    const settings = {
      ...currentSettings,
      general: this.normalizeGeneralSettings(currentSettings.general),
      bingoQuiz: bingoQuizSettings
    };

    this.saveSettingsToDb(settings, appName)
      .then(() => console.info('saved successfully'))
      .catch(error => console.error(error));
  }

  saveMemoryGameToDb(memoryGameSettings, state) {
    const appName = metaInfoSelectors.getAppName(state);
    const currentSettings = this.getSettings(state);
    const settings = {
      ...currentSettings,
      general: this.normalizeGeneralSettings(currentSettings.general),
      memoryGame: memoryGameSettings
    };

    this.saveSettingsToDb(settings, appName)
      .then(() => console.info('saved successfully'))
      .catch(error => console.error(error));
  }

  getSettings(state) {
    return {
      general: settingsSelectors.getGeneralSettings(state),
      quiz: settingsSelectors.getQuizSettings(state),
      bingoQuiz: settingsSelectors.getBingoQuizSettings(state),
      memoryGame: settingsSelectors.getMemoryGameSettings(state)
    };
  }

  normalizeGeneralSettings(generalSettings) {
    return {...generalSettings, meta: null};
  }
}

export default new SettingsService;
