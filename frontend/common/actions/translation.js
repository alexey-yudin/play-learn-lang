import axios from 'axios';
import * as constants from '../constants/translation';

export const fetchTranslations = () => {
  return dispatch => {
    axios.get('/api/translations/getTranslations')
      .then(response => {
        const translations = response.data.translations;
        dispatch(fetchTranslationsSuccess(translations));
      })
      .catch(error => {
        dispatch(fetchTranslationsError());
      });
  }
}

const fetchTranslationsSuccess = translations => {
  return {
    type: constants.FETCH_TRANSLATIONS_SUCCESS,
    translations
  }
}

const fetchTranslationsError = () => {
  return {
    type: constants.FETCH_TRANSLATIONS_ERROR
  }
}

export const setDefaultTranslation = () => {
  return {
    type: constants.SET_DEFAULT_TRANSLATION
  }
}

export const setTranslation = id => {
  return {
    type: constants.SET_TRANSLATION,
    id
  }
}
