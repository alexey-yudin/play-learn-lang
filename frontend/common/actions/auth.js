import * as constants from '../constants/auth';
import * as firebase from 'firebase';
import authService from '../services/auth';

export const socialLogin = provider => {
  return (dispatch, getState) => {
    firebase.auth().signInWithPopup(provider)
      .then(result => {
        const state = getState();
        const credential = {
          token: result.credential.accessToken,
          user: result.user
        };
        dispatch(loginSuccess(credential));
        authService.setSession(credential);
      }).catch(error => {
        if (error.code === 'auth/account-exists-with-different-credential') {
          const currentUser = firebase.auth().currentUser;
          const credential = {
            token: error.credential.accessToken,
            user: currentUser
          };
          dispatch(loginSuccess(credential));
          authService.setSession(credential);
          currentUser.linkWithRedirect(provider);
        } else {
          console.error(error);
          dispatch(loginError({
            errorCode: error.code,
            errorMessage: error.message,
            credential: error.credential
          }));
        }
      });
  };
};

export const loginSuccess = (result) => ({
  type: constants.SOCIAL_LOGIN_SUCCESS,
  payload: {
    token: result.token,
    user: result.user
  }
});

export const loginError = (error) => ({
  type: constants.SOCIAL_LOGIN_ERROR,
  payload: {
    errorCode: error.errorCode,
    errorMessage: error.errorMessage,
    credential: error.credential
  }
});

export const logout = () => {
  return dispatch => {
    authService.clearSession();
    dispatch(logoutSuccess());
  };
};

const logoutSuccess = () => ({
  type: constants.LOGOUT
});
