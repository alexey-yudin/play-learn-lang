import * as types from '../constants/reviewCards';

export const clearReviewCardsList = () => {
  return {
    type: types.CLEAR_REVIEW_CARDS_LIST
  }
}
