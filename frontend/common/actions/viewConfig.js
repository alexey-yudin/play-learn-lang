import axios from 'axios';
import * as routeConstants from '../constants/routes';
import * as constants from '../constants/viewConfig';

export const getNestedPackageIds = appName => {
  return dispatch => {
    axios.get(`/api/package/getNestedPackagesIds/${appName}`)
      .then(response => dispatch(getNestedPackagesIdsSuccess(response.data)))
      .catch(error => dispatch(getNestedPackagesIdsError()))
  }
}

const getNestedPackagesIdsSuccess = nestedIds => {
  return {
    type: constants.GET_NESTED_PACKAGE_IDS_SUCCESS,
    nestedIds
  }
}

const getNestedPackagesIdsError = () => {
  return {
    type: constants.GET_NESTED_PACKAGE_IDS_ERROR
  }
}

export const getCardsConfig = appName => {
  return dispatch => {
    axios.get(`/api/cards/getCardsConfig/${appName}`)
      .then(response => dispatch(getCardsConfigSuccess(response.data.cardsConfig)))
      .catch(error => dispatch(getCardsConfigError()));
  }
}

const getCardsConfigSuccess = cardsConfig => {
  return {
    type: constants.GET_CARDS_CONFIG_SUCCESS,
    cardsConfig
  }
}

const getCardsConfigError = () => {
  return {
    type: constants.GET_CARDS_CONFIG_ERROR
  }
}
