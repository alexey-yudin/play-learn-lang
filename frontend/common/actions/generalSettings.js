import axios from 'axios';
import * as constants from '../constants/generalSettings';
import settingsService from '../services/settings';
import * as settingsSelectors from '../selectors/settings';
import * as settingsActions from './settings';

// Meta info
export const fetchMetaInfo = appName => {
  return dispatch => {
    axios.get(`/api/metaInfo/getMetaInfo/${appName}`)
      .then(response => {
        const metaInfo = response.data.strings.resources.string;
        dispatch(fetchMetaInfoSuccess(metaInfo));
        dispatch(settingsActions.setSettingsFromAuth(appName));
      })
      .catch(errors => {
        dispatch(fetchMetaInfoError());
      });
  }
}

const fetchMetaInfoSuccess = metaInfo => {
  return {
    type: constants.FETCH_META_INFO_SUCCESS,
    metaInfo
  }
}

const fetchMetaInfoError = () => {
  return {
    type: constants.FETCH_META_INFO_ERROR
  }
}

// App name
export const setAppName = appName => {
  return {
    type: constants.SET_APP_NAME,
    appName
  };
}

// Game audio settings
export const enableAudioAnswers = appName => {
  return (dispatch, getState) => {
    const state = getState();
    const gameAudio = true;
    const generalSettingsState = {...settingsSelectors.getGeneralSettings(state), gameAudio};
    localStorage.setItem(`${appName}/generalSettings`, JSON.stringify(generalSettingsState));
    settingsService.saveGeneralSettingsToDb(generalSettingsState, state);
    dispatch({
      type: constants.ENABLE_AUDIO_ANSWERS,
      gameAudio
    });
  };
}

export const disableAudioAnswers = appName => {
  return (dispatch, getState) => {
    const state = getState();
    const gameAudio = false;
    const generalSettingsState = {...settingsSelectors.getGeneralSettings(state), gameAudio};
    localStorage.setItem(`${appName}/generalSettings`, JSON.stringify(generalSettingsState));
    settingsService.saveGeneralSettingsToDb(generalSettingsState, state);
    dispatch({
      type: constants.DISABLE_AUDIO_ANSWERS,
      gameAudio
    });
  };
}

// Romanised settings
export const enableRomanised = appName => {
  return (dispatch, getState) => {
    const state = getState();
    const romanisation = true;
    const generalSettingsState = {...settingsSelectors.getGeneralSettings(state), romanisation};
    localStorage.setItem(`${appName}/generalSettings`, JSON.stringify(generalSettingsState));
    settingsService.saveGeneralSettingsToDb(generalSettingsState, state);
    dispatch({
      type: constants.ENABLE_ROMANISED,
      romanisation
    });
  };
}

export const disableRomanised = appName => {
  return (dispatch, getState) => {
    const state = getState();
    const romanisation = false;
    const generalSettingsState = {...settingsSelectors.getGeneralSettings(state), romanisation};
    localStorage.setItem(`${appName}/generalSettings`, JSON.stringify(generalSettingsState));
    settingsService.saveGeneralSettingsToDb(generalSettingsState, state);
    dispatch({
      type: constants.DISABLE_ROMANISED,
      romanisation
    });
  };
}

// General audio settings
export const enableGeneralAudio = appName => {
  return (dispatch, getState) => {
    const state = getState();
    const generalAudio = true;
    const generalSettingsState = {...settingsSelectors.getGeneralSettings(state), generalAudio};
    localStorage.setItem(`${appName}/generalSettings`, JSON.stringify(generalSettingsState));
    settingsService.saveGeneralSettingsToDb(generalSettingsState, state);
    dispatch({
      type: constants.ENABLE_GENERAL_AUDIO,
      generalAudio
    });
  };
}

export const disableGeneralAudio = appName => {
  return (dispatch, getState) => {
    const state = getState();
    const generalAudio = false;
    const generalSettingsState = {...settingsSelectors.getGeneralSettings(state), generalAudio};
    localStorage.setItem(`${appName}/generalSettings`, JSON.stringify(generalSettingsState));
    settingsService.saveGeneralSettingsToDb(generalSettingsState, state);
    dispatch({
      type: constants.DISABLE_GENERAL_AUDIO,
      generalAudio
    });
  };
}

// Upper case
export const enableUpperCase = appName => {
  return (dispatch, getState) => {
    const state = getState();
    const upperCase = true;
    const generalSettingsState = {...settingsSelectors.getGeneralSettings(state), upperCase};
    localStorage.setItem(`${appName}/generalSettings`, JSON.stringify(generalSettingsState));
    settingsService.saveGeneralSettingsToDb(generalSettingsState, state);
    dispatch({
      type: constants.ENABLE_UPPER_CASE,
      upperCase
    });
  };
}

export const disableUpperCase = appName => {
  return (dispatch, getState) => {
    const state = getState();
    const upperCase = false;
    const generalSettingsState = {...settingsSelectors.getGeneralSettings(state), upperCase};
    localStorage.setItem(`${appName}/generalSettings`, JSON.stringify(generalSettingsState));
    settingsService.saveGeneralSettingsToDb(generalSettingsState, state);
    dispatch({
      type: constants.DISABLE_UPPER_CASE,
      upperCase
    });
  };
}

// Default
export const setDefaultSettings = appName => {
  return {
    type: constants.SET_DEFAULT_SETTINGS,
    appName
  }
}

// Set settings
export const setGeneralSettings = settings => ({
  type: constants.SET_GENERAL_SETTINGS,
  settings
});
