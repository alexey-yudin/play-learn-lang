import * as constants from '../constants/quiz';
import * as settingsSelectors from '../selectors/settings';
import settingsService from '../services/settings';

export const setDefaultSettings = appName => {
  return {
    type: constants.SET_DEFAULT_SETTINGS,
    appName
  }
}

export const setCountOfLevels = (levelsCount, appName) => {
  return (dispatch, getState) => {
    const state = getState();
    const quizSettings = {...settingsSelectors.getQuizSettings(state), levelsCount};
    localStorage.setItem(`${appName}/quizSettings`, JSON.stringify(quizSettings));
    settingsService.saveQuizSettingsToDb(quizSettings, state);
    dispatch({
      type: constants.SET_COUNT_OF_LEVELS,
      levelsCount
    });
  };
}

export const setCountOfCards = (cardsCount, appName) => {
  return (dispatch, getState) => {
    const state = getState();
    const quizSettings = {...settingsSelectors.getQuizSettings(state), cardsCount};
    localStorage.setItem(`${appName}/quizSettings`, JSON.stringify(quizSettings));
    settingsService.saveQuizSettingsToDb(quizSettings, state);
    dispatch({
      type: constants.SET_COUNT_OF_CARDS,
      cardsCount
    });
  };
}

export const setQuizSettings = (settings) => ({
  type: constants.SET_QUIZ_SETTINGS,
  settings
});
