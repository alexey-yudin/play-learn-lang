import * as constants from '../constants/localities';

export const setForeignLocality = (localityId, appName) => {
  return {
    type: constants.SET_FOREIGN_LOCALITY,
    localityId,
    appName
  }
}

export const setNativeLocality = (localityId, appName) => {
  return {
    type: constants.SET_NATIVE_LOCALITY,
    localityId,
    appName
  }
}

export const disableNativeLocality = appName => {
  return {
    type: constants.DISABLE_NATIVE_LOCALITY,
    appName
  }
}

export const setDefaultSelectedLocalities = appName => {
  return {
    type: constants.SET_DEFAULT_SELECTED_LOCALITIES,
    appName
  }
}

export const clearSelectedLocalities = () => {
  return {
    type: constants.CLEAR_SELECTED_LOCALITIES
  }
}

export const clearLocalities = () => {
  return {
    type: constants.CLEAR_LOCALITIES
  }
}
