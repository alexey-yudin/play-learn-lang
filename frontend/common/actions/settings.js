import * as metaInfoSelectors from '../selectors/metaInfo';
import * as generalSettingsActions from '../actions/generalSettings';
import * as bingoQuizSettingsActions from '../actions/bingoQuiz';
import * as quizSettingsActions from '../actions/quizSettings';
import * as memoryGameActions from '../actions/memoryGameSettings';
import settingsService from '../services/settings';
import {defaultState as defaultGeneralSettings} from '../constants/generalSettings';
import {DEFAULT_STATE_FOR_QUIZ_SETTINGS as defaultQuizSettings} from '../constants/quiz';
import {defaultState as bingoQuizSettings} from '../constants/bingoQuiz';
import {defaultState as memoryGameSettings} from '../constants/memoryGame';

export const setSettings = settings => {
  return (dispatch, getState) => {
    const state = getState();
    const appName = metaInfoSelectors.getAppName(state);
    const generalSettings = {...settings.general, meta: metaInfoSelectors.getMetaInfo(state)};
    localStorage.setItem(`${appName}/generalSettings`, JSON.stringify(generalSettings));
    dispatch(generalSettingsActions.setGeneralSettings(generalSettings));

    const bingoQuizSettings = {...settings.bingoQuiz};
    localStorage.setItem(`${appName}/bingoQuizSettings`, JSON.stringify(bingoQuizSettings));
    dispatch(bingoQuizSettingsActions.setBingoQuizSettings(bingoQuizSettings));

    const quizSettings = {...settings.quiz};
    localStorage.setItem(`${appName}/quizSettings`, JSON.stringify(quizSettings));
    dispatch(quizSettingsActions.setQuizSettings(quizSettings));

    const memoryGameSettings = {...settings.memoryGame};
    localStorage.setItem(`${appName}/memoryGameSettings`, JSON.stringify(memoryGameSettings));
    dispatch(memoryGameActions.setMemoryGameSettings(memoryGameSettings));
  };
};

const setSettingsSuccess = (settings) => ({
  type: settingsConstants.SET_SETTINGS,
  payload: settings
});

export const setSettingsFromAuth = appName => {
  return (dispatch, getState) => {
    const defaultSettings = {
      general: {...defaultGeneralSettings, meta: null},
      quiz: defaultQuizSettings,
      bingoQuiz: bingoQuizSettings,
      memoryGame: memoryGameSettings
    };
    settingsService.getSettingsFromDb()
      .then(settingsFromDb => {
        if (!settingsFromDb) {
          settingsService.saveSettingsToDb(defaultSettings, appName);
          dispatch(setSettings(defaultSettings));
        } else {
          if (!settingsFromDb.settings[appName]) {
            dispatch(setSettings(defaultSettings));
          } else {
            dispatch(setSettings(settingsFromDb.settings[appName]));
          }
        }
      }).catch(error => console.error(error));
  };
};
