import * as constants from '../constants/bingoQuiz';
import * as settingsSelectors from '../selectors/settings';
import settingsService from '../services/settings';

export const setDefaultSize = appName => ({
  type: constants.SET_DEFAULT_GRID_SIZE,
  appName
});

export const setGridSize = (value, appName) => {
  return (dispatch, getState) => {
    const state = getState();
    const bingoQuizState = {...settingsSelectors.getBingoQuizSettings(state), value};
    localStorage.setItem(`${appName}/bingoQuizSettings`, JSON.stringify(bingoQuizState));
    settingsService.saveBingoQuizToDb(bingoQuizState, state);
    dispatch({
      type: constants.SET_GRID_SIZE,
      value
    });
  };
};

export const setBingoQuizSettings = settings => ({
  type: constants.SET_BINGO_QUIZ_SETTINGS,
  settings
});
