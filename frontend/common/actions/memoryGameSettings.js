import * as constants from '../constants/memoryGame';
import * as settingsSelectors from '../selectors/settings';
import settingsService from '../services/settings';

export const setDefaultSettings = appName => {
  return {
    type: constants.SET_DEFAULT_SETTINGS,
    appName
  }
}

export const setAmountOfCards = (amount, appName) => {
  return (dispatch, getState) => {
    const state = getState();
    const memoryGameSettings = {...settingsSelectors.getMemoryGameSettings(state), amount};
    localStorage.setItem(`${appName}/memoryGameSettings`, JSON.stringify(memoryGameSettings));
    settingsService.saveMemoryGameToDb(memoryGameSettings, state);
    dispatch({
      type: constants.SET_AMOUNT_OF_CARDS,
      amount
    });
  };
}

export const setMemoryGameSettings = settings => ({
  type: constants.SET_MEMORY_GAME_SETTINGS,
  settings
});
