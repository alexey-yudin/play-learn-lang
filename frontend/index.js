import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, browserHistory } from 'react-router'
import ReduxThunk from 'redux-thunk';
import rootReducer from './rootReducer.js';
import routes from './routes';
import * as firebase from 'firebase';
import {firebaseConfig} from './config';

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/dist/js/bootstrap.min.js';
import './styles/main.scss';

firebase.initializeApp(firebaseConfig);
const createStoreWithMiddleware = applyMiddleware(ReduxThunk)(createStore);

$(document).ready(function() {
  ReactDOM.render(
    <Provider store={createStoreWithMiddleware(rootReducer)}>
      <Router history={browserHistory} routes={routes}/>
    </Provider>
    , document.querySelector('#app'));
});
