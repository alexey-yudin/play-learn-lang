import { combineReducers } from 'redux';

import packages from './packages';
import cards from './cards';
import nestedPacks from './nestedPackages';

const rootReducer = combineReducers({
  packages,
  cards,
  nestedPacks
});

export default rootReducer;
