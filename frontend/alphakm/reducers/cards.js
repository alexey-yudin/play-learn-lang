import * as types from '../actionTypes/cards';
import * as commonTypes from '../../common/constants/reviewCards';

const defaultState = {
  cards: [],
  config: {},
  id: ''
}

export default (state = defaultState, action) => {
  switch(action.type) {
    case types.FETCH_CARDS_SUCCESS:
    {
      const newState = {...state,
        cards: action.cardsItem.cards,
        config: action.cardsItem.config,
        id: action.cardsItem.id
      };
      return newState;
    }

    case types.FETCH_CARDS_ERROR:
      return state;

    case commonTypes.CLEAR_REVIEW_CARDS_LIST:
      return defaultState;

    default:
      return state;
  }
}
