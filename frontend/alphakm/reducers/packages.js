import * as types from '../actionTypes/packages';

export default (state = [], action) => {
  switch(action.type) {
    case types.FETCH_PACKAGE_SUCCESS:
      if (action.packages) {
        return action.packages;
      } else {
        return state;
      }

    case types.FETCH_PACKAGE_ERROR:
      return state;

    default:
      return state;
  }
}
