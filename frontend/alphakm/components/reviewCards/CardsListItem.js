import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as localityConstants from '../../../common/constants/localities';
import RomanisationWithTranslation from
  '../../../common/components/shared/romanization/RomanisationWithTranslation';
import AudioPlayer from '../../../common/components/shared/generalAudio/AudioPlayer';
import CardImageHelper from '../../../common/components/shared/helpers/CardImageHelper';

class CardsListItem extends Component {
  constructor(props) {
    super(props);
    this.playAudio = this.playAudio.bind(this);
    this.cardImageHelper = new CardImageHelper;
  }

  playAudio() {
    const card = this.props.card;
    $(`#khmer-review-card${card.id}`).trigger('play');
  }

  renderBody() {
    const { card } = this.props;
    if (this.cardImageHelper.cardHasImage(card)) {
      return (
        <img src={card.image.url_600} alt="" className='img-responsive package-card-img'/>
      );
    } else {
      return (
        <div className={`${this.props.textColor}`}>
          {card.translations[1].translation.translation}
        </div>
      );
    }
  }

  isTranslationExist() {
    const { card, packtype } = this.props;
    const foreignTranslation = card.translations[0].translation.translation;
    const nativeTranslation = card.translations[1].translation.translation;
    return packtype === 'alphabet_samples_for_letters' && foreignTranslation !== nativeTranslation;
  }

  renderTranslation() {
    const { card } = this.props;
    if (this.isTranslationExist()) {
      return (
        <RomanisationWithTranslation
          translations={card.translations}
          localityType={localityConstants.NATIVE_LOCALITY}
        />
      );
    }
  }

  render() {
    const { card } = this.props;
    return (
      <div className="col-xs-12 col-sm-6 col-md-4">
        <div className="review-card-item khmer-card-item">
          <div className="body">
            {this.renderBody()}
          </div>
          <div className="number">
            {card.number}
          </div>
          <div onClick={this.playAudio} className="cursor-pointer">
            <RomanisationWithTranslation
              translations={card.translations}
              localityType={localityConstants.FOREIGN_LOCALITY}
            />
            <AudioPlayer
              localityType={localityConstants.FOREIGN_LOCALITY}
              translations={card.translations}
              generalId={`khmer-review-card`}
              cardId={card.id}
              autoPlay={false}
            />
          </div>
          <div>
            {this.renderTranslation()}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    packtype: state.khmerAlphabet.cards.config.packtype
  }
};

CardsListItem.propTypes = {
  card: PropTypes.object.isRequired,
  textColor: PropTypes.string.isRequired
};

export default connect(mapStateToProps)(CardsListItem);
