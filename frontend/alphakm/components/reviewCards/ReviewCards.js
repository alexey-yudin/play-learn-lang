import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/cards'
import CardsListItem from './CardsListItem';
import CardsListItemNotes from './CardsListItemNotes';
import CardsList from '../../../common/components/reviewCards/CardsList';
import './styles.scss';

class ReviewCards extends Component {
  constructor(props) {
    super(props);
    this.renderCard = this.renderCard.bind(this);
    this.textColor = this.textColor.bind(this);
  }

  textColor() {
    const id = this.props.params.id;
    if (id.indexOf('consonants_sub') > -1) {
      return 'color-green';
    } else if (id.indexOf('consonants') > -1) {
      return 'color-blue';
    } else if (id.indexOf('vowels_independent') > -1) {
      return 'color-red';
    } else if (id.indexOf('vowels_dependant_consonant') > -1) {
      return 'color-blue';
    } else if (id.indexOf('vowels') > -1) {
      return 'color-yellow';
    } else if (id.indexOf('diacritics') > -1) {
      return 'color-dark-red';
    } else if (id.indexOf('numbers') > -1) {
      return 'color-pink';
    } else {
      return 'color-default';
    }
  }

  hasTranslation(card) {
    let found = false;
    const translations = card.translations;
    if (Array.isArray(translations)) {
      translations.forEach(translation => {
        if (translation.translation.translation) {
          found = true;
        }
      });
    }
    return found;
  }

  renderCard(card, index) {
    const textColor = this.textColor();
    const { cards } = this.props;
    if (card['cfg-note_char'] === 'true') {
      return (
        <CardsListItemNotes
          key={index}
          card={card}
          textColor={textColor}
          cards={cards}
        />
      );
    } else {
      return (
        <CardsListItem
          key={index}
          card={card}
          textColor={textColor}
        />
      );
    }
  }

  render() {
    return (
      <CardsList
        fetchCards={this.props.fetchCards}
        cards={this.props.cards}
        cardsId={this.props.params.id}
        renderCard={this.renderCard}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    cards: state.khmerAlphabet.cards.cards
  }
}

export default connect(mapStateToProps, {
  fetchCards: actions.fetchCards
})(ReviewCards);
