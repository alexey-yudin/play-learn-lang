import React, { Component, PropTypes } from 'react';
import CardsListItemNotesBody from '../../../common/components/reviewCards/CardListItemNotes';

class CardsListItemNotes extends Component {
  render() {
    const { card, cards } = this.props;
    return (
      <div className="col-xs-12 col-sm-6 col-md-4">
        <div className="review-card-item khmer-definition-card-item">
          <CardsListItemNotesBody
            card={card}
            cards={cards}
          />
        </div>
      </div>
    );
  }
}

CardsListItemNotes.propTypes = {
  card: PropTypes.object.isRequired,
  cards: PropTypes.array.isRequired
}

export default CardsListItemNotes;
