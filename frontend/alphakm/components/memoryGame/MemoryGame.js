import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions/cards';
import MemoryGameContainer from '../../../common/components/memoryGame/MemoryGameContainer';

class MemoryGame extends Component {
  render() {
    return (
      <MemoryGameContainer
        cards={this.props.cards}
        fetchCards={this.props.fetchCards}
        cardsId={this.props.params.id}
        memoryGameSettings={this.props.memoryGameSettings}
        appName={'khmerAlphabet'}
        packConfig={this.props.packConfig}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    cards: state.khmerAlphabet.cards.cards,
    packConfig: state.khmerAlphabet.cards.config,
    memoryGameSettings: state.common.memoryGameSettings
  }
}

export default connect(mapStateToProps, {
  fetchCards: actions.fetchCards
})(MemoryGame);
