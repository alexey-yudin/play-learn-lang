import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions/packages';
import PackageListItem from './PackageListItem';
import PackageListAbstract from '../../../common/components/packages/PackageList';

class PackageList extends Component {
  constructor(props) {
    super(props);
    this.renderPackage = this.renderPackage.bind(this);
  }

  renderPackage(packageItem) {
    return (
      <PackageListItem
        key={packageItem.id}
        packageItem={packageItem}
      />
    );
  }

  render() {
    const { packages, fetchPackages } = this.props;
    return (
      <PackageListAbstract
        packages={packages}
        renderPackage={this.renderPackage}
        fetchPackages={fetchPackages}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    packages: state.khmerAlphabet.packages
  }
}

export default connect(mapStateToProps, {
  fetchPackages: actions.fetchPackages
})(PackageList);
