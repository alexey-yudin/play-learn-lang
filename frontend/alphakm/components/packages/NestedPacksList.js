import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/packages';

import PackageListItem from './PackageListItem';
import PackageList from '../../../common/components/packages/PackageList';

class NestedPacksList extends Component {
  constructor(props) {
    super(props);
    this.renderPackage = this.renderPackage.bind(this);
  }

  renderPackage(packageItem) {
    return (
      <PackageListItem
        key={packageItem.id}
        packageItem={packageItem}
      />
    );
  }

  render() {
    const { nestedPacks, fetchNestedPackages, params } = this.props;
    return (
      <PackageList
        packages={nestedPacks}
        renderPackage={this.renderPackage}
        fetchPackages={fetchNestedPackages}
        packageId={params.id}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    nestedPacks: state.khmerAlphabet.nestedPacks
  }
}

export default connect(mapStateToProps, {
  fetchNestedPackages: actions.fetchNestedPackages
})(NestedPacksList);
