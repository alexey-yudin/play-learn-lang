import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import * as routeConstants from '../../../common/constants/routes';
import PackageListItemMenu from '../../../common/components/packages/PackageListItemMenu';

class PackageListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      packtype: null,
      isMenu: false
    };
    this.toggleIsMenu = this.toggleIsMenu.bind(this);
  }

  componentDidMount() {
    const { packageItem } = this.props;
    if (packageItem.hasChildPacks === 'true') {
      const packageElem = this.refs['package-item'];
      packageElem.onclick = () => {
        browserHistory.push(`/${routeConstants.alphakm}/package-list/${packageItem.id}`);
      }
    }
    this.setPacktype();
  }

  toggleIsMenu() {
    this.setState({ isMenu: !this.state.isMenu });
  }

  setPacktype() {
    const { packageItem, cardsConfig } = this.props;
    const config = cardsConfig.find(cardConfig => cardConfig.id === packageItem.id);
    if (config) {
      const packtype = config.config.packtype;
      this.setState({ packtype });
    }
  }

  renderPackageItem() {
    const { packageItem } = this.props;
    return (
      <div className='col-xs-12 col-sm-6 col-md-4'>
        <div className="package-item" ref="package-item">
          <div>
            <img src={packageItem.image.url_standard} alt=""/>
          </div>
          <div className="locality-translation">
            {packageItem.label}
          </div>
        </div>
      </div>
    );
  }

  renderPackage() {
    const { packageItem } = this.props;
    if (this.state.isMenu) {
      return (
        <PackageListItemMenu appName={'alphakm'} packageId={packageItem.id}/>
      );
    } else {
      return (
        <div>{this.renderPackageItem()}</div>
      );
    }
  }

  render() {
    return (
      <div onClick={this.toggleIsMenu}>
        {this.renderPackage()}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    cardsConfig: state.common.viewConfig.cardsConfig
  }
};

PackageListItem.propTypes = {
  packageItem: PropTypes.object.isRequired
};

export default connect(mapStateToProps)(PackageListItem);
