import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as constants from '../../../common/constants/quiz';

import * as actions from '../../actions/cards';
import QuizPage from '../../../common/components/quiz/QuizPage';

class Quiz extends Component {
  render() {
    return (
      <QuizPage
        cards={this.props.cards}
        fetchCards={this.props.fetchCards}
        appName={'khmerAlphabet'}
        cardsId={this.props.params.id}
        quizSettings={this.props.quizSettings}
        pageId={this.props.params.id}
        mode={constants.QUIZ_MODE}
        packConfig={this.props.packConfig}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    cards: state.khmerAlphabet.cards.cards,
    packConfig: state.khmerAlphabet.cards.config,
    quizSettings: state.common.quizSettings
  }
}

export default connect(mapStateToProps, {
  fetchCards: actions.fetchCards
})(Quiz);
