import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions/cards';
import CarouselContainer from '../../../common/components/carousel/CarouselContainer';

class CardsCarousel extends Component {
  render() {
    return (
      <CarouselContainer
        cards={this.props.cards}
        fetchCards={this.props.fetchCards}
        cardsId={this.props.params.id}
        packConfig={this.props.packConfig}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    cards: state.khmerAlphabet.cards.cards,
    packConfig: state.khmerAlphabet.cards.config
  }
}

export default connect(mapStateToProps, {
  fetchCards: actions.fetchCards
})(CardsCarousel);
