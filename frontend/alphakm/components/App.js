import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as localityActions from '../actions/localities';
import * as viewConfigActions from '../../common/actions/viewConfig';
import * as settingsActions from '../../common/actions/generalSettings';
import * as routeConstants from '../../common/constants/routes';
import NavBar from '../../common/components/navbar/NavBar';

class App extends Component {
  componentWillMount() {
    this.props.setAppName(routeConstants.alphakm);
    this.props.fetchLocalities();
    this.props.fetchMetaInfo(routeConstants.alphakm);
    this.props.getCardsConfig(routeConstants.alphakm);
  }

  render() {
    const{ params, children, location } = this.props;
    return (
      <div>
        <NavBar params={params} location={location}/>
        <div className='container'>
          {children}
        </div>
      </div>
    );
  }
}

export default connect(null, {
  fetchLocalities: localityActions.fetchLocalities,
  setAppName: settingsActions.setAppName,
  fetchMetaInfo: settingsActions.fetchMetaInfo,
  getCardsConfig: viewConfigActions.getCardsConfig
})(App);
