import axios from 'axios';
import * as types from '../actionTypes/cards';
import * as routeConstants from '../../common/constants/routes';

export const fetchCards = id => {
  return dispatch => {
    axios.get(`/api/cards/getCards/${routeConstants.alphakm}/${id}`)
      .then(response => dispatch(fetchCardsSuccess(response.data)))
      .catch(error => dispatch(fetchCardsError()));
  }
}

const fetchCardsSuccess = cardsItem => {
  return {
    type: types.FETCH_CARDS_SUCCESS,
    cardsItem
  }
}

const fetchCardsError = () => {
  return {
    type: types.FETCH_CARDS_ERROR
  }
}
