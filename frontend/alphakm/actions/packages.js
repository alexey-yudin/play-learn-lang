import axios from 'axios';
import * as routeConstants from '../../common/constants/routes';
import * as types from '../actionTypes/packages';

export const fetchPackages = () => {
  return dispatch => {
    axios.get(`/api/package/getPackages/${routeConstants.alphakm}`)
      .then(response => {
        dispatch({
          type: types.FETCH_PACKAGE_SUCCESS,
          packages: response.data.packages
        });
      }).catch(error => {
        dispatch({
          type: types.FETCH_PACKAGE_ERROR
        });
      });
  }
}

export const fetchNestedPackages = id => {
  return dispatch => {
    axios.get(`/api/package/getNestedPackages/${routeConstants.alphakm}/${id}`)
      .then(response => {
        dispatch({
          type: types.FETCH_NESTED_PACKAGE_SUCCESS,
          packages: response.data.packages
        });
      }).catch(error => {
        dispatch({
          type: types.FETCH_NESTED_PACKAGE_ERROR
        });
      });
  }
}
