import axios from 'axios';
import * as routeConstants from '../../common/constants/routes';
import * as constants from '../../common/constants/localities';

export const fetchLocalities = () => {
  return dispatch => {
    axios.get(`/api/localities/getLocalities/${routeConstants.alphakm}`)
      .then(response => {
        dispatch({
          type: constants.FETCH_LOCALITIES,
          localities: response.data
        });
      });
  }
}
