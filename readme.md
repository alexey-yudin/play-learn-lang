### To get started
1. Install Node.js 6 or higher
2. Download this repo
3. Open the command line and cd to the root directory of this repo
4. ```npm install``` - to install the packages
5. ```npm run build:dev``` - to build the project and run the compiler
6. In a new tab ```npm start``` - to start the server
7. Open in browser [localhost:3000](http://localhost:3000)


### To deploy on heroku
1. Install git
2. Install the heroku toolbet
3. Open the command line and cd to the root directory of this repo
4. ```heroku git:remote -a lyrebirdlearns``` - to add a repository to the git remote
5. ```git push heroku``` - to deploy

Heroku needs to have the NPM_CONFIG_PRODUCTION config variable to be false.
