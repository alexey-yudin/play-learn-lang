const fs = require('fs');
const Hjson = require('hjson');

const DirectoryReader = require('../api/models/helpers/DirectoryReader');
const Package = require('../api/models/Package');
const Card = require('../api/models/Card');
const Locality = require('../api/models/Locality');
const Translation = require('../api/models/Translation');
const MetaInfo = require('../api/models/MetaInfo');

const jsonAppsPath = `${__dirname}/../config/apps`;
const translationPath = `${__dirname}/../config/translations`;

const pathToStatic = `${__dirname}/../public/static/`;
const directoryReader = new DirectoryReader(jsonAppsPath);

class JsonWriter {
  constructor(_package, card, locality, translation, metaInfo, pathToStatic) {
    this.package = _package;
    this.card = card;
    this.locality = locality;
    this.translation = translation;
    this.metaInfo = metaInfo;
    this.pathToStatic = pathToStatic;
  }

  save() {
    return new Promise(async (resolve, reject) => {
      try {
        console.info('Starting generating json.');
        const lyrebird = {};

        const packages = await this.package.getPackages()
        console.info('The packages done')
        lyrebird.packages = packages;

        const nestedPackages = await this.package.getNestedPackages();
        console.info('The nested packages done');
        lyrebird.nestedPackages = nestedPackages;

        const nestedPackageIds = await this.package.getNestedPackagesIds();
        console.info('The nested packages ids done');
        lyrebird.nestedPackageIds = nestedPackageIds;

        const cards = await this.card.getCards();
        console.info('The cards done');
        lyrebird.cards = cards;

        const cardsConfig = await this.card.getCardsConfig();
        console.info('The cards config done');
        lyrebird.cardsConfig = cardsConfig;

        const localities = await this.locality.getLocalities();
        console.info('The localities done');
        lyrebird.localities = localities;

        const translations = await this.translation.getTranslations();
        console.info('The translations done');
        lyrebird.translations = translations;

        const metaInfo = await this.metaInfo.getMetaInfo();
        console.info('The meta info done');
        lyrebird.metaInfo = metaInfo;

        this.writeFile(lyrebird, resolve);
      } catch (error) {

      }
    });
  }

  writeFile(lyrebird, resolve) {
    const stringifiedLyrebird = Hjson.stringify(lyrebird);
    const path = `${this.pathToStatic}/jsonData.json`;
    fs.writeFile(path, stringifiedLyrebird, 'utf8', (err) => {
      if (err) reject(err);
      resolve();
    });
  }
}

new JsonWriter(
  new Package(directoryReader),
  new Card(directoryReader),
  new Locality(directoryReader),
  new Translation(translationPath),
  new MetaInfo(directoryReader),
  pathToStatic
).save().then(() => {
  console.info('Successfully writen the json');
}).catch(error => {
  console.error(error);
});
