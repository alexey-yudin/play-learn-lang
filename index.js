const express = require('express');
const favicon = require('serve-favicon');
const logger = require('morgan');
const path = require('path');
const fs = require('fs');
const Hjson = require('hjson');

const port = process.env.PORT || 3000;
const app = express();

const packageRoutes = require('./api/routes/package');
const cardRoutes = require('./api/routes/cards');
const localityRoutes = require('./api/routes/localities');
const translationRoutes = require('./api/routes/translations');
const metaInfoRoutes = require('./api/routes/metaInfo');
const index = require('./api/routes/index');

app.use(favicon(path.join(__dirname, '/public/static/', 'favicon.ico')));
app.use(logger('dev'));

app.set('views', path.join(__dirname, 'public'));
app.set('view engine', 'jade');

app.use(express.static(path.join(__dirname, '/public/static/')));
app.use(express.static(path.join(__dirname, '/public/audio/')));

app.use('/api/package/', packageRoutes);
app.use('/api/cards/', cardRoutes);
app.use('/api/localities/', localityRoutes);
app.use('/api/translations/', translationRoutes);
app.use('/api/metaInfo/', metaInfoRoutes);
app.use('/', index);

fs.readFile(`${__dirname}/public/static/jsonData.json`, (err, file) => {
  if (err) {
    console.error(err);
  } else {
    global.lyrebird = Hjson.parse(file.toString());
  }
});
app.listen(port, () => console.info('ok'))
